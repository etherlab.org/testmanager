/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2018  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "DataSlot.h"
#include "DataModel.h"
#include "DataSource.h"
#include "ScaleDelegate.h"
#include "SlotModelCollection.h"
#include "SlotModel.h"

#include <pdcom5/Variable.h>

#include <QtPdCom1/ScalarSubscriber.h>

#include <QFile>
#include <QIcon>
#include <QJsonObject>
#include <QLocale>
#include <QSettings>
#include <QSslCertificate>
#include <QVariant>

/****************************************************************************/

DataSlot::DataSlot(SlotNode *parent, int pos, Flags flags):
    BasicDataSlot(parent, {}, pos),
    period(QtPdCom::event_mode),
    scale(1.0),
    offset(0.0),
    tau(0.0),
    flags_(flags)
{
}

/****************************************************************************/

QIcon BasicDataSlot::getIcon(const DataModel *dataModel) const
{
    if (!dataModel) {
        return QIcon();
    }

    DataNode *dataNode(dataModel->findDataNode(url));
    if (dataNode) {
        return dataNode->getIcon();
    }
    else {
        return QIcon();
    }
}

/****************************************************************************/

void BasicDataSlot::fromJson(const QJsonValue &value)
{
    QJsonObject obj(value.toObject());
    url = obj["url"].toString();
    alias = obj["alias"].toString();
}

/****************************************************************************/

QJsonValue BasicDataSlot::toJson() const
{
    QJsonObject obj;
    obj["url"] = url.toString();

    if (not alias.isEmpty()) {
        obj["alias"] = alias;
    }

    return obj;
}

/****************************************************************************/

QVariant BasicDataSlot::nodeData(
        int role,
        int section,
        const DataModel *dataModel) const
{
    QVariant ret;
        switch (role) {
        case Qt::DisplayRole:
            switch (section) {
                case UrlColumn:
                    ret = url;
                    break;
                case AliasColumn:
                    ret = alias;
                    break;
                default:
                    break;
            }
            break;
        case Qt::EditRole:
            switch (section) {
                case AliasColumn:
                    ret = alias;
                    break;
                default:
                    break;
            }
            break;
        case Qt::DecorationRole:
            switch (section) {
                case UrlColumn:
                    return getIcon(dataModel);
                    break;
                default:
                    break;
            }
            break;
        case Qt::ToolTipRole:
            switch (section) {
                // also used in SlotView.cpp
                case UrlColumn:
                    ret = url;
                    break;
            }
            break;

        default:
            break;
    }

    return ret;

}

/****************************************************************************/

void BasicDataSlot::nodeFlags(Qt::ItemFlags &flags, int section) const
{
    if (section == AliasColumn)
        flags |= Qt::ItemIsEditable;
}

/****************************************************************************/

bool BasicDataSlot::nodeSetData(int section, const QVariant &value)
{
    if (section == AliasColumn) {
        alias = value.toString();
        return true;
    }
    return false;
}

/****************************************************************************/

void BasicDataSlot::replaceUrl(const QUrl &oldUrl, const QUrl &newUrl)
{
    QUrl dataSourceUrl(url.adjusted(QUrl::RemovePassword |
                QUrl::RemovePath | QUrl::RemoveQuery |
                QUrl::RemoveFragment));
    if (dataSourceUrl != oldUrl) {
        return;
    }

    url.setScheme(newUrl.scheme());
    url.setAuthority(newUrl.authority()); // user, pass, host, port
}

/****************************************************************************/

void BasicDataSlot::appendDataSources(DataModel *dataModel)
{
    dataModel->ensureDataSource(url, QSettings());

    SlotNode::appendDataSources(dataModel);
}

/****************************************************************************/

void DataSlot::fromJson(const QJsonValue &value)
{
    BasicDataSlot::fromJson(value);

    QJsonObject obj(value.toObject());

    period = QtPdCom::event_mode;
    if (const auto d = obj["period"].toDouble())
        period = std::chrono::duration<double>(d);
    if (obj.contains("scale")) {
        scale = obj["scale"].toDouble();
    }
    offset = obj["offset"].toDouble();
    if (!(flags_ & DoesNotSupportTau))
        tau = obj["tau"].toDouble();
    if (flags_ & SupportsColor)
        color = obj["color"].toString();
    if (obj.contains("selector") && !(flags_ & DoesNotSupportSelector)) {
        selector.fromJson(obj["selector"]);
    } else {
        selector = {};
    }
}

/****************************************************************************/

QJsonValue DataSlot::toJson() const
{
    QJsonObject obj = BasicDataSlot::toJson().toObject();

    if (period.getInterval()) {
        obj["period"] = period.getInterval();
    }

    if (scale != 1.0) {
        obj["scale"] = scale;
    }

    if (offset) {
        obj["offset"] = offset;
    }

    if (!(flags_ & DoesNotSupportTau) && tau) {
        obj["tau"] = tau;
    }

    if ((flags_ & SupportsColor) && color.isValid()) {
        obj["color"] = color.name();
    }

    if (!(flags_ & DoesNotSupportTau)) {
        const auto s = selector.toJson();
        if (!s.empty())
            obj["selector"] = s;
    }

    return obj;
}

/****************************************************************************/

QVariant DataSlot::nodeData(int role, int section,
        const DataModel *dataModel) const
{
    QVariant ret = BasicDataSlot::nodeData(role, section, dataModel);

    switch (role) {
        case Qt::DisplayRole:
            switch (section) {
                case SelectorColumn:
                    if (!(flags_ & DoesNotSupportSelector))
                        ret = selector.itemData(Qt::DisplayRole);
                    break;
                case PeriodColumn:
                    ret = QLocale().toString(period.getInterval());
                    break;
                case ScaleColumn:
                    {
                        QString text = ScaleDelegate::text(scale);
                        if (text.isEmpty()) {
                            ret = QLocale().toString(scale);
                        }
                        else {
                            ret = text;
                        }
                    }
                    break;
                case OffsetColumn:
                    ret = QLocale().toString(offset);
                    break;
                case TauColumn:
                    if (!(flags_ & DoesNotSupportTau))
                        ret = QLocale().toString(tau);
                    break;
                case ColorColumn:
                    if (flags_ & SupportsColor)
                        ret = color.name();
                    break;
                default:
                    break;
            }
            break;

        case Qt::EditRole:
            switch (section) {
                case SelectorColumn:
                    if (!(flags_ & DoesNotSupportSelector)) {
                        ret = QJsonValue(selector.toJson()).toVariant();
                    }
                    break;
                case PeriodColumn:
                    ret = QLocale().toString(period.getInterval());
                    break;
                case ScaleColumn:
                    ret = QLocale().toString(scale);
                    break;
                case OffsetColumn:
                    ret = QLocale().toString(offset);
                    break;
                case TauColumn:
                    if (!(flags_ & DoesNotSupportTau))
                        ret = QLocale().toString(tau);
                    break;
                case ColorColumn:
                    if (flags_ & SupportsColor)
                        ret = color.rgb();
                    break;
                default:
                    break;
            }
            break;

        case Qt::DecorationRole:
            switch (section) {
                case ColorColumn:
                    if (flags_ & SupportsColor)
                        ret = color;
                    break;
            }
            break;

        case Qt::ToolTipRole:
            switch (section) {
                case SelectorColumn:
                    if (!(flags_ & DoesNotSupportSelector))
                        ret = selector.itemData(Qt::ToolTipRole);
                    break;
            }
            break;

        case Qt::UserRole:
            switch (section) {
                case ScaleColumn:
                    ret = scale; // used in ScaleDelegate to compare raw value
                    break;
                default:
                    break;
            }
            break;

        default:
            break;
    }

    return ret;
}

/****************************************************************************/

void DataSlot::nodeFlags(Qt::ItemFlags &flags, int section) const
{
    BasicDataSlot::nodeFlags(flags, section);

    if (section == SelectorColumn && !(flags_ & DoesNotSupportSelector)) {
        flags |= Qt::ItemIsEditable;
    }
    if (section >= PeriodColumn && section < TauColumn) {
        flags |= Qt::ItemIsEditable;
    }
    if (section == TauColumn && !(flags_ & DoesNotSupportTau)) {
        flags |= Qt::ItemIsEditable;
    }
    if (section == ColorColumn && flags_ & SupportsColor) {
        flags |= Qt::ItemIsEditable;
    }
}

/****************************************************************************/

bool DataSlot::nodeSetData(int section, const QVariant &value)
{
    if (BasicDataSlot::nodeSetData(section, value))
        return true;

    switch (section) {
        case SelectorColumn:
            if (flags_ & DoesNotSupportSelector)
                return false;
            return selector.fromJson(QJsonValue::fromVariant(value));

        case PeriodColumn:
            if (const double tmp = QLocale().toDouble(value.toString()))
                period = std::chrono::duration<double>(tmp);
            else
                period = QtPdCom::event_mode;
            return true;

        case ScaleColumn:
            scale = QLocale().toDouble(value.toString());
            return true;

        case OffsetColumn:
            offset = QLocale().toDouble(value.toString());
            return true;

        case TauColumn:
            tau = QLocale().toDouble(value.toString());
            return true;

        case ColorColumn:
            {
                QColor c(QColor().fromRgb(value.toInt()));
                if (c.isValid()) {
                    color = c;
                    return true;
                }
            }
            break;

        default:
            break;
    }

    return false;
}

/****************************************************************************/

void DataSlot::countColors(QMap<QRgb, unsigned int> &colorUsage) const
{
    auto colorIt = colorUsage.find(color.rgba());
    if (colorIt != colorUsage.end()) {
        colorIt.value()++;
    }
}

/****************************************************************************/

bool ScalarSubscriberDataSlot::nodeSetData(int col, const QVariant &data)
{
    if (!DataSlot::nodeSetData(col, data)) {
        return false;
    }

    const auto dm = getRoot().dataModel;
    const auto oldVar = nodeData(Qt::EditRole, col, dm);
    auto saved_url = std::move(url);
    const auto node = getRoot().dataModel->findDataNode(saved_url);
    if (!node) {
        DataSlot::nodeSetData(col, oldVar);
        return false;
    }
    subscriber.clearVariable();
    node->notifyUnsubscribed();
    try {
        subscriber.setVariable(
                node->getVariable(),
                selector.toPdComSelector(),
                period,
                scale,
                offset,
                tau);
    }
    catch (const PdCom::Exception &) {
        DataSlot::nodeSetData(col, oldVar);
        return false;
    }
    url = std::move(saved_url);
    node->notifySubscribed(period);
    return true;
}

/****************************************************************************/

bool ScalarSubscriberDataSlot::supportsRemovingByUser() const
{
    return true;
}

/****************************************************************************/

ScalarSubscriberDataSlot::~ScalarSubscriberDataSlot()
{
    const auto node = getRoot().dataModel->findDataNode(url);
    url.clear();
    subscriber.clearVariable();
    if (node) {
        node->notifyUnsubscribed();
    }
    if (const auto sm = getSlotModel()) {
        sm->clearWidgetData();
    }
}

/****************************************************************************/

void ScalarSubscriberDataSlot::appendDataSources(DataModel *dataModel)
{
    if (url.isEmpty())
        url = std::move(url_backup);

    DataSlot::appendDataSources(dataModel);

    auto *dataNode = dataModel->findDataNode(url);

    if (dataNode) {
        const auto pv(dataNode->getVariable());
        if (!pv.empty()) {
            subscriber.setVariable(pv,
                    selector.toPdComSelector(),
                    period,
                    scale,
                    offset,
                    tau);
            dataNode->notifySubscribed(period);
        } else {
            qWarning() << tr("Url not found: %1").arg(url.toString());
        }
    }

}

/****************************************************************************/

void ScalarSubscriberDataSlot::disconnectDataSources()
{
    auto *dataNode = getRoot().dataModel->findDataNode(url);
    url_backup = std::move(url);
    if (dataNode) {
        subscriber.clearVariable();
        dataNode->notifyUnsubscribed();
    }
}

/****************************************************************************/
