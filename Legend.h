/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2019  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef LEGEND_H
#define LEGEND_H

/****************************************************************************/

#include <QFrame>

class WidgetContainer;

/****************************************************************************/

class Legend:
    public QFrame
{
    Q_OBJECT

    public:
        Legend(WidgetContainer *, QWidget *);

        void editModeChanged();
        void setAlignment(Qt::AlignmentFlag);
        Qt::AlignmentFlag getAlignment() const { return alignment; }

    protected:
        void resizeEvent(QResizeEvent *);
        void contextMenuEvent(QContextMenuEvent *);
        void mouseMoveEvent(QMouseEvent *);
        void mousePressEvent(QMouseEvent *);
        void mouseReleaseEvent(QMouseEvent *);
        void keyPressEvent(QKeyEvent *event);
        void paintEvent(QPaintEvent *);

    private:
        WidgetContainer * const container;
        Qt::AlignmentFlag alignment;

        enum { NumHandles = 4 };
        QRect area[NumHandles];

        enum ResizeDirection {
            None,
            Left,
            Top,
            Right = 4,
            Bottom = 8
        };
        int resizeDir;
        QPoint startGlobalPos;
        QRect startRect;
        bool moving;

        void adjustLeft(int);
        void adjustTop(int);
        void adjustWidth(int);
        void adjustHeight(int);
        Legend();

    private slots:
        void hideLegend();
        void moveLegend(QKeyEvent *);
        void editStyleSheet();
        void alignLeft();
        void alignCenter();
        void alignRight();
};

/****************************************************************************/

#endif
