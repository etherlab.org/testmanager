/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2023 Daniel Ramirez <dr@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "ParameterModel.h"

#include "DataModel.h"
#include "DataNode.h"
#include "DataSource.h"
#include "MainWindow.h"
#include "Parameter.h"
#include "ParameterSetModel.h"
#include "TabPage.h"
#include "WidgetContainer.h"

#include <pdcom5/SizeTypeInfo.h>

#include <QAbstractItemModel>
#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QJsonArray>
#include <QJsonDocument>
#include <QList>
#include <QLocale>
#include <QMimeData>
#include <QUrl>
#include <QVariant>

// literate macro
#define TM_LIT(X) #X
#define TM_STR(X) TM_LIT(X)

/****************************************************************************/

ParameterModel::ParameterModel(MainWindow *mainWindow):
    QAbstractTableModel(mainWindow),
    mainWindow(mainWindow)
{}

/****************************************************************************/

ParameterModel::~ParameterModel()
{
    clear();
}

/****************************************************************************/

void ParameterModel::clear()
{
    if (this->isEmpty()) {
        return;
    }

    beginResetModel();
    foreach (Parameter *parameter, parameters) {
        delete parameter;
    }
    parameters.clear();
    endResetModel();
}

/****************************************************************************/

void ParameterModel::fromJson(const QJsonArray &array)
{
    if (array.isEmpty()) {
        return;
    }
    beginInsertRows(QModelIndex(), 0,
        array.size() - 1);
    foreach (QJsonValue arrayValue, array) {
        Parameter *parameter = new Parameter(this);
        parameter->fromJson(arrayValue);

        parameters.append(parameter);
    }
    endInsertRows();
}

/****************************************************************************/

QJsonArray ParameterModel::toJson(bool withValue = true) const
{
    QJsonArray array;

    foreach (const Parameter *parameter, parameters) {
        array.append(parameter->toJson(withValue));
    }

    return array;
}

/****************************************************************************/

void ParameterModel::appendDataSources(DataModel *dataModel) const
{
    foreach (const Parameter *parameter, parameters) {
        if (!dataModel->hasDataSource(parameter->getUrl())) {
            DataSource *dataSource =
                new DataSource(dataModel, parameter->getUrl());
            dataModel->append(dataSource);
        }
    }
}

/****************************************************************************/

int ParameterModel::rowCount(const QModelIndex &parent) const
{
    int ret = 0;

    if (parent.isValid()) {
        qWarning() << __func__ << parent << "inval";
    }
    else {
        ret = parameters.size();
    }

#ifdef DEBUG_MODEL
    qDebug() << __func__ << parent << ret;
#endif

    return ret;
}

/****************************************************************************/

int ParameterModel::columnCount(const QModelIndex &) const
{
    return 3;  // name, value, url
}

/****************************************************************************/

QVariant ParameterModel::data(const QModelIndex &index, int role) const
{
    QVariant ret(QVariant::Invalid);

    if (index.isValid()) {
        Parameter *parameter = parameters[index.row()];

        switch (role) {
            case Qt::DisplayRole:
            case Qt::EditRole:
                switch (index.column()) {
                    case 0:
                        ret = parameter->getName();
                        break;
                    case 1:
                        if (parameter->hasData()) {
                            ret = buildVectorString(
                                    parameter->getCurrentValue());
                        }
                        break;
                    case 2:
                        ret = parameter->getUrl();
                        break;
                }
                break;

            case Qt::BackgroundRole:
                if (index.flags() & static_cast<Qt::ItemFlags>(SubInactive)) {
                    QBrush grayBrush(QColor(192, 192, 192)); // Adjust the color as needed
                    ret = grayBrush;
                }
                break;

            case Qt::DecorationRole:
                switch (index.column()) {
                    case 2:
                        ret = parameter->getIcon();
                        break;
                }
                break;
            case Qt::ToolTipRole:
                {
                    QString toolTip;
                    if (not parameter->getUrl().isEmpty()) {
                        toolTip += tr("Path: %1").arg(
                            parameter->getUrl().
                            adjusted(QUrl::RemoveAuthority
                            | QUrl::RemoveScheme).toString());
                    }
                    QString dimStr;
                    if (not parameter->getVariable()->empty()) {
                        dimStr = dimensionString(
                            parameter->getVariable()->getSizeInfo());
                        if (!dimStr.isEmpty()) {
                            if (!toolTip.isEmpty()) {
                                toolTip += "\n";
                            }
                            toolTip += tr("Dimension: %1").arg(dimStr);
                        }
                        if (!toolTip.isEmpty()) {
                            toolTip += "\n";
                        }
                    }

                    return toolTip;
                }
                break;

            default:
                break;
        }
    }

#ifdef DEBUG_MODEL
    if (role <= 1) {
        qDebug() << __func__ << index << role << ret;
    }
#endif

    return ret;
}

/****************************************************************************/

QVariant ParameterModel::headerData(
        int section,
        Qt::Orientation orientation,
        int role) const
{
    QVariant ret;

    if (orientation == Qt::Horizontal && role == Qt::DisplayRole) {
        switch (section) {
            case 0:
                ret = tr("Name");
                break;
            case 1:
                ret = tr("Value");
                break;
            case 2:
                ret = tr("Variable");
                break;
        }
    }

    return ret;
}

/****************************************************************************/

Qt::ItemFlags ParameterModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags ret = QAbstractTableModel::flags(index);

    if (index.isValid()) {
        Parameter *parameter = parameters[index.row()];
        if (not parameter->getSubscription()->empty()) {
            if (index.column() == 0 or index.column() == 1) {
                ret |= Qt::ItemIsEditable;
            }
        }
        else {
            ret |= static_cast<Qt::ItemFlags>(SubInactive);
        }
    }
    else {
        ret |= Qt::ItemIsDropEnabled;
    }

#ifdef DEBUG_MODEL
    qDebug() << __func__ << index << ret;
#endif

    return ret;
}

/****************************************************************************/

bool ParameterModel::setData(
        const QModelIndex &index,
        const QVariant &value,
        int role)
{
    if (!index.isValid() || role != Qt::EditRole) {
        return false;
    }

    Parameter *parameter = parameters.value(index.row());
    if (!parameter) {
        return false;
    }

    switch (index.column()) {
        case 0:  // Name
            parameter->setName(value.toString());
            break;

        case 1:  // Current value
            if (!value.isNull()) {
                auto list = parseVectorString(value.toString());
                parameter->setCurrentValue(list);
                parameter->setSavedValue(list);
            }
            break;

        default:
            return false;
    }

    emit dataChanged(index, index);
    return true;
}

/****************************************************************************/

QStringList ParameterModel::mimeTypes() const
{
    QStringList types;
    types << "text/uri-list";

#ifdef DEBUG_MODEL
    qDebug() << __func__ << types;
#endif

    return types;
}

/****************************************************************************/

bool ParameterModel::dropMimeData(
        const QMimeData *data,
        Qt::DropAction action,
        int row,
        int column,
        const QModelIndex &parent)
{
#ifdef DEBUG_MODEL
    qDebug() << __func__ << data << action << row << column << parent;
#else
    Q_UNUSED(column);
    Q_UNUSED(parent);
#endif

    if (!canDropMimeData(data, action, row, column, parent)) {
        QGuiApplication::restoreOverrideCursor();
        return false;
    }

    ParameterSetModel *parameterSetModel = mainWindow->getParameterSetModel();
    QList<QUrl> urls = data->urls();

    int insertPos;
    if (row == -1) {
        insertPos = parameters.size();
    }
    else {
        insertPos = row;
    }

    if ((not mainWindow->listViewParameterSets->
        selectionModel()->hasSelection() or parameterSetModel->isEmpty())
        and isEmpty()) {
        // create new set with new parameter model and return the row.
        int row = parameterSetModel->newParameterSet();
        // call add parameters on the newly created parameter model.
        parameterSetModel->getParameterSet(parameterSetModel->index(row))
            ->getParameterModel()->addParameters(urls, insertPos);
        // make sure that the created set is selected
        mainWindow->listViewParameterSets->setCurrentIndex(
            parameterSetModel->index(row));
        mainWindow->tableViewParameters->setModel(parameterSetModel->
            getParameterSet(parameterSetModel->index(row))->getParameterModel());
        QObject::connect(mainWindow->tableViewParameters->selectionModel(),
            &QItemSelectionModel::selectionChanged,
            mainWindow, &MainWindow::updateParameterButtons);

        // update parameter buttons after setting the models.
        mainWindow->updateParameterButtons();
        
        // keep the default model clean.
        clear();
    }
    else {
        addParameters(urls, insertPos);
    }
    
    emit layoutChanged();
    return true;
}

/****************************************************************************/

bool ParameterModel::canDropMimeData(
        const QMimeData *data,
        Qt::DropAction action,
        int row,
        int column,
        const QModelIndex &parent) const
{
    Q_UNUSED(row);
    Q_UNUSED(column);
    Q_UNUSED(parent);

    if (not data->hasFormat("text/uri-list")) {
        return false;
    }

    if (action != Qt::CopyAction) {
        return false;
    }

    bool wrong = false;

    DataNode *node;

    foreach (QUrl url, data->urls()) {
        node = mainWindow->getDataModel()->findDataNode(url);
        if (!node || node->getVariable().empty()) {
            mainWindow->statusBar()->showMessage(
                    tr("Please select variables only."),
                    5000);
            QGuiApplication::setOverrideCursor(Qt::ForbiddenCursor);
            wrong = true;
            break;
        }
        if (!node->getVariable().isWriteable()) {
            mainWindow->statusBar()->showMessage(
                    tr("Process variable %1 must be writable!")
                            .arg(node->getName()),
                    5000);
            QGuiApplication::setOverrideCursor(Qt::ForbiddenCursor);
            wrong = true;
            break;
        }
    }
    if (wrong) {
        QGuiApplication::restoreOverrideCursor();
        return false;
    }

    mainWindow->statusBar()->clearMessage();

    return true;
}

/****************************************************************************/

void ParameterModel::addParameters(QList<QUrl> urls, int insertPos)
{
    foreach (QUrl url, urls) {
        dataNode = mainWindow->getDataModel()->findDataNode(url);
        if (!dataNode) {
            mainWindow->statusBar()->showMessage(
                    tr("Url not found: %1").arg(url.toString()),
                    2000);
            continue;
        }

        if (getUrls().contains(url)) {
            // already existing
            continue;
        }

        Parameter *parameter = new Parameter(this);
        parameter->setUrl(url);

        parameter->connectParameters();

        beginInsertRows(QModelIndex(), insertPos, insertPos);
        parameters.insert(insertPos, parameter);
        endInsertRows();

        insertPos++;
    }

    mainWindow->updateParameterButtons();

    QGuiApplication::restoreOverrideCursor();
}

/****************************************************************************/

void ParameterModel::connectParameters()
{
    for (int row = 0; row < parameters.size(); row++) {
        parameters[row]->connectParameters();
    }
    QModelIndex i(createIndex(0, 0));
    QModelIndex j(createIndex(parameters.size(), 2));
    emit QAbstractItemModel::dataChanged(i, j);
}

/****************************************************************************/

void ParameterModel::saveParameters()
{
    QJsonObject layoutObject;

    QJsonArray parameterArray = toJson();

    layoutObject["parameters"] = parameterArray;

    QString creator;
#ifdef VERSION
    creator = QString::fromUtf8("Testmanager %1").arg(TM_STR(VERSION));
#else
    creator = QString::fromUtf8("Testmanager");
#endif

    layoutObject["creator"] = creator;

    layoutObject["creation date"] = QDateTime::currentDateTime().
        toString("yyyy-MM-dd hh:mm:ss");

    QString username;

#if defined(Q_OS_WIN)
    username = qgetenv("USERNAME").constData();
#elif defined(Q_OS_UNIX) || defined(Q_OS_LINUX)
    QDir homeDir = QDir::home();
    username = homeDir.dirName();
#else
    // For other platforms, not implemented yet.
    qWarning() << 
        "Username identification not implemented on this platform";
    username = "unknown";
#endif

    layoutObject["created by"] = username;

    layoutObject["description"] = description.isEmpty() ? 
        QString() : description;

    QJsonDocument saveDoc(layoutObject);

    QFile saveFile(parameterPath);
    if (!saveFile.open(QIODevice::WriteOnly)) {
        mainWindow->statusBar()->showMessage(
                tr("Failed to open %1").arg(parameterPath),
                2000);
        return;
    }

    saveFile.write(saveDoc.toJson());
    saveFile.close();
}

/***************************************************************************/

void ParameterModel::updateParameters()
{
    foreach (Parameter *parameter, parameters) {
        if (parameter->setSavedValue(parameter->getCurrentValue())) {
            notify(parameter);
        }
    }
}

/****************************************************************************/

void ParameterModel::replaceUrl(const QUrl &oldUrl, const QUrl &newUrl)
{
    for (int row = 0; row < parameters.size(); row++) {
        parameters[row]->replaceUrl(oldUrl, newUrl);
        QModelIndex i(createIndex(row, 2));
        QModelIndex j(createIndex(row, 2));
        emit QAbstractItemModel::dataChanged(i, j);
    }
}

/****************************************************************************/

QSet<QUrl> ParameterModel::getUrls() const
{
    QSet<QUrl> urls;

    for (int row = 0; row < parameters.size(); row++) {
        urls.insert(parameters[row]->getUrl());
    }

    return urls;
}

/****************************************************************************/

void ParameterModel::notify(Parameter *parameter)
{
    if (!parameters.contains(parameter)) {
        return;
    }

    int row = parameters.indexOf(parameter);
    emit dataChanged(index(row,1),
        index(row,1));
}

/****************************************************************************/

Parameter *ParameterModel::getParameter(const QModelIndex &index) const
{
    if (index.row() < 0 or index.row() >= parameters.size()) {
        return nullptr;
    }

    return parameters[index.row()];
}

/****************************************************************************/

void ParameterModel::remove(Parameter *parameter)
{
    for (int row = 0; row < parameters.size(); row++) {  // FIXME
        if (parameters[row] == parameter) {
            beginRemoveRows(QModelIndex(), row, row);
            parameters.removeAt(row);
            endRemoveRows();
            delete parameter;
            return;
        }
    }
}

/***************************************************************************
Private methods
***************************************************************************/

QString ParameterModel::buildVectorString(const QVector<double> &values)
{
    int nelem = values.count();

    QString output;
    QLocale locale;
    for (int i = 0; i < nelem; i++) {
        // Format each number using the current locale
        output.append(locale.toString(values.at(i)));
        if (i != nelem - 1) {
            output.append(" ");
        }
    }

    return output;
}

/***************************************************************************/

QVector<double> ParameterModel::parseVectorString(const QString &input)
{
    // remove trailing spaces (begin and end)
    QString trimmedInput(input.trimmed());

    // Split the list using ' ' (space) as the separator
    QStringList items(trimmedInput.split(' '));

    QLocale locale; // Use current locale

    QVector<double> result;

    for (const QString &item : items) {
        bool ok;
        // Parse the item using the current locale
        double value(locale.toDouble(item, &ok));
        if (not ok) {
            return QVector<double>();
        }
        result.append(value);
    }

    return result;
}

/****************************************************************************/

QString ParameterModel::dimensionString(PdCom::SizeInfo dim) const
{
    QString ret;

    for (unsigned int i = 0; i < dim.size(); i++) {
        if (!ret.isEmpty()) {
            ret += "×";
        }
        ret += QLocale().toString((qlonglong) dim[i]);
    }

    return ret;
}

/****************************************************************************/
