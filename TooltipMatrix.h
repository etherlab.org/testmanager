/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2019  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef TOOLTIPMATRIX_H
#define TOOLTIPMATRIX_H

#include <pdcom5/Variable.h>
#include <memory>

#include <QFrame>

/****************************************************************************/

class TooltipMatrix: public QFrame
{
    Q_OBJECT

    public:
        TooltipMatrix(QWidget *);
        ~TooltipMatrix();

        void setVariable(PdCom::Variable const&, double);
        void clearVariable();

    private:
        PdCom::Variable variable;
        bool dataPresent;
        int rowCount;
        int columnCount;
        QSize fieldSize;

        void resizeEvent(QResizeEvent *);
        void paintEvent(QPaintEvent *);

        struct Subscription;
        std::unique_ptr<Subscription> subscription;
};

/****************************************************************************/

#endif
