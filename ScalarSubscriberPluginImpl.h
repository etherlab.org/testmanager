/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2018  Florian Pose <fp@igh.de>
 *               2023  Bjarne von Horn <vh@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include "Plugin.h"
#include "DataModel.h"
#include "DataNode.h"
#include "DataSlot.h"
#include "SlotModel.h"

#include <QDebug>
#include <type_traits>

template<class = void>
struct DefaultPeriod
{
    static constexpr std::chrono::milliseconds Period {0};
};

namespace details {
template <class BaseClass, class PdWidget, bool hasClearData = false>
class SimpleSlotModelWithClear: public BaseClass
{
    public:
        using BaseClass::BaseClass;
};

template <class BaseClass, class PdWidget>
class SimpleSlotModelWithClear<BaseClass, PdWidget, true>: public BaseClass
{
    public:
        using BaseClass::BaseClass;
        void clearWidgetData() const override
        {
            static_cast<PdWidget *>(BaseClass::getWidget())->clearData();
        }
};

template <class T> using void_t = void;

template <class PdWidget, class = void> struct has_clear_data: std::false_type
{};

template <class PdWidget> struct has_clear_data<
        PdWidget,
        void_t<decltype(std::declval<PdWidget>().clearData())>>:
    std::true_type
{};
}  // namespace details

/****************************************************************************/

template <class PdWidget, bool expects_writeable> inline SlotModel *
ScalarSubscriberPlugin<PdWidget, expects_writeable>::createSlotModel(
        QWidget *widget,
        SlotModelCollection &parent,
        WidgetContainer &container) const
{
    auto pdwidget = qobject_cast<PdWidget *>(widget);
    if (!pdwidget) {
        return nullptr;
    }
    using BaseClass = typename std::conditional<
            expects_writeable,
            SimpleRWSlotModel,
            SimpleROSlotModel>::type;

    return new details::SimpleSlotModelWithClear<
            BaseClass,
            PdWidget,
            details::has_clear_data<PdWidget>::value>(
            parent,
            container,
            *pdwidget);
}

/****************************************************************************/

template <class PdWidget, bool expects_writeable> inline QWidget *
ScalarSubscriberPlugin<PdWidget, expects_writeable>::createWidget(
        QWidget *parent) const
{
    return new PdWidget(parent);
}

/****************************************************************************/
