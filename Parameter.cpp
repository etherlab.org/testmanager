/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2023  Daniel Ramirez <dr@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "Parameter.h"

#include "DataModel.h"
#include "DataNode.h"
#include "MainWindow.h"
#include "ParameterModel.h"
#include "ParameterTableModel.h"
#include "TabPage.h"

#include <QtPdCom1/Process.h>

#include <pdcom5/Exception.h>
#include <pdcom5/SizeTypeInfo.h>
#include <pdcom5/Subscriber.h>
#include <pdcom5/Subscription.h>
#include <pdcom5/Variable.h>

#include <QAbstractItemModel>
#include <QDebug>
#include <QJsonArray>
#include <QJsonObject>
#include <QVariant>
#include <QVector>
#include <QtGlobal>

#include <chrono>
#include <exception>
#include <vector>

#include <sys/types.h>

/*****************************************************************************
 * public
 ****************************************************************************/

Parameter::Parameter(ParameterModel *model):
    PdCom::Subscriber(PdCom::event_mode),
    model(model),
    mainWindow(model->getMainWindow()),
    dataNode(nullptr),
    dataPresent(false),
    savedDataPresent(false)
{}

/****************************************************************************/

Parameter::Parameter(ParameterTableModel *model):
    PdCom::Subscriber(PdCom::event_mode),
    model((ParameterModel *) model),
    mainWindow(model->getMainWindow()),
    dataNode(nullptr),
    dataPresent(false),
    savedDataPresent(false)
{}

/****************************************************************************/

Parameter::~Parameter()
{
}

/****************************************************************************/

QIcon Parameter::getIcon() const
{
    if (dataNode) {
        return dataNode->getIcon();
    }
    else {
        return QIcon();
    }
}

/****************************************************************************/

void Parameter::fromJson(const QJsonValue &value)
{
    QJsonObject obj(value.toObject());

    if (obj.contains("name")) {
        name = obj["name"].toString();
    }
    else {
        name = QString();
    }

    url = obj["url"].toString();

    double scale{1.0};
    if (obj.contains("scale")) {
        auto scaleValue(obj["scale"]);
        if (not scaleValue.isDouble()) {
            throw(std::invalid_argument(
                    "scale must be a scalar double value."));
        }

        scale = obj["scale"].toDouble();
        if (scale == 0.0) {
            throw(std::invalid_argument("scale must not be zero"));
        }
    }

    double offset{0.0};
    if (obj.contains("offset")) {
        auto offsetValue(obj["offset"]);
        if (not offsetValue.isDouble()) {
            throw(std::invalid_argument(
                    "offset must be a scalar double value."));
        }

        offset = obj["offset"].toDouble();
    }

    savedItems = QVector<double>();
    savedDataPresent = false;

    if (obj.contains("value")) {
        auto value(obj["value"]);
        if (value.isArray()) {
            auto array(value.toArray());
            for (int i = 0; i < array.count(); i++) {
                if (not array.at(i).isDouble()) {
                    throw(std::invalid_argument(
                                "value must be a scalar"
                                " or array of doubles."));
                }
                savedItems.append((array.at(i).toDouble() - offset) / scale);
            }
            savedDataPresent = true;
        }
        else if (value.isDouble()) {
            savedItems.append((value.toDouble() - offset) / scale);
            savedDataPresent = true;
        }
        else {
            throw(std::invalid_argument(
                        "value must be a scalar or array of doubles."));
        }
    }
}

/****************************************************************************/

QJsonObject Parameter::toJson(bool withValue) const
{
    QJsonObject obj;

    if (not name.isEmpty()) {
        obj["name"] = name;
    }

    obj["url"] = url.toString();

    if (withValue) {
        if (savedItems.count() == 1) {
            obj["value"] = savedItems.at(0);
        }
        else {
            QJsonArray valueArray;
            foreach (double v, savedItems) {
                valueArray.append(v);
            }
            obj["value"] = valueArray;
        }
    }

    return obj;
}

/****************************************************************************/

void Parameter::replaceUrl(const QUrl &oldUrl, const QUrl &newUrl)
{
    QUrl dataSourceUrl(url.adjusted(
            QUrl::RemovePassword | QUrl::RemovePath | QUrl::RemoveQuery
            | QUrl::RemoveFragment));
    if (dataSourceUrl != oldUrl) {
        return;
    }

    url.setScheme(newUrl.scheme());
    url.setAuthority(newUrl.authority());  // user, pass, host, port
}

/****************************************************************************/

bool Parameter::setSavedValue(const QVector<double> &values)
{
    if (pv.empty() or not dataPresent) {
        return false;
    }

    int nelem(pv.getSizeInfo().totalElements());
    if (values.size() != nelem) {
        return false;
    }

    savedItems = values;
    savedDataPresent = true;
    model->notify(this);
    return true;
}

/***************************************************************************/

bool Parameter::setCurrentValue(const QVector<double> &value)
{
    if (pv.empty() or not dataPresent) {
        return false;
    }

    int nelem(pv.getSizeInfo().totalElements());
    if (nelem != value.size()) {
        return false;
    }

    pv.setValue(value.constData(),
            PdCom::details::TypeInfoTraits<double>::type_info.type,
            nelem);
    return true;
}

/****************************************************************************/

void Parameter::connectParameters()
{
    DataModel *dataModel(mainWindow->getDataModel());
    dataNode = dataModel->findDataNode(url);

    if (dataNode) {
        try {
            // get variable from data node based on url.
            pv = dataNode->getVariable();
            subscription = PdCom::Subscription(*this, pv);
        }
        catch (PdCom::Exception &e) {
            QString msg(tr("Fetching variable failed: %1").arg(e.what()));
            mainWindow->statusBar()->showMessage(msg, 5000);
            qWarning() << msg;
            dataNode = nullptr;
            return;
        }

        try {
            subscription.poll();
        }
        catch (PdCom::Exception &e) {
            QString msg(tr("Parameter poll failed: %1").arg(e.what()));
            mainWindow->statusBar()->showMessage(msg, 5000);
            qWarning() << msg;
            dataNode = nullptr;
            return;
        }
    }
    else {
        QString msg(tr("URL not found: %1").arg(url.toString()));
        mainWindow->statusBar()->showMessage(msg, 5000);
        qWarning() << msg;
    }
}

/*****************************************************************************
 * private
 ****************************************************************************/

void Parameter::newValues(std::chrono::nanoseconds)
{
    auto pv(getVariable());
    auto nelem(pv->getSizeInfo().totalElements());

    QVector<double> values(nelem);
    PdCom::details::copyData(
            values.data(),
            PdCom::details::TypeInfoTraits<double>::type_info.type,
            subscription.getData(),
            pv->getTypeInfo().type,
            nelem);
    currentItems = values;
    dataPresent = true;
    model->notify(this);
}

/****************************************************************************/

void Parameter::stateChanged(const PdCom::Subscription &sub)
{
#ifdef DEBUG_PARAMETER
    qDebug() << __func__ << url << (int) sub.getState();
#endif
    if (sub.getState() != PdCom::Subscription::State::Active) {
        dataPresent = false;
        model->notify(this);
    }
}

/****************************************************************************/
