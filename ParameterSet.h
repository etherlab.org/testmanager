/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2023  Daniel Ramirez <dr@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PARAMETER_SET_H
#define PARAMETER_SET_H

#include <QObject>

class ParameterSetModel;
class ParameterModel;
class DataNode;
class DataModel;

/****************************************************************************/

class ParameterSet:
    public QObject
{
    Q_OBJECT

    public:
        ParameterSet(ParameterSetModel *);
        virtual ~ParameterSet();

        ParameterModel *getParameterModel() const { return parameterModel; }
        void setParameterModel(ParameterModel *);

        const QString &getName() const { return name; }
        void setName(const QString &name) { this->name = name; }
        void fromJson(const QJsonValue &);
        QJsonValue toJson() const;

        void appendDataSources(DataModel *) const;

        void connectSets();

    private:
        ParameterSetModel * const model;
        ParameterModel * parameterModel;
        QString name;

        ParameterSet();
};

/****************************************************************************/

#endif
