/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2023  Daniel Ramirez <dr@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PARAMETER_H
#define PARAMETER_H

#include <pdcom5/Subscriber.h>
#include <pdcom5/Subscription.h>
#include <pdcom5/Variable.h>

#include <QIcon>
#include <QUrl>
#include <QVariant>

#include <chrono>

class ParameterModel;
class ParameterTableModel;
class DataNode;
class MainWindow;

/****************************************************************************/

class Parameter: public QObject, public PdCom::Subscriber
{
        Q_OBJECT

    public:
        Parameter(ParameterModel *);
        Parameter(ParameterTableModel *);
        virtual ~Parameter();

        QString getName() const { return name; }
        void setName(QString newName) { name = newName; }
        QUrl getUrl() const { return url; }
        void setUrl(QUrl newUrl) { url = newUrl; }

        QIcon getIcon() const;

        void fromJson(const QJsonValue &);
        QJsonObject toJson(bool) const;

        void replaceUrl(const QUrl &, const QUrl &);

        PdCom::Subscription *getSubscription() { return &subscription; }
        PdCom::Variable *getVariable() { return &pv; }

        bool hasData() const { return dataPresent; }
        QVector<double> getCurrentValue() const { return currentItems; }
        bool setCurrentValue(const QVector<double> &);

        bool hasSavedData() const { return savedDataPresent; }
        const QVector<double> &getSavedValue() const { return savedItems; }
        bool setSavedValue(const QVector<double> &);

        void connectParameters();

    private:
        QUrl url;
        QString name;
        ParameterModel *const model;
        MainWindow *const mainWindow;
        DataNode *dataNode;
        PdCom::Subscription subscription;
        PdCom::Variable pv;
        QVector<double> currentItems;
        bool dataPresent;
        QVector<double> savedItems;
        bool savedDataPresent;

        // virtual from PdCom::Subscriber
        void newValues(std::chrono::nanoseconds) override;
        void stateChanged(const PdCom::Subscription &) override;

        Parameter();
};

/****************************************************************************/

#endif
