/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2018  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef TABPAGE_H
#define TABPAGE_H

/****************************************************************************/

#include "Plugin.h"

#include <QFrame>
#include <QRubberBand>

class MainWindow;

/****************************************************************************/

class TabPage:
    public QFrame
{
    Q_OBJECT

    public:
        TabPage(MainWindow *, QWidget *);

        MainWindow *getMainWindow() const { return mainWindow; }

        void editModeChanged();

        void read(const QJsonObject &);
        void write(QJsonObject &) const;

        void addContainerArray(const QJsonArray &, bool);

        void connectDataSlots() const;
        void replaceUrl(const QUrl &, const QUrl &) const;

        void updateMinimumSize();

        QString getTabName();

    protected:
        void contextMenuEvent(QContextMenuEvent *);
        void mousePressEvent(QMouseEvent *);
        void mouseMoveEvent(QMouseEvent *);
        void mouseReleaseEvent(QMouseEvent *);
        void keyPressEvent(QKeyEvent *);
        void paintEvent(QPaintEvent *);
        void moveSelected(QKeyEvent *);
        void changeOrder(QKeyEvent *);

    private:
        MainWindow * const mainWindow;
        QPoint createPos;
        QRubberBand rubberBand;
        QPoint rubberOrigin;

        TabPage();

    private slots:
        void createWidget();
        void editStyleSheet();
        void paste();
};

/****************************************************************************/

#endif
