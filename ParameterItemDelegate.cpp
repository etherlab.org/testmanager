/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2018  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "ParameterItemDelegate.h"

#include <QDebug>
#include <QColorDialog>
#include <QApplication>

/****************************************************************************/

ParameterItemDelegate::ParameterItemDelegate(QObject *parent):
    QStyledItemDelegate(parent)
{
}

/****************************************************************************/

void ParameterItemDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const {
        if (index.column() == 2) { // Assuming column 2 contains the path
            QString fullPath = index.data(Qt::EditRole).toString();
            QIcon icon = index.data(Qt::DecorationRole).value<QIcon>(); // Obtain the icon

            QRect rect = option.rect;

            // Calculate the space available for text (considering icon width)
            int availableWidth = rect.width() - iconWidth - 10;

            // Calculate the text size
            QFontMetrics fm(option.font);
            QString text = fm.elidedText(fullPath, Qt::ElideMiddle, availableWidth);

            // Calculate vertical position for centering the icon
            int verticalCenter = rect.top() + (rect.height() - iconHeight) / 2;
            
            // Draw the icon
            if (!icon.isNull()) {
                QRect iconRect = rect;
                iconRect.setWidth(iconWidth);
                iconRect.setHeight(iconHeight); // You may need to adjust the height
                iconRect.moveTop(verticalCenter); // Center the icon vertically
                iconRect.moveLeft(rect.left() + leftMargin);
                icon.paint(painter, iconRect, Qt::AlignLeft);
            }

            // Draw the text
            QStyleOptionViewItem newOption = option;
            newOption.displayAlignment = Qt::AlignLeft | Qt::AlignVCenter;
            newOption.text = text;
            newOption.rect.setLeft(rect.left() + iconWidth + spaceIconText);
            QApplication::style()->drawItemText(painter, newOption.rect, newOption.displayAlignment, newOption.palette, true, text);
        } else {
            QStyledItemDelegate::paint(painter, option, index);
        }
    }

/****************************************************************************/
