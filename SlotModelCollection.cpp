/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2018  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "SlotModelCollection.h"

#include <QDebug>
#include <QMimeData>
#include "DataModel.h"
#include "MainWindow.h"
#include "SelectorDialog.h"
#include "SlotModel.h"

/****************************************************************************/

void SlotModelCollection::SlotModelDeleter::operator()(SlotModel *sm) const
{
    auto &model_collection =
            *static_cast<SlotModelCollection *>(sm->getParentNode());
    const auto index = model_collection.getChildIndex(sm);
    model_collection.deleteRecursive(sm);
    model_collection.beginRemoveRows(
            model_collection.parent(sm->getIndex()),
            index,
            index);
    model_collection.removeChild(index);
    model_collection.endRemoveRows();
}

/****************************************************************************/

SlotModelCollection::SlotModelCollection(
        const DataModel *dataModel,
        MainWindow *mainWindow):
    SlotNode(NULL),
    dataModel(dataModel),
    mainWindow(mainWindow)
{}

/****************************************************************************/

SlotModelCollection::~SlotModelCollection()
{
    // erasing our children before dtor of SlotNode does it
    // because ~SlotNode() will try to call beginRemoveRows
    // which does not go well because we're already gone
    clear();
}

/****************************************************************************/

void SlotModelCollection::clear()
{
    beginResetModel();
    clearChildren();
    endResetModel();
}


/****************************************************************************/

QStringList SlotModelCollection::mimeTypes() const
{
    return {QString {"text/uri-list"}};
}

/****************************************************************************/

bool SlotModelCollection::canDropMimeData(
        const QMimeData *data,
        Qt::DropAction action,
        int row,
        int column,
        const QModelIndex &parent) const
{
    QString err;
    QList<DataNode *> nodes;
    if (parseMimeData(data, action, row, column, parent, err, nodes)
        != AcceptedDataConnectionModes::None) {
        mainWindow->statusbar->showMessage(
                tr("Drop to establish connection."));
        return true;
    }
    mainWindow->statusbar->showMessage(err, 2000);
    return false;
}

/****************************************************************************/

bool SlotModelCollection::dropMimeData(
        const QMimeData *data,
        Qt::DropAction action,
        int row,
        int column,
        const QModelIndex &parent)
{
    QString err;
    QList<DataNode *> nodes;
    const bool replace = action == Qt::MoveAction;
    const auto mode =
            parseMimeData(data, action, row, column, parent, err, nodes);
    if (mode == AcceptedDataConnectionModes::None) {
        mainWindow->statusbar->showMessage(err, 2000);
        return false;
    }

    SlotNode *slotNode = this;
    if (parent.isValid()) {
        slotNode = reinterpret_cast<SlotNode *>(parent.internalPointer());
    }

    if (SelectorDialog::needsToShow(nodes, mode)) {
        auto dialog = new SelectorDialog(nodes, mode, mainWindow);
        auto mw = mainWindow;

        connect(dialog, &QDialog::finished, dialog, &QObject::deleteLater);
        connect(dialog,
                &QDialog::accepted,
                this,
                [replace, mw, row, dialog, slotNode]() {
                    QString err;

                    if (slotNode->insertDataNodes(
                                dialog->getNodes(),
                                dialog->getSelector(),
                                row,
                                err,
                                replace)) {
                        mw->statusBar()->clearMessage();
                    }
                    else {
                        mw->statusbar->showMessage(err, 2000);
                    }
                });
        dialog->open();

        return true;
    }

    if (slotNode->insertDataNodes(nodes, {}, row, err, replace)) {
        mainWindow->statusbar->clearMessage();
        return true;
    }
    mainWindow->statusbar->showMessage(err, 2000);
    return false;
}

/****************************************************************************/

SlotNode::AcceptedDataConnectionModes SlotModelCollection::parseMimeData(
        const QMimeData *data,
        Qt::DropAction action,
        int &row,
        int column,
        const QModelIndex &parent,
        QString &err,
        QList<DataNode *> &nodes) const
{
    const bool replace = action == Qt::MoveAction;
    if (!parent.isValid() && row == -1 && column == -1) {
        // empty area
        err = tr("Please drop Variables or Sources onto Widgets or its "
                 "categories in the Connection Editor.");
        return AcceptedDataConnectionModes::None;
    }
    if (!data->hasUrls()) {
        err = tr("Process Variables or Data Sources required.");
        return AcceptedDataConnectionModes::None;
    }

    for (const auto &url : data->urls()) {
        auto *node = dataModel->findDataNode(url);
        if (node) {
            nodes.append(node);
        }
        else {
            err = tr("Url not found: %1").arg(url.toDisplayString());
            return AcceptedDataConnectionModes::None;
        }
    }

    if (column != -1 && column != 0) {
        err = tr("Please use the first column for dragging.");
        return AcceptedDataConnectionModes::None;
    }

    const SlotNode *slotNode = this;
    if (parent.isValid()) {
        slotNode =
                reinterpret_cast<const SlotNode *>(parent.internalPointer());
    }

    return slotNode->canInsertDataNodes(nodes, row, err, replace);
}

/****************************************************************************/

void SlotModelCollection::deleteRecursive(SlotNode *node)
{
    const int count = node->getChildCount();
    if (count == 0) {
        // leaf, nothing to do.
        return;
    }
    for (int i = 0; i < count; ++i) {
        deleteRecursive(node->getChildNode(i));
    }
    beginRemoveRows(node->getIndex(), 0, count - 1);
    node->clearChildren();
    endRemoveRows();
}

/****************************************************************************/

void SlotModelCollection::remove(const QModelIndex &index)
{
    QModelIndex parent(index.parent());
    if (parent.isValid()) {
        SlotNode *parentNode = (SlotNode *) parent.internalPointer();
        if (parentNode) {
            beginRemoveRows(parent, index.row(), index.row());
            parentNode->removeChild(index.row());
            endRemoveRows();
        }
    }
    else {
        beginRemoveRows(parent, index.row(), index.row());
        removeChild(index.row());
        endRemoveRows();
    }
}

/****************************************************************************/

bool SlotModelCollection::removeRows(
        int row,
        int count,
        const QModelIndex &parent)
{
    SlotNode *np = this;
    if (parent.isValid()) {
        np = reinterpret_cast<SlotNode *>(parent.internalPointer());
    }

    if (row < 0 || count <= 0 || (row + count) > np->getChildCount()) {
        return false;
    }

    for (int i = row; i < (row + count); ++i) {
        if (!np->getChildNode(i)->supportsRemovingByUser()) {
            return false;
        }
    }

    beginRemoveRows(parent, row, row + count - 1);
    // removing in reverse direction
    for (int i = row + count - 1; i >= row; --i) {
        np->removeChild(i);
    }
    endRemoveRows();
    return true;
}

/****************************************************************************/

QModelIndex SlotModelCollection::index(
        int row,
        int column,
        const QModelIndex &parent) const
{
    SlotNode *node = NULL;

    if (parent.isValid()) {
        SlotNode *parentNode = (SlotNode *) parent.internalPointer();
        if (parentNode) {
            node = parentNode->getChildNode(row);
        }
    }
    else {
        node = getChildNode(row);
    }

    QModelIndex ret;

    if (node) {
        ret = createIndex(row, column, node);
    }

#ifdef DEBUG_MODEL
    qDebug() << __func__ << row << column << parent << ret;
#endif

    return ret;
}

/****************************************************************************/

QModelIndex SlotModelCollection::parent(const QModelIndex &index) const
{
    QModelIndex ret;

    if (index.isValid()) {
        SlotNode *n = (SlotNode *) index.internalPointer();
        SlotNode *p = n->getParentNode();
        // if p == this, parent is the root of the model
        // which also has a default-constructed QModelIndex.
        if (p && p != this) {
            SlotNode *pp = p->getParentNode();  // grandparent for row
            int row;
            if (pp) {
                row = pp->getChildIndex(p);
            }
            else {
                row = getChildIndex(p);
            }
            ret = createIndex(row, 0, p);
        }
    }

#ifdef DEBUG_MODEL
    qDebug() << __func__ << index << ret;
#endif

    return ret;
}

/****************************************************************************/

int SlotModelCollection::rowCount(const QModelIndex &parent) const
{
    int ret = 0;

    if (parent.isValid()) {
        SlotNode *parentNode = (SlotNode *) parent.internalPointer();
        if (parentNode) {
            ret = parentNode->getChildCount();
        }
        else {
#ifdef DEBUG_MODEL
            qDebug() << __func__ << parent << "inval";
#endif
        }
    }
    else {
        ret = getChildCount();
    }

#ifdef DEBUG_MODEL
    qDebug() << __func__ << parent << ret;
#endif

    return ret;
}

/****************************************************************************/

int SlotModelCollection::columnCount(const QModelIndex &) const
{
    return _columnCount;
}

/****************************************************************************/

QVariant SlotModelCollection::data(const QModelIndex &index, int role) const
{
    QVariant ret;

    if (index.isValid()) {
        SlotNode *node = (SlotNode *) index.internalPointer();
        ret = node->nodeData(role, index.column(), dataModel);
    }

#ifdef DEBUG_MODEL
    if (role <= 1) {
        qDebug() << __func__ << index << role << ret;
    }
#endif

    return ret;
}

/****************************************************************************/

QVariant SlotModelCollection::headerData(
        int section,
        Qt::Orientation orientation,
        int role) const
{
    QVariant ret;

    if (orientation == Qt::Horizontal && role == Qt::DisplayRole) {
        switch (section) {
            case UrlColumn:
                ret = tr("Variable");
                break;
            case AliasColumn:
                ret = tr("Alias");
                break;
            case SelectorColumn:
                ret = tr("Selection");
                break;
            case PeriodColumn:
                ret = tr("Period [s]");
                break;
            case ScaleColumn:
                ret = tr("Scale");
                break;
            case OffsetColumn:
                ret = tr("Offset");
                break;
            case TauColumn:
                ret = tr("LPF [s]");
                break;
            case ColorColumn:
                ret = tr("Color");
                break;
            default:
                break;
        }
    }

    if (orientation == Qt::Horizontal &&
            role == Qt::ToolTipRole &&
            section == SelectorColumn) {
        ret = tr("Field selection from multidimensional Variables.");
    }

    return ret;
}

/****************************************************************************/

Qt::ItemFlags SlotModelCollection::flags(const QModelIndex &index) const
{
    Qt::ItemFlags ret = QAbstractItemModel::flags(index);

    if (index.isValid()) {
        SlotNode *node = (SlotNode *) index.internalPointer();
        node->nodeFlags(ret, index.column());
        if (node->supportsRemovingByUser()) {
            ret |= Qt::ItemIsSelectable;
        } else {
            ret &= ~Qt::ItemIsSelectable;
        }
    }

#ifdef DEBUG_MODEL
    qDebug() << __func__ << index << ret;
#endif

    return ret;
}

/****************************************************************************/

bool SlotModelCollection::setData(
        const QModelIndex &index,
        const QVariant &value,
        int role)
{
    bool ret = false;

    if (index.isValid() && role == Qt::EditRole) {
        SlotNode *slotNode = (SlotNode *) index.internalPointer();
        ret = slotNode->nodeSetData(index.column(), value);
        if (ret) {
            emit dataChanged(index, index, {Qt::DisplayRole, Qt::EditRole});
        }
    }

    return ret;
}

/****************************************************************************/
