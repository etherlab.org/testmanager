#!/bin/bash

BUILD_DIR=build-leap
REPO=$(realpath .)

docker run -it --rm \
    -v $REPO:$REPO \
    --workdir $REPO/$BUILD_DIR \
    -v /tmp/.X11-unix:/tmp/.X11-unix:rw \
    --user $(id -u):$(id -g) \
    -e DISPLAY=$DISPLAY \
    -e QT_GRAPHICSSYSTEM=native \
    -e XDG_CONFIG_HOME=$REPO/xdg \
    docker:5000/testmanager-leap \
    bash -i
