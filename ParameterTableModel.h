/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2023 Daniel Ramirez <dr@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/
#ifndef PARAMETER_TABLE_MODEL_H
#define PARAMETER_TABLE_MODEL_H

/****************************************************************************/

#include "Parameter.h"
#include "ParameterModel.h"

#include <QAbstractTableModel>
#include <QJsonArray>

/****************************************************************************/

class MainWindow;

/****************************************************************************/

class ParameterTableModel:
    public ParameterModel
{
    Q_OBJECT

    public:
        ParameterTableModel(MainWindow *);
        ~ParameterTableModel();

        int columnCount(const QModelIndex &) const override;
        QVariant data(const QModelIndex &, int) const override;
        QVariant headerData(int, Qt::Orientation, int) const override;
        Qt::ItemFlags flags(const QModelIndex &) const override;
        bool setData(const QModelIndex &, const QVariant &, int) override;

        void applyParameters();
        void saveParametersAs();
        void loadParameters();

        void remove(Parameter *);

    private:
        ParameterTableModel();

        void fileError(const QString &);
};

/****************************************************************************/

#endif
