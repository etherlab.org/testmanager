/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2018  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "ui_ReplaceDialog.h"

#pragma once

#include <QDialog>
#include <QSslCertificate>
#include <QUrl>

/****************************************************************************/

class DataSource;
class DataModel;

/****************************************************************************/

class ConnectDialog:
    public QDialog,
    public Ui::ReplaceDialog
{
    Q_OBJECT

    public:
        ConnectDialog(DataModel *, QWidget *parent = 0);
        ~ConnectDialog();

        DataSource *adoptSource();

        QUrl getUrl() const
        {
            return url;
        }

    private slots:
#if QT_VERSION < 0x050000
        void on_addressComboBox_textChanged();
#else
        void on_addressComboBox_currentTextChanged();
#endif
        void on_okButton_clicked();
        void on_cancelButton_clicked();
        void connected();
        void error();
        void credentialsNeeded();
        void onLoginFailed();
        void on_cbUseTls_stateChanged(int state);
        void on_cbTlsMode_currentIndexChanged(int index);
        void on_addressComboBox_currentTextChanged(const QString &text);
        void on_pbLoadCAs_clicked();

    private:
        void setWidgetsEnabled(bool enabled);
        bool tryLoadCustomCA(QString path);

        DataSource *dataSource;
        QStringList recentSources;
        QUrl url;
        QSslCertificate customCA;
        QString customCaPath;
        bool loginDialogShowed;

        virtual DataSource* createDataSource() const;

    protected:
        DataModel * const dataModel;
};

/****************************************************************************/
