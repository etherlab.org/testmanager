/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2018  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "SlotModel.h"

#include "DataNode.h"
#include "DataModel.h"
#include "DataSlot.h"
#include "Plugin.h"
#include "Selector.h"
#include "SlotModelCollection.h"
#include "TabPage.h"
#include "WidgetContainer.h"

#include <QColorDialog>
#include <QMap>
#include <QPersistentModelIndex>
#include <QtPdCom1/ScalarSubscriber.h>

/****************************************************************************/

/* Default colors, taken from Ethan Schoonover's wonderful color scheme
 * "Solarized". See http://ethanschoonover.com/solarized
 */
const QColor SlotModel::colorList[] = {
    QColor( 38, 139, 210), // blue
    QColor(220,  50,  47), // red
    QColor(133, 153,   0), // green
    QColor(181, 137,   0), // yellow
    QColor(211,  54, 130), // magenta
    QColor(108, 113, 196), // violet (close to blue)
    QColor( 42, 161, 152), // cyan
    QColor(203,  75,  22), // orange (close to red)
};

#define NUM_COLORS (sizeof(colorList) / sizeof(QColor))

SlotModel::SlotModel(SlotModelCollection &parent,
                     WidgetContainer& container):
    SlotNode(&parent),
    container(&container)
{
    connect(container.getWidget(), &QObject::objectNameChanged,
        &getRoot(), [this](){
            emit this->getRoot().dataChanged(this->getIndex(), this->getIndex(), {Qt::DisplayRole});
        });
}

void SlotModel::initCustomColors()
{
    for (unsigned int i = 0;
            i < NUM_COLORS && i < (unsigned int) QColorDialog::customCount();
            i++) {
        QColorDialog::setCustomColor(i, colorList[i]);
    }
}

/****************************************************************************/

QColor SlotModel::nextColor() const
{
    QMap<QRgb, unsigned int> colorUsage;
    for (unsigned int i = 0; i < NUM_COLORS; i++) {
        colorUsage[colorList[i].rgba()] = 0;
    }

    countColors(colorUsage);

    unsigned int min = 0xffffffff;
    foreach (QRgb rgb, colorUsage.keys()) {
        if (colorUsage[rgb] < min) {
            min = colorUsage[rgb];
        }
    }

    for (unsigned int i = 0; i < NUM_COLORS; i++) {
        if (colorUsage[colorList[i].rgba()] == min) {
            return colorList[i];
        }
    }

    return QColor(); // never reached
}

/****************************************************************************/

QList<const DataSlot *> SlotModel::getDataSlots() const
{
    QList<const DataSlot *> slotList;
    collectDataSlots(slotList);
    return slotList;
}

/****************************************************************************/

QSet<QUrl> SlotModel::getUrls() const
{
    QSet<QUrl> urls;
    collectUrls(urls);
    return urls;
}

/****************************************************************************/

QVariant SlotModel::nodeData(int role, int section, const DataModel *) const
{
    if (section == UrlColumn) {
        switch (role) {
            case Qt::DisplayRole:
                {
                    const auto name = container->getWidget()->objectName();
                    if (!name.isEmpty())
                        return name;
                    return tr("<%1 Widget at Tab \"%2\">").arg(
                            container->getPlugin()->name()).arg(
                                    container->getTabPage()->getTabName());
                }
            case Qt::EditRole:
                return container->getWidget()->objectName();
            case Qt::DecorationRole:
                return container->getPlugin()->icon();
        }
    }
    return {};
}

/****************************************************************************/

void SlotModel::nodeFlags(Qt::ItemFlags &flags, int section) const
{
    if (section == UrlColumn)
        flags |= Qt::ItemIsEditable;
}

/****************************************************************************/

bool SlotModel::nodeSetData(int section, const QVariant &data)
{
    if (section != UrlColumn)
        return false;
    container->setProperty("objectName", data);
    return true;
}

QWidget *SlotModel::getWidget() const
{
    return container->getWidget();
}

/****************************************************************************/

template<class BaseClass>
void SimpleSlotModelBase<BaseClass>::fromJson(const QJsonValue &val)
{
    auto array = val.toArray();
    if (array.size() == 1) {
        auto node = new ScalarSubscriberDataSlot(this, subscriber);
        node->fromJson(array[0]);
    }
}

/****************************************************************************/

template<class BaseClass>
SlotModel::AcceptedDataConnectionModes
SimpleSlotModelBase<BaseClass>::canInsertDataNodes(
        QList<DataNode *> const &nodes,
        int,
        QString &err,
        bool replace) const
{
    if (!replace && BaseClass::getChildCount() > 0) {
        err = QObject::tr("Already connected. Press Shift to replace.");
        return SlotModel::AcceptedDataConnectionModes::None;
    }
    if (nodes.size() != 1) {
        err = QObject::tr("Exactly one connection possible.");
        return SlotModel::AcceptedDataConnectionModes::None;
    }
    const auto var = nodes[0]->getVariable();
    if (var.empty()) {
        err = QObject::tr("Please select a Variable");
        return SlotModel::AcceptedDataConnectionModes::None;
    }
    return SlotModel::AcceptedDataConnectionModes::Scalar;
}

/****************************************************************************/

template<class BaseClass>
bool SimpleSlotModelBase<BaseClass>::insertDataNodes(
        QList<DataNode *> const &nodes,
        const Selector &selector,
        int row,
        QString &err,
        bool replace)
{
    if (canInsertDataNodes(nodes, row, err, replace)
        != SlotModel::AcceptedDataConnectionModes::Scalar) {
        return false;
    }

    if (replace)
        clear();

    const auto var = nodes[0]->getVariable();
    QtPdCom::Transmission transmission {std::chrono::milliseconds(100)};
    if (var.isWriteable()) {
        // assuming variable is parameter which does not support period
        transmission = QtPdCom::event_mode;
    }

    auto child = new ScalarSubscriberDataSlot(this, subscriber);
    child->selector = selector;
    child->url = nodes[0]->nodeUrl();
    child->period = transmission;
    try {
        subscriber.setVariable(
                var,
                selector.toPdComSelector(),
                transmission,
                child->scale,
                child->offset,
                child->tau);
    }
    catch (const PdCom::Exception &e) {
        SlotModelCollection& root = SlotNode::getRoot();
        root.remove(root.index(0, 0, SlotNode::getIndex()));
        err = QObject::tr("Error occured during subscribing to a variable: ")
                + e.what();
        return false;
    }
    return true;
}

/****************************************************************************/

template <class BaseClass> void SimpleSlotModelBase<BaseClass>::clear()
{
    auto& root = SlotNode::getRoot();
    const auto count = SlotNode::getChildCount();
    if (count > 0) {
        root.removeRows(0, count, SlotNode::getIndex());
    }
}

/****************************************************************************/

template <class BaseClass> void SimpleSlotModelBase<BaseClass>::nodeFlags(
        Qt::ItemFlags &flags,
        int section) const
{
    if (section == SlotNode::UrlColumn)
        flags |= Qt::ItemIsDropEnabled;
    BaseClass::nodeFlags(flags, section);
}

/****************************************************************************/

SlotModel::AcceptedDataConnectionModes SimpleRWSlotModel::canInsertDataNodes(
        QList<DataNode *> const &nodes,
        int row,
        QString &err,
        bool replace) const
{
    if (SimpleSlotModelBase::canInsertDataNodes(nodes, row, err, replace)
        != AcceptedDataConnectionModes::Scalar) {
        return AcceptedDataConnectionModes::None;
    }

    const auto var = nodes[0]->getVariable();
    if (!var.isWriteable()) {
        err = tr("Variable is not writeable!");
        return AcceptedDataConnectionModes::None;
    }
    return AcceptedDataConnectionModes::Scalar;
}

/****************************************************************************/

template class SimpleSlotModelBase<SlotModel>;
template class SimpleSlotModelBase<SlotNode>;

/****************************************************************************/
