/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2023 Daniel Ramirez <dr@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PARAMETER_SET_MODEL_H
#define PARAMETER_SET_MODEL_H

/****************************************************************************/

#include "ParameterSet.h"

#include <QAbstractListModel>
#include <QJsonArray>
#include <QDropEvent>

/****************************************************************************/

class MainWindow;
class DataModel;

/****************************************************************************/

class ParameterSetModel:
    public QAbstractListModel
{
    Q_OBJECT

    public:
        ParameterSetModel(MainWindow *);
        ~ParameterSetModel();

        MainWindow *getMainWindow() const { return mainWindow; }

        void clear();
        bool isEmpty() const { return parameterSets.isEmpty(); }

        void fromJson(const QJsonArray &);
        QJsonArray toJson() const;

        void appendDataSources(DataModel *) const;

        int rowCount(const QModelIndex & = QModelIndex()) const override;
        int columnCount(const QModelIndex &) const override;
        QVariant data(const QModelIndex &, int) const override;
        Qt::ItemFlags flags(const QModelIndex &) const override;
        bool setData(const QModelIndex &, const QVariant &, int) override;

        void connectSets();
        void replaceUrl(const QUrl &, const QUrl &);

        ParameterSet *getParameterSet(const QModelIndex &) const;

        void remove(ParameterSet *);
        int newParameterSet();

    private:
        QList<ParameterSet *> parameterSets;
        MainWindow * const mainWindow;
        int created;

        ParameterSetModel();
};

/****************************************************************************/

#endif
