/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2023  Bjarne von Horn <vh@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "SendBroadcastPlugin.h"

#include "DataModel.h"
#include "DataNode.h"
#include "DataSource.h"
#include "DataSlot.h"
#include "SlotModel.h"
#include "SlotModelCollection.h"

#include <QtPdWidgets2/SendBroadcastWidget.h>

#include <QJsonArray>
#include <QJsonObject>

namespace {
class BroadcastSlotModel;

class ProcessSlot: public BasicDataSlot
{
    public:
        ProcessSlot(BroadcastSlotModel &parent);

        ~ProcessSlot() { clear(); }

        bool supportsRemovingByUser() const { return true; }

        void clear();

        void appendDataSources(DataModel *) override;
        void disconnectDataSources() override;
};

class BroadcastSlotModel: public SlotModel
{
        Q_DECLARE_TR_FUNCTIONS(BroadcastSlotModel)

    public:
        BroadcastSlotModel(
                SlotModelCollection &parent,
                WidgetContainer &container,
                Pd::SendBroadcastWidget &widget):
            SlotModel(parent, container),
            widget(widget)
        {}

        void clear() override
        {
            const auto count = getChildCount();
            if (count > 0) {
                getRoot().removeRows(0, count, getIndex());
        }
        }

        AcceptedDataConnectionModes canInsertDataNodes(
                QList<DataNode *> const &nodes,
                int,
                QString &err,
                bool replace) const override
        {
            if (!replace && getChildCount() > 0) {
                err = tr("Already connected. Press Shift to replace.");
                return AcceptedDataConnectionModes::None;
            }
            if (nodes.size() != 1) {
                err = tr("Exactly one connection possible.");
                return AcceptedDataConnectionModes::None;
            }
            auto *dataSource = qobject_cast<DataSource *>(nodes[0]);
            if (!dataSource) {
                err = tr("Please select a Data Source");
                return AcceptedDataConnectionModes::None;
            }
            return AcceptedDataConnectionModes::Process;
        }

        bool insertDataNodes(
                QList<DataNode *> const &nodes,
                const Selector&,
                int row,
                QString &err,
                bool replace) override
        {
            if (canInsertDataNodes(nodes, row, err, replace)
                != AcceptedDataConnectionModes::Process) {
                return false;
            }

            const auto children = getChildCount();
            if (replace && children > 0) {
                getRoot().removeRows(0, children, getIndex());
            }

            auto child = new ProcessSlot(*this);
            child->url = nodes[0]->nodeUrl();
            auto source = qobject_cast<DataSource *>(nodes[0]);
            widget.setProcess(source->process.get());
            return true;
        }

        void nodeFlags(Qt::ItemFlags &flags, int section) const override
        {
            SlotModel::nodeFlags(flags, section);

            if (getChildCount() == 0 && section == UrlColumn) {
                flags |= Qt::ItemIsDropEnabled;
            }
        }

        void fromJson(const QJsonValue &val) override
        {
            const auto array = val.toArray();
            if (array.size() == 1) {
                auto child = new ProcessSlot(*this);
                child->fromJson(array[0]);
            }
        }

        Pd::SendBroadcastWidget &widget;
};

ProcessSlot::ProcessSlot(BroadcastSlotModel &parent):
    BasicDataSlot(&parent)
{}

void ProcessSlot::clear()
{
    auto &parent = *static_cast<BroadcastSlotModel *>(getParentNode());
    parent.widget.setProcess(nullptr);
}

void ProcessSlot::appendDataSources(DataModel *model)
{
    BasicDataSlot::appendDataSources(model);

    auto &parent = *static_cast<BroadcastSlotModel *>(getParentNode());
    auto sourceNode = model->findDataNode(url);
    if (auto dataSource = dynamic_cast<DataSource *>(sourceNode)) {
        parent.widget.setProcess(dataSource->process.get());
    }
}

void ProcessSlot::disconnectDataSources()
{
    auto &parent = *static_cast<BroadcastSlotModel *>(getParentNode());
    parent.widget.setProcess(nullptr);
}
}  // namespace

QString SendBroadcastPlugin::name() const
{
    return tr("Send Broadcast");
}

QIcon SendBroadcastPlugin::icon() const
{
    return QIcon(":/images/plugin-broadcast.svg");
}

SlotModel *SendBroadcastPlugin::createSlotModel(
        QWidget *widget,
        SlotModelCollection &parent,
        WidgetContainer &container) const
{
    auto *pdwidget = qobject_cast<Pd::SendBroadcastWidget *>(widget);
    if (pdwidget) {
        return new BroadcastSlotModel(parent, container, *pdwidget);
    }
    return nullptr;
}

QWidget *SendBroadcastPlugin::createWidget(QWidget *parent) const
{
    return new Pd::SendBroadcastWidget(parent);
}
