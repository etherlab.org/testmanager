/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2019  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "SvgPlugin.h"
#include "SvgPluginDialog.h"

#include "DataSlot.h"
#include "DataModel.h"
#include "DataNode.h"
#include "SlotModel.h"

#include <QtPdWidgets2/Svg.h>

#include <QDebug>
#include <QJsonObject>
#include <QJsonArray>

/****************************************************************************/

QString SvgPlugin::name() const
{
    return tr("Animated Vector Graphics");
}

/****************************************************************************/

QIcon SvgPlugin::icon() const
{
    return QIcon(":/images/plugin-text.svg");
}

/****************************************************************************/

void *SvgPlugin::createPrivateData(QWidget *widget) const
{
    Pd::Svg *svg = dynamic_cast<Pd::Svg *>(widget);
    if (!svg) {
        qWarning() << __func__ << "failed to cast";
        return NULL;
    }

    QList<Row> *rows = new QList<Row>();
    return rows;
}

/****************************************************************************/

void SvgPlugin::readPrivateData(
        const QJsonObject &json,
        QWidget *widget,
        void *data) const
{
    Pd::Svg *svg = dynamic_cast<Pd::Svg *>(widget);
    if (!svg) {
        qWarning() << __func__ << "failed to cast";
        return;
    }

    auto rows = static_cast<QList<Row> *>(data);
    rows->clear();

    auto listObj(json["attributeID"].toArray());

    foreach (QJsonValue value, listObj) {
        auto dict(value.toObject());
        QString idStr(dict["id"].toString());

        Row row;
        row.id = idStr;
        row.available = svg->existId(idStr);
        rows->append(row);
    }

    svg->setIdList(getIdList(*rows));
}

/****************************************************************************/

QJsonObject SvgPlugin::writePrivateData(QWidget *, const void *data) const
{
    auto rows = static_cast<const QList<Row> *>(data);
    QJsonArray idArray;

    foreach (Row row, *rows) {
        QJsonObject value;
        value["id"] = row.id;
        idArray.append(value);
    }

    QJsonObject ret;
    ret["attributeID"] = idArray;
    return ret;
}

/****************************************************************************/

void SvgPlugin::deletePrivateData(QWidget *widget, void *data) const
{
    Pd::Svg *svg = dynamic_cast<Pd::Svg *>(widget);
    if (svg) {
        svg->setIdList(QStringList());
    }

    auto rows = static_cast<QList<Row> *>(data);
    delete rows;
}

/****************************************************************************/

void SvgPlugin::openEditor(
        QWidget *widget,
        void *data,
        SlotModel *,
        QWidget *parent)
{
    auto rows = static_cast<QList<Row> *>(data);

    Pd::Svg *svg = dynamic_cast<Pd::Svg *>(widget);
    auto dialog = new SvgPluginDialog(*svg, *rows, parent);
    connect(dialog, &QDialog::finished, dialog, &QObject::deleteLater);
    dialog->open();
}

/****************************************************************************/

QWidget *SvgPlugin::createWidget(QWidget *parent) const
{
    return new Pd::Svg(parent);
}

/****************************************************************************/

/** Create a list with svg IDs.
 *  This method creates and returns a new list wich contains only the ID
 *  strings.
 */
QStringList SvgPlugin::getIdList(const QList<Row> &list)
{
    QStringList idList;
    for (int i = 0; i < list.length(); i++) {
        idList.append(list[i].id);
    }

    return idList;
}

/****************************************************************************/
