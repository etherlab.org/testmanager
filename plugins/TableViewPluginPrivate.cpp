/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2019-2023  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "TableViewPluginPrivate.h"

#include "SlotModelCollection.h"
#include "DataModel.h"
#include "DataNode.h"
#include <QtPdWidgets2/TableView.h>

#include <QJsonArray>
#include <QJsonObject>

/****************************************************************************/

TableViewSlotModel::TableViewSlotModel(
        SlotModelCollection &parent,
        WidgetContainer &container,
        Pd::TableView *widget):
    SlotModel(parent, container),
    model(new QtPdCom::TableModel(this)),
    widget(widget)
{
    new TableViewTableCategory(this, tr("Columns"));
    new TableViewVisibleCategory(this, tr("Number of Rows"));
    new TableViewHighlightCategory(this, tr("Highlighted row"));
    widget->setModel(model);
}

TableViewSlotModel::~TableViewSlotModel()
{
    widget->setModel(nullptr);
}

SlotNode::AcceptedDataConnectionModes TableViewSlotModel::canInsertDataNodes(
        QList<DataNode *> const &nodes,
        int row,
        QString &err,
        bool replace) const
{
    return getChildNode(0)->canInsertDataNodes(nodes, row, err, replace);
}

bool TableViewSlotModel::insertDataNodes(
        QList<DataNode *> const &nodes,
        const Selector &selector,
        int row,
        QString &err,
        bool replace)
{
    return getChildNode(0)->insertDataNodes(nodes, selector, row, err, replace);
}


void TableViewSlotModel::clear()
{
    static_cast<TableViewTableCategory *>(getChildNode(0))->clear();
    static_cast<TableViewVisibleCategory *>(getChildNode(1))->clear();
    static_cast<TableViewHighlightCategory *>(getChildNode(2))->clear();
}

void TableViewSlotModel::fromJson(const QJsonValue &value)
{
    const auto obj = value.toObject();
    getChildNode(0)->fromJson(obj["columns"]);
    getChildNode(1)->fromJson(obj["visibility"]);
    getChildNode(2)->fromJson(obj["highlighting"]);
}

QJsonValue TableViewSlotModel::toJson() const
{
    if (getChildCount() != 3)
        return {};
    QJsonObject ans;
    ans["columns"]      = getChildNode(0)->toJson();
    ans["visibility"]   = getChildNode(1)->toJson();
    ans["highlighting"] = getChildNode(2)->toJson();
    return ans;
}

/****************************************************************************/

void TableViewTableCategory::clear()
{
    auto &root = getRoot();
    for (int i = 0; i < getChildCount(); ++i) {
        auto *node = getChildNode(i);
        const auto count = node->getChildCount();
        if (count > 0) {
            root.removeRows(0, count, node->getIndex());
        }
    }
}

SlotNode::AcceptedDataConnectionModes
TableViewTableCategory::canInsertDataNodes(
        QList<DataNode *> const &nodes,
        int /* row */,
        QString &err,
        bool /* replace */) const
{
    for (const auto &node : nodes) {
        if (node->getVariable().empty()) {
            err = QObject::tr("Please select one or more variables.");
            return AcceptedDataConnectionModes::None;
        }
    }
    return AcceptedDataConnectionModes::VariableButNoSelector;
}

bool TableViewTableCategory::insertDataNodes(
        QList<DataNode *> const &nodes,
        const Selector &selector,
        int row,
        QString &err,
        bool replace)
{
    if (replace) {
        row = 0;
    }
    else if (row < 0) {
        row = getChildCount();
    }

    if (canInsertDataNodes(nodes, row, err, replace)
        != AcceptedDataConnectionModes::VariableButNoSelector) {
        return false;
    }

    const auto childs = getChildCount();
    if (replace && childs > 0) {
        getRoot().removeRows(0, childs, getIndex());
    }

    for (int i = 0; i < nodes.size(); ++i) {
        auto node = new TableViewColumnCategory(*this, row + i);
        node->insertDataNodes({nodes[i]}, selector, 0, err, false);
    }
    return true;
}

void TableViewTableCategory::nodeFlags(Qt::ItemFlags &flags, int section)
        const
{
    SlotNode::nodeFlags(flags, section);
    if (section == UrlColumn) {
        flags |= Qt::ItemIsDropEnabled;
    }
}

void TableViewTableCategory::fromJson(const QJsonValue &value)
{
    for (const auto& col : value.toArray())
    {
        auto node = new TableViewColumnCategory(*this, -1);
        node->fromJson(col);
    }
}

/****************************************************************************/

TableViewColumnSlot::~TableViewColumnSlot()
{
    disconnectDataSources();
}

void TableViewColumnSlot::reconnectVariable()
{
    auto *parent = static_cast<TableViewColumnCategory *>(getParentNode());
    disconnectDataSources();
    auto *node = getRoot().dataModel->findDataNode(url);
    if (!node || node->getVariable().empty()) {
        return;
    }
    parent->column->setVariable(node->getVariable(), period, scale, offset);
}

void TableViewColumnSlot::disconnectDataSources()
{
    auto *parent = static_cast<TableViewColumnCategory *>(getParentNode());
    parent->column->clearVariable();
}

void TableViewColumnSlot::appendDataSources(DataModel *model)
{
    DataSlot::appendDataSources(model);
    reconnectVariable();
}

bool TableViewColumnSlot::nodeSetData(int section, const QVariant &value)
{
    if (DataSlot::nodeSetData(section, value)) {
        reconnectVariable();
        return true;
    }
    return false;
}

/****************************************************************************/

TableViewColumnCategory::TableViewColumnCategory(
        TableViewTableCategory &category,
        int pos):
    SlotNode(&category, {}, pos),
    column(new QtPdCom::TableColumn())
{
    auto *root = &getRoot();
    QObject::connect(
            column,
            &QtPdCom::TableColumn::headerChanged,
            root,
            [this, root]() {
                const auto idx = this->getIndex();
                emit root->dataChanged(idx, idx, {Qt::DisplayRole});
            });
    static_cast<TableViewSlotModel *>(getParentNode()->getParentNode())
            ->model->insertColumn(column, pos);
}

TableViewColumnCategory::~TableViewColumnCategory()
{
    if (getChildCount() > 0) {
        getRoot().removeRows(0, getChildCount(), getIndex());
    }
    static_cast<TableViewSlotModel *>(getParentNode()->getParentNode())
            ->model->removeColumn(column);
    column->deleteLater();
}

bool TableViewColumnCategory::nodeSetData(int section, const QVariant &value)
{
    if (section == UrlColumn) {
        column->setHeader(value.toString());
        return true;
    }
    return SlotNode::nodeSetData(section, value);
}

QVariant TableViewColumnCategory::nodeData(
        int role,
        int section,
        const DataModel *dataModel) const
{
    if (role == Qt::DisplayRole && section == UrlColumn) {
        return column->getHeader();
    } else if (role == Qt::DecorationRole && section == UrlColumn) {
        return QIcon(":/images/object-columns.svg");
    }
    return SlotNode::nodeData(role, section, dataModel);
}

SlotNode::AcceptedDataConnectionModes
TableViewColumnCategory::canInsertDataNodes(
        QList<DataNode *> const &nodes,
        int /* row */,
        QString &err,
        bool replace) const
{
    if (nodes.size() != 1 || nodes[0]->getVariable().empty()) {
        err = QObject::tr("Please select exactly one variable.");
        return AcceptedDataConnectionModes::None;
    }
    if (!replace && getChildCount() != 0) {
        err = QObject::tr("Already connected. Press Shift to replace.");
        return AcceptedDataConnectionModes::None;
    }
    return AcceptedDataConnectionModes::VariableButNoSelector;
}

bool TableViewColumnCategory::insertDataNodes(
        QList<DataNode *> const &nodes,
        const Selector &,
        int row,
        QString &err,
        bool replace)
{
    if (canInsertDataNodes(nodes, row, err, replace)
        != AcceptedDataConnectionModes::VariableButNoSelector) {
        return false;
    }

    if (replace && getChildCount() > 0) {
        getRoot().removeRows(0, getChildCount(), getIndex());
    }

    auto node = new TableViewColumnSlot(this);
    node->url = nodes[0]->nodeUrl();
    if (!nodes[0]->getVariable().isWriteable()) {
        node->period = std::chrono::milliseconds {200};
    }
    node->reconnectVariable();
    return true;
}

void TableViewColumnCategory::nodeFlags(Qt::ItemFlags &flags, int section)
        const
{
    SlotNode::nodeFlags(flags, section);
    if (getChildCount() == 0) {
        flags |= Qt::ItemIsDropEnabled;
    }
    if (section == UrlColumn) {
        flags |= Qt::ItemIsEditable;
    }
}

QJsonValue TableViewColumnCategory::toJson() const
{
    QJsonObject ans;

    ans["header"] = column->getHeader();
    ans["decimals"] = static_cast<int>(column->getDecimals());

    ans["columns"] = SlotNode::toJson();

    return ans;
}

/****************************************************************************/

void TableViewColumnCategory::fromJson(QJsonValue const &val)
{
    const auto obj = val.toObject();

    if (obj.contains("header"))
        column->setHeader(obj["header"].toString());

    if (obj.contains("decimals"))
        column->setDecimals(obj["decimals"].toInt());

    if (obj.contains("columns")) {
        const auto array = obj["columns"].toArray();
        if (array.size() == 1) {
            auto node = new TableViewColumnSlot(this);
            node->fromJson(array[0]);
        }
    }
}

/****************************************************************************/

TableViewVisibleSlot::~TableViewVisibleSlot()
{
    disconnectDataSources();
}

bool TableViewVisibleSlot::nodeSetData(int section, const QVariant &value)
{
    if (DataSlot::nodeSetData(section, value)) {
        reconnectVariable();
        return true;
    }
    return false;
}

void TableViewVisibleSlot::reconnectVariable()
{
    auto model = static_cast<TableViewSlotModel *>(
            getParentNode()->getParentNode());
    model->model->clearVisibleRowsVariable();
    auto dataModel = getRoot().dataModel;
    auto node = dataModel->findDataNode(url);
    if (!node || node->getVariable().empty()) {
        return;
    }
    model->model->setVisibleRowsVariable(
            node->getVariable(),
            selector.toPdComSelector(),
            period);
}

void TableViewVisibleSlot::disconnectDataSources()
{
    auto model = static_cast<TableViewSlotModel *>(
            getParentNode()->getParentNode());
    model->model->clearVisibleRowsVariable();
}

void TableViewVisibleSlot::appendDataSources(DataModel *model)
{
    DataSlot::appendDataSources(model);
    reconnectVariable();
}

/****************************************************************************/

void TableViewVisibleCategory::fromJson(const QJsonValue &val)
{
    const auto array = val.toArray();
    if (array.size() == 1) {
        auto node = new TableViewVisibleSlot(this);
        node->fromJson(array[0]);
    }
}

bool TableViewVisibleCategory::insertDataNodes(
        QList<DataNode *> const &nodes,
        const Selector &selector,
        int row,
        QString &err,
        bool replace)
{
    if (canInsertDataNodes(nodes, row, err, replace)
        != AcceptedDataConnectionModes::Scalar) {
        return false;
    }

    if (replace && getChildCount() > 0) {
        getRoot().removeRows(0, getChildCount(), getIndex());
    }

    auto node = new TableViewVisibleSlot(this);
    if (!nodes[0]->getVariable().isWriteable()) {
        node->period = std::chrono::milliseconds {200};
    }
    node->url = nodes[0]->nodeUrl();
    node->selector = selector;
    node->reconnectVariable();
    return true;
}

/****************************************************************************/

TableViewHighlightSlot::~TableViewHighlightSlot()
{
    disconnectDataSources();
}

void TableViewHighlightSlot::reconnectVariable()
{
    auto model = static_cast<TableViewSlotModel *>(
            getParentNode()->getParentNode());
    model->model->clearHighlightRowVariable();
    auto dataModel = getRoot().dataModel;
    auto node = dataModel->findDataNode(url);
    if (!node || node->getVariable().empty()) {
        return;
    }
    model->model->setHighlightRowVariable(
            node->getVariable(),
            selector.toPdComSelector(),
            period);
}

void TableViewHighlightSlot::disconnectDataSources()
{
    auto model = static_cast<TableViewSlotModel *>(
            getParentNode()->getParentNode());
    model->model->clearHighlightRowVariable();
}

void TableViewHighlightSlot::appendDataSources(DataModel *model)
{
    DataSlot::appendDataSources(model);
    reconnectVariable();
}

bool TableViewHighlightSlot::nodeSetData(int section, const QVariant &value)
{
    if (DataSlot::nodeSetData(section, value)) {
        reconnectVariable();
        return true;
    }
    return false;
}

/****************************************************************************/

void TableViewHighlightCategory::fromJson(const QJsonValue &val)
{
    const auto array = val.toArray();
    if (array.size() == 1) {
        auto node = new TableViewHighlightSlot(this);
        node->fromJson(array[0]);
    }
}

bool TableViewHighlightCategory::insertDataNodes(
        QList<DataNode *> const &nodes,
        const Selector &selector,
        int row,
        QString &err,
        bool replace)
{
    if (canInsertDataNodes(nodes, row, err, replace)
        != AcceptedDataConnectionModes::Scalar) {
        return false;
    }

    if (replace && getChildCount() > 0) {
        getRoot().removeRows(0, getChildCount(), getIndex());
    }

    auto node = new TableViewHighlightSlot(this);
    if (!nodes[0]->getVariable().isWriteable()) {
        node->period = std::chrono::milliseconds {200};
    }
    node->url = nodes[0]->nodeUrl();
    node->selector = selector;
    node->reconnectVariable();
    return true;
}

/****************************************************************************/
