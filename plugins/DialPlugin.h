/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2018  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef DIALPLUGIN_H
#define DIALPLUGIN_H

/****************************************************************************/

#include "Plugin.h"

#include <QCoreApplication>

/****************************************************************************/

class DialPlugin: public Plugin
{
        Q_DECLARE_TR_FUNCTIONS(DialPlugin)

    public:
        // Plugin information
        QString name() const override;
        QString type() const override { return "QtPdWidgets::Dial"; }
        QIcon icon() const override;

        // Private data
        void *createPrivateData(QWidget *) const override;
        void readPrivateData(const QJsonObject &, QWidget *, void *)
                const override;
        QJsonObject writePrivateData(QWidget *, const void *) const override;
        void deletePrivateData(QWidget *, void *) const override;

        void openEditor(QWidget *, void *, SlotModel *, QWidget *) override;

        // Process data connection

        SlotModel* createSlotModel(
                QWidget *,
                SlotModelCollection &parent,
                WidgetContainer&) const override;

    private:
        QWidget *createWidget(QWidget *) const override;
};

/****************************************************************************/

#endif
