/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2023 Daniel Ramirez <dr@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "ParameterSetWidgetPlugin.h"

#include "DataModel.h"
#include "DataSource.h"
#include "SlotModel.h"
#include "WidgetContainer.h"

#include <QtPdWidgets2/ParameterSetWidget.h>

#include <QtPdCom1/Process.h>

#include <pdcom5/Process.h>

#include <QFileDialog>

namespace {

class ParameterSetSlotModel: public SlotModel
{
    public:
        ParameterSetSlotModel(
                SlotModelCollection &parent,
                WidgetContainer &container,
                Pd::ParameterSetWidget &widget):
            SlotModel(parent, container),
            widget(widget)
        {
            appendDataSources(parent.dataModel);
        }

        void appendDataSources(DataModel *model) override
        {
            appendDataSources(static_cast<const DataModel*>(model));
        }
        void appendDataSources(const DataModel *model);

        Pd::ParameterSetWidget &widget;
};

}  // namespace

/****************************************************************************/

QString ParameterSetWidgetPlugin::name() const
{
    return tr("Parameter Set Widget");
}

/****************************************************************************/

QIcon ParameterSetWidgetPlugin::icon() const
{
    return QIcon(":/images/plugin-parametersetwidget.svg");
}

/****************************************************************************/


SlotModel *ParameterSetWidgetPlugin::createSlotModel(
        QWidget *widget,
        SlotModelCollection &parent,
        WidgetContainer &container) const
{
    auto pdwidget = qobject_cast<Pd::ParameterSetWidget*>(widget);
    if (!pdwidget)
        return nullptr;
    return new ParameterSetSlotModel(parent, container, *pdwidget);
}

/****************************************************************************/

void ParameterSetWidgetPlugin::openEditor(
        QWidget *widget,
        void *data,
        SlotModel *,
        QWidget *parent)
{
    Q_UNUSED(data);
    Q_UNUSED(parent);

    auto *parametersWidget = dynamic_cast<Pd::ParameterSetWidget *>(widget);
    if (!parametersWidget) {
        return;
    }
    auto container = qobject_cast<WidgetContainer *>(widget->parent());

    auto dialog = new QFileDialog(parent);
    dialog->setAcceptMode(QFileDialog::AcceptOpen);
    dialog->setFileMode(QFileDialog::Directory);
    dialog->setOption(QFileDialog::ShowDirsOnly, true);

    connect(dialog, &QDialog::finished, dialog, &QObject::deleteLater);

    connect(dialog, &QDialog::accepted,
    parametersWidget, [parametersWidget, container, dialog]()
            {
                parametersWidget->setPath(dialog->selectedFiles()[0]);
                container->setProperty("path", // FIXME: update property name
                    QVariant::fromValue(dialog->selectedFiles()[0]));
            }
    );
    dialog->open();
}

/****************************************************************************/

void ParameterSetSlotModel::appendDataSources(const DataModel *dataModel)
{
    QSet<QtPdCom::Process *> processes;
    int numberRows = dataModel->rowCount(QModelIndex());
    for (int row = 0; row < numberRows; row++) {
        QModelIndex index = dataModel->index(row, 0, QModelIndex());
        QtPdCom::Process &process =
                dataModel->getDataSource(index)->getProcess();
        processes.insert(&process);
    }
    widget.setProcesses(processes);
}

/****************************************************************************/

QWidget *ParameterSetWidgetPlugin::createWidget(QWidget *parent) const
{
    auto widget = new Pd::ParameterSetWidget(parent);
    auto container = qobject_cast<WidgetContainer *>(widget->parent());

    widget->setContextMenuPolicy(Qt::NoContextMenu);
    container->setMinimumSize(331, 30);
    return widget;
}

/****************************************************************************/
