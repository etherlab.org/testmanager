/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2018  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "DigitalPlugin.h"

#include "DataSlot.h"
#include "DataModel.h"
#include "DataNode.h"
#include "SlotModel.h"
#include "ScalarSubscriberPluginImpl.h"

#include <QtPdWidgets2/Digital.h>

#include <QDebug>
#include <QJsonObject>

template<>
struct DefaultPeriod<Pd::Digital>
{
    static constexpr std::chrono::milliseconds Period {200};
};


/****************************************************************************/

QString DigitalPlugin::name() const
{
    return tr("Digital Display");
}

/****************************************************************************/

QIcon DigitalPlugin::icon() const
{
    return QIcon(":/images/plugin-digital.svg");
}

/****************************************************************************/

template class ScalarSubscriberPlugin<Pd::Digital, false>;

/****************************************************************************/
