/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2019-2023  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef TABLEVIEWPLUGINPRIVATE_H
#define TABLEVIEWPLUGINPRIVATE_H

#include <QtPdCom1/TableColumn.h>
#include <QtPdCom1/TableModel.h>

#include <QDebug>

#include "SlotModel.h"
#include "SlotNode.h"
#include "DataSlot.h"

namespace Pd {
class TableView;
}

/****************************************************************************/

class TableViewSlotModel: public SlotModel
{
        Q_OBJECT

    public:
        TableViewSlotModel(
                SlotModelCollection &parent,
                WidgetContainer &,
                Pd::TableView *widget);
        ~TableViewSlotModel();

        QtPdCom::TableModel *const model;
        Pd::TableView *const widget;

        AcceptedDataConnectionModes canInsertDataNodes(
                QList<DataNode *> const &nodes,
                int row,
                QString &err,
                bool replace) const override;
        bool insertDataNodes(
                QList<DataNode *> const &,
                const Selector &selector,
                int row,
                QString &err,
                bool replace);

        void clear() override;

        void fromJson(const QJsonValue &value) override;
        QJsonValue toJson() const override;
};

class TableViewTableCategory: public SlotNode
{
    public:
        using SlotNode::SlotNode;

        void clear();
        AcceptedDataConnectionModes canInsertDataNodes(
                QList<DataNode *> const &nodes,
                int row,
                QString &err,
                bool replace) const override;
        bool insertDataNodes(
                QList<DataNode *> const &,
                const Selector &selector,
                int row,
                QString &err,
                bool replace);

        void nodeFlags(Qt::ItemFlags &flags, int section) const override;
        void fromJson(const QJsonValue &value) override;
};

class TableViewColumnSlot: public DataSlot
{
    public:
        TableViewColumnSlot(SlotNode *parent, int pos = -1):
            DataSlot(
                    parent,
                    pos,
                    DataSlot::DoesNotSupportTau
                            | DataSlot::DoesNotSupportSelector)
        {}

        ~TableViewColumnSlot();

        void reconnectVariable();

        void disconnectDataSources() override;
        void appendDataSources(DataModel *) override;
        bool nodeSetData(int section, const QVariant &value) override;
        bool supportsRemovingByUser() const override { return true; }
};

class TableViewColumnCategory: public SlotNode
{
    public:
        TableViewColumnCategory(TableViewTableCategory &, int pos);
        ~TableViewColumnCategory();

        bool supportsRemovingByUser() const override { return true; }

        QtPdCom::TableColumn *const column;

        bool nodeSetData(int, const QVariant &) override;
        QVariant
        nodeData(int, int, const DataModel *dataModel) const override;


        AcceptedDataConnectionModes canInsertDataNodes(
                QList<DataNode *> const &nodes,
                int /* row */,
                QString &err,
                bool replace) const override;
        bool insertDataNodes(
                QList<DataNode *> const &,
                const Selector &selector,
                int row,
                QString &err,
                bool replace);

        void nodeFlags(Qt::ItemFlags &flags, int section) const override;

        QJsonValue toJson() const override;
        void fromJson(QJsonValue const &val) override;
};

class TableViewVisibleSlot: public DataSlot
{
    public:
        TableViewVisibleSlot(SlotNode *parent, int pos = -1):
            DataSlot(parent, pos, DataSlot::DoesNotSupportTau)
        {}

        bool supportsRemovingByUser() const override { return true; }
        bool nodeSetData(int, const QVariant &) override;

        ~TableViewVisibleSlot();

        void reconnectVariable();

        void disconnectDataSources() override;
        void appendDataSources(DataModel *) override;
};

class TableViewVisibleCategory: public SingleVariableCategory
{
    public:
        using SingleVariableCategory::SingleVariableCategory;


        void fromJson(const QJsonValue &);
        bool insertDataNodes(
                QList<DataNode *> const &,
                const Selector &selector,
                int row,
                QString &err,
                bool replace) override;
};

class TableViewHighlightSlot: public DataSlot
{
    public:
        TableViewHighlightSlot(SlotNode *parent, int pos = -1):
            DataSlot(parent, pos, DataSlot::DoesNotSupportTau)
        {}

        bool supportsRemovingByUser() const override { return true; }
        bool nodeSetData(int, const QVariant &) override;

        ~TableViewHighlightSlot();

        void reconnectVariable();

        void disconnectDataSources() override;
        void appendDataSources(DataModel *) override;
};

class TableViewHighlightCategory: public SingleVariableCategory
{
    public:
        using SingleVariableCategory::SingleVariableCategory;


        void fromJson(const QJsonValue &value);
        bool insertDataNodes(
                QList<DataNode *> const &,
                const Selector &selector,
                int row,
                QString &err,
                bool replace) override;
};


#endif
