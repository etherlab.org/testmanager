/******************************************************************************
 * SVG Plugin.
 * Unittest to develope the integration of animated svg files to the qt
 * interface of the testmanager.
 *
 * Jonathan Grahl <jonathan.grahl@igh.de>
******************************************************************************/

// ----------------------------------------------------------------------------
#include <QApplication>
#include <QDateTime>
#include <QDebug>
#include <QSettings>

#include "SvgTest.h"




// ============================================================================
int main(int argc, char *argv[])
{
    // ---------- Static initialisations
    QCoreApplication::setOrganizationName("IgH");
    QCoreApplication::setApplicationName("Unittest SVG");
    QSettings::setDefaultFormat(QSettings::IniFormat);


    // ---------- Init application
    QApplication app(argc, argv);
    SvgPlugin svg;

    svg.show();
    return app.exec();

}



//
