/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2019  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "BarPlugin.h"

#include "DataSlot.h"
#include "DataModel.h"
#include "DataNode.h"
#include "SlotModel.h"
#include "SlotModelCollection.h"

#include <QtPdWidgets2/Bar.h>
#include <QTimer>

#include <QDebug>
#include <QJsonObject>
#include <QJsonArray>

namespace {
class BarModel;
class BarStack;

class BarVariable: public DataSlot
{
    public:
        explicit BarVariable(BarStack &parent, int pos = -1);
        ~BarVariable();

        bool supportsRemovingByUser() const override { return true; }

        bool nodeSetData(int col, const QVariant &data) override;
};

class BarStack: public SlotNode
{
    public:
        BarStack(BarModel &parent, int pos = -1);
        ~BarStack();

        bool supportsRemovingByUser() const override { return true; }
        void nodeFlags(Qt::ItemFlags &flags, int section) const override
        {
            SlotNode::nodeFlags(flags, section);
            flags |= Qt::ItemIsDropEnabled;
        }

        QVariant
        nodeData(int role, int section, const DataModel *model) const override
        {
            if (section == UrlColumn && role == Qt::DisplayRole) {
                return QObject::tr("Stack %1")
                        .arg(getParentNode()->getChildIndex(this) + 1);
            }
            return SlotNode::nodeData(role, section, model);
        }
        AcceptedDataConnectionModes canInsertDataNodes(
                QList<DataNode *> const &,
                int row,
                QString &err,
                bool replace) const override;
        bool insertDataNodes(
                QList<DataNode *> const &,
                const Selector &selector,
                int row,
                QString &err,
                bool replace) override;

        void fromJson(const QJsonValue &) override;
};


class BarModel: public SlotModel
{
        Q_OBJECT

    public:
        BarModel(
                SlotModelCollection &parent,
                WidgetContainer &container,
                Pd::Bar &bar):
            SlotModel(parent, container),
            bar(bar)
        {}
        ~BarModel()
        {
            bulkErasing = true;
            clear();
        }
        void nodeFlags(Qt::ItemFlags &flags, int section) const override
        {
            SlotModel::nodeFlags(flags, section);
            flags |= Qt::ItemIsDropEnabled;
        }

        void clear() override;
        AcceptedDataConnectionModes canInsertDataNodes(
                QList<DataNode *> const &,
                int row,
                QString &err,
                bool replace) const override;
        bool insertDataNodes(
                QList<DataNode *> const &,
                const Selector &selector,
                int row,
                QString &err,
                bool replace) override;

        void fromJson(const QJsonValue &) override;
        QJsonValue toJson() const override;
        void disconnectDataSources() override { bar.clearVariables(); }
        void appendDataSources(DataModel *model) override
        {
            SlotModel::appendDataSources(model);
            reconnectVariables();
        }

        Pd::Bar &bar;
        // set this to true to avoid reconnecting variables;
        bool bulkErasing = false;

    public slots:
        void reconnectVariables();
};

BarStack::BarStack(BarModel &parent, int pos):
    SlotNode(&parent, {}, pos)
{}

BarStack::~BarStack()
{
    auto model = static_cast<BarModel *>(getParentNode());
    if (!model->bulkErasing) {
        // schedule reconnecting bar variables
        QTimer::singleShot(0, model, &BarModel::reconnectVariables);
    }
}

SlotNode::AcceptedDataConnectionModes BarStack::canInsertDataNodes(
        QList<DataNode *> const &nodes,
        int row,
        QString &err, bool replace) const
{
    auto &parent = *static_cast<BarModel *>(getParentNode());
    return parent.canInsertDataNodes(nodes, row, err, replace);
}

bool BarStack::insertDataNodes(
        QList<DataNode *> const &nodes,
        const Selector &selector,
        int row,
        QString &err,
        bool replace)
{
    auto &parent = *static_cast<BarModel *>(getParentNode());
    if (parent.canInsertDataNodes(nodes, row, err, replace)
        != AcceptedDataConnectionModes::Scalar) {
        return false;
    }

    const auto childs = getChildCount();
    if (replace && childs > 0) {
        getRoot().removeRows(0, childs, getIndex());
    }

    for (int i = 0; i < nodes.size(); ++i) {
        auto *node = new BarVariable(*this, row + i);
        node->url = nodes[i]->nodeUrl();
        node->selector = selector;
        node->color = parent.nextColor();
        if (!nodes[i]->getVariable().isWriteable()) {
            node->period = std::chrono::milliseconds {100};
        }
    }
    parent.reconnectVariables();
    return true;
}

void BarStack::fromJson(const QJsonValue &val)
{
    for (const auto &slot : val.toArray()) {
        auto node = new BarVariable(*this);
        node->fromJson(slot);
    }
}

BarVariable::BarVariable(BarStack &parent, int pos):
    DataSlot(&parent, pos, DataSlot::SupportsColor)
{}

BarVariable::~BarVariable()
{
    auto model = static_cast<BarModel *>(getParentNode()->getParentNode());
    if (!model->bulkErasing) {
        // schedule reconnecting bar variables
        QTimer::singleShot(0, model, &BarModel::reconnectVariables);
    }
}

bool BarVariable::nodeSetData(int col, const QVariant &data)
{
    if (!DataSlot::nodeSetData(col, data)) {
        return false;
    }

    auto *model = getParentNode()->getParentNode();
    static_cast<BarModel *>(model)->reconnectVariables();
    return true;
}

void BarModel::clear()
{
    bulkErasing = true;
    bar.clearVariables();
    auto &root = getRoot();
    for (int i = 0; i < getChildCount(); ++i) {
        auto *node = getChildNode(i);
        root.removeRows(0, node->getChildCount(), node->getIndex());
    }
    root.removeRows(0, getChildCount(), getIndex());
    bulkErasing = false;
}

SlotModel::AcceptedDataConnectionModes BarModel::canInsertDataNodes(
        QList<DataNode *> const &nodes,
        int /* row */,
        QString &err,
        bool /* replace */) const
{
    for (const auto &node : nodes) {
        if (node->getVariable().empty()) {
            err = QObject::tr("Please select a Variable");
            return SlotModel::AcceptedDataConnectionModes::None;
        }
    }
    return SlotModel::AcceptedDataConnectionModes::Scalar;
}

bool BarModel::insertDataNodes(
        QList<DataNode *> const &nodes,
        const Selector &selector,
        int row,
        QString &err,
        bool replace)
{
    if (canInsertDataNodes(nodes, row, err, replace)
        != AcceptedDataConnectionModes::Scalar) {
        return false;
    }

    const auto childs = getChildCount();
    if (replace && childs > 0) {
        getRoot().removeRows(0, childs, getIndex());
    }

    auto *stack = new BarStack(*this, replace ? 0 : row);
    return stack->insertDataNodes(nodes, selector, 0, err, false);
}

void BarModel::fromJson(const QJsonValue &val)
{
    const auto obj = val.toObject();
    for (const auto &stack : obj["stacks"].toArray()) {
        auto node = new BarStack(*this);
        node->fromJson(stack);
    }
}

QJsonValue BarModel::toJson() const
{
    QJsonObject ans;
    ans["stacks"]  = SlotNode::toJson();
    return ans;
}

void BarModel::reconnectVariables()
{
    auto &dataModel = *getRoot().dataModel;
    bar.clearVariables();
    for (int stack_idx = 0; stack_idx < getChildCount(); ++stack_idx) {
        auto &stack = *static_cast<BarStack *>(getChildNode(stack_idx));
        if (stack.getChildCount() > 0) {
            auto &node1 = *static_cast<BarVariable *>(stack.getChildNode(0));
            auto *var1 = dataModel.findDataNode(node1.url);
            if (!var1 || var1->getVariable().empty()) {
                continue;
            }
            bar.addVariable(
                    var1->getVariable(),
                    node1.selector.toPdComSelector(),
                    node1.period,
                    node1.scale,
                    node1.offset,
                    node1.tau,
                    node1.color);
            var1->notifySubscribed(node1.period);
        }
        for (int i = 1; i < stack.getChildCount(); ++i) {
            auto &nextnode =
                    *static_cast<BarVariable *>(stack.getChildNode(i));
            auto *var = dataModel.findDataNode(nextnode.url);
            if (!var || var->getVariable().empty()) {
                continue;
            }
            bar.addStackedVariable(
                    var->getVariable(),
                    nextnode.selector.toPdComSelector(),
                    nextnode.period,
                    nextnode.scale,
                    nextnode.offset,
                    nextnode.tau,
                    nextnode.color);
            var->notifySubscribed(nextnode.period);
        }
    }
}

}  // namespace


/****************************************************************************/

QString BarPlugin::name() const
{
    return tr("Bar");
}

/****************************************************************************/

QIcon BarPlugin::icon() const
{
    return QIcon(":/images/plugin-bar.svg");
}

/****************************************************************************/

SlotModel *BarPlugin::createSlotModel(
        QWidget *widget,
        SlotModelCollection &parent,
        WidgetContainer &container) const
{
    auto pdwidget = qobject_cast<Pd::Bar *>(widget);
    if (!pdwidget) {
        return nullptr;
    }
    return new BarModel(parent, container, *pdwidget);
}

/****************************************************************************/

QWidget *BarPlugin::createWidget(QWidget *parent) const
{
    return new Pd::Bar(parent);
}

/****************************************************************************/

#include "BarPlugin.moc"
