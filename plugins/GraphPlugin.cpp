/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2018  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "GraphPlugin.h"

#include "DataSlot.h"
#include "DataModel.h"
#include "DataNode.h"
#include "SlotModel.h"
#include "SlotModelCollection.h"

#include <QtPdWidgets2/Graph.h>

#include <QDebug>
#include <QJsonObject>
#include <QJsonArray>
#include <QTimer>

namespace {


class GraphModel: public SlotModel
{
        Q_OBJECT
    public:
        GraphModel(
                SlotModelCollection &parent,
                WidgetContainer &container,
                Pd::Graph &graph);

    public slots:
        void reconnectGraphVariables();
        void reconnectTriggerVariable();

    public:
        void clear() override;

        AcceptedDataConnectionModes canInsertDataNodes(
                QList<DataNode *> const &nodes,
                int row,
                QString &err,
                bool replace) const override;


        bool insertDataNodes(
                QList<DataNode *> const &nodes,
                const Selector &selector,
                int row,
                QString &err,
                bool replace) override;


        void fromJson(const QJsonValue &) override;
        QJsonValue toJson() const override;
        void appendDataSources(DataModel * model) override
        {
            SlotModel::appendDataSources(model);
            reconnectGraphVariables();
            reconnectTriggerVariable();
        }
        void disconnectDataSources() override
        {
            graph.clearVariables();
            graph.clearTriggerVariable();
        }

        Pd::Graph &graph;
        bool bulkErase = false;
        bool scheduledReconnecting = false;
};

class GraphCategory: public SlotNode
{
    public:
        GraphCategory(GraphModel &parent):
            SlotNode(&parent, GraphModel::tr("Graphs"))
        {}
        void nodeFlags(Qt::ItemFlags &flags, int section) const override
        {
            SlotNode::nodeFlags(flags, section);
            flags |= Qt::ItemIsDropEnabled;
        }

        void clear()
        {
            if (getChildCount() <= 0) {
                return;
            }
            getRoot().removeRows(0, getChildCount(), getIndex());
            static_cast<GraphModel *>(getParentNode())
                    ->graph.clearVariables();
        }

        void scheduleReconnecting()
        {
            auto &graphModel = *static_cast<GraphModel *>(getParentNode());
            if (!graphModel.bulkErase && !graphModel.scheduledReconnecting) {
                graphModel.scheduledReconnecting = true;
                QTimer::singleShot(
                        0,
                        &graphModel,
                        &GraphModel::reconnectGraphVariables);
            }
        }

        void fromJson(const QJsonValue &val) override;

        AcceptedDataConnectionModes canInsertDataNodes(
                QList<DataNode *> const &nodes,
                int row,
                QString &err,
                bool replace) const override;


        bool insertDataNodes(
                QList<DataNode *> const &nodes,
                const Selector &selector,
                int row,
                QString &err,
                bool replace) override;
};

class GraphSlot: public DataSlot
{
    public:
        GraphSlot(SlotNode *parent, int pos):
            DataSlot(parent, pos, GraphSlot::SupportsColor)
        {}

        ~GraphSlot()
        {
            static_cast<GraphCategory *>(getParentNode())
                    ->scheduleReconnecting();
        }
        bool supportsRemovingByUser() const override { return true; }

        bool nodeSetData(int col, const QVariant &val) override
        {
            if (DataSlot::nodeSetData(col, val)) {
                static_cast<GraphCategory *>(getParentNode())
                    ->scheduleReconnecting();
                return true;
            }
            return false;
        }
};

class TriggerSlot: public DataSlot
{
    public:
        TriggerSlot(SlotNode *parent):
            DataSlot(parent)
        {}

        ~TriggerSlot()
        {
            auto &graphModel = *static_cast<GraphModel *>(
                    getParentNode()->getParentNode());
            graphModel.graph.clearTriggerVariable();
        }
        bool supportsRemovingByUser() const override { return true; }

        bool nodeSetData(int col, const QVariant &val) override
        {
            if (DataSlot::nodeSetData(col, val)) {
                static_cast<GraphModel*>(getParentNode()->getParentNode())->reconnectTriggerVariable();
                return true;
            }
            return false;
        }
};


class TriggerCategory: public SingleVariableCategory
{
    public:
        TriggerCategory(GraphModel &parent):
            SingleVariableCategory(&parent, GraphModel::tr("Trigger"))
        {}

        bool insertDataNodes(
                QList<DataNode *> const &nodes,
                const Selector &selector,
                int row,
                QString &err,
                bool replace) override;
        void fromJson(const QJsonValue &val) override;
};


GraphModel::GraphModel(
        SlotModelCollection &parent,
        WidgetContainer &container,
        Pd::Graph &graph):
    SlotModel(parent, container),
    graph(graph)
{
    new GraphCategory(*this);
    new TriggerCategory(*this);
}

void GraphModel::reconnectTriggerVariable()
{
    graph.clearTriggerVariable();
    auto category = static_cast<TriggerCategory *>(getChildNode(1));
    if (category->getChildCount() == 1) {
        auto &dataModel = *getRoot().dataModel;
        auto &item = *static_cast<TriggerSlot *>(category->getChildNode(0));
        auto var = dataModel.findDataNode(item.url);
        if (!var || var->getVariable().empty()) {
            return;
        }
        graph.setTriggerVariable(
                var->getVariable(),
                item.selector.toPdComSelector(),
                item.period,
                item.scale,
                item.offset,
                item.tau);
    }
}

void GraphModel::clear()
{
    bulkErase = true;
    static_cast<GraphCategory *>(getChildNode(0))->clear();
    static_cast<TriggerCategory *>(getChildNode(1))->clear();
    bulkErase = false;
}

SlotNode::AcceptedDataConnectionModes GraphModel::canInsertDataNodes(
        QList<DataNode *> const &nodes,
        int /* row */,
        QString &err,
        bool replace) const
{
    auto graphcategory = static_cast<GraphCategory *>(getChildNode(0));
    return graphcategory->canInsertDataNodes(nodes, -1, err, replace);
}

bool GraphModel::insertDataNodes(
        QList<DataNode *> const &nodes,
        const Selector &selector,
        int /* row */,
        QString &err,
        bool replace)
{
    auto graphcategory = static_cast<GraphCategory *>(getChildNode(0));
    return graphcategory->insertDataNodes(nodes, selector, -1, err, replace);
}

void GraphModel::fromJson(const QJsonValue &val)
{
    const auto obj = val.toObject();
    getChildNode(0)->fromJson(obj["graphs"]);
    getChildNode(1)->fromJson(obj["trigger"]);
}

QJsonValue GraphModel::toJson() const
{
    if (getChildCount() != 2)
        return {};
    QJsonObject ans;
    ans["graphs"] = getChildNode(0)->toJson();
    ans["trigger"] = getChildNode(1)->toJson();
    return ans;
}

void GraphModel::reconnectGraphVariables()
{
    graph.clearVariables();
    auto graphcategory = static_cast<GraphCategory *>(getChildNode(0));
    auto &dataModel = *getRoot().dataModel;
    for (int i = 0; i < graphcategory->getChildCount(); ++i) {
        const auto &item =
                *static_cast<GraphSlot *>(graphcategory->getChildNode(i));
        auto var = dataModel.findDataNode(item.url);
        if (!var || var->getVariable().empty()) {
            continue;
        }
        graph.addVariable(
                var->getVariable(),
                item.selector.toPdComSelector(),
                item.period,
                item.scale,
                item.offset,
                item.tau,
                item.color);
    }
    scheduledReconnecting = false;
}

bool TriggerCategory::insertDataNodes(
        QList<DataNode *> const &nodes,
        const Selector &selector,
        int row,
        QString &err,
        bool replace)
{
    if (canInsertDataNodes(nodes, row, err, replace)
        != AcceptedDataConnectionModes::Scalar) {
        return false;
    }

    if (replace) {
        clear();
    }

    auto node = new TriggerSlot(this);
    node->url = nodes[0]->nodeUrl();
    node->selector = selector;
    if (!nodes[0]->getVariable().isWriteable()) {
        node->period = std::chrono::milliseconds(100);
    }

    auto &graphModel = *static_cast<GraphModel *>(getParentNode());
    graphModel.graph.setTriggerVariable(
            nodes[0]->getVariable(),
            selector.toPdComSelector(),
            node->period,
            node->scale,
            node->offset,
            node->tau);
    return true;
}

void TriggerCategory::fromJson(const QJsonValue &val)
{
    const auto array = val.toArray();
    if (array.size() == 1) {
        auto node = new TriggerSlot(this);
        node->fromJson(array[0]);
    }
}

void GraphCategory::fromJson(const QJsonValue &val)
{
    const auto array = val.toArray();
    for (const auto &item : array) {
        auto node = new GraphSlot(this, -1);
        node->fromJson(item);
    }
}

SlotNode::AcceptedDataConnectionModes GraphCategory::canInsertDataNodes(
        QList<DataNode *> const &nodes,
        int /* row */,
        QString &err,
        bool /* replace */) const
{
    for (const auto &node : nodes) {
        if (node->getVariable().empty()) {
            err = QObject::tr("Please select a Variable");
            return AcceptedDataConnectionModes::None;
        }
    }
    return AcceptedDataConnectionModes::Scalar;
}

bool GraphCategory::insertDataNodes(
        QList<DataNode *> const &nodes,
        const Selector &selector,
        int row,
        QString &err,
        bool replace)
{
    if (canInsertDataNodes(nodes, row, err, replace)
        != AcceptedDataConnectionModes::Scalar) {
        return false;
    }
    auto &parent = *static_cast<GraphModel*>(getParentNode());


    if (replace) {
        row = 0;
        const auto children = getChildCount();
        if (children > 0) {
            getRoot().removeRows(0, children, getIndex());
        }
    }

    for (int i = 0; i < nodes.size(); ++i) {
        auto slot = new GraphSlot(this, row + i);
        if (!nodes[i]->getVariable().isWriteable()) {
            slot->period = std::chrono::milliseconds {100};
        }
        slot->url = nodes[i]->nodeUrl();
        slot->selector = selector;
        slot->color = parent.nextColor();
    }
    scheduleReconnecting();
    return true;
}
}  // namespace

/****************************************************************************/

QString GraphPlugin::name() const
{
    return tr("Graph");
}

/****************************************************************************/

QIcon GraphPlugin::icon() const
{
    return QIcon(":/images/plugin-graph.svg");
}

/****************************************************************************/

SlotModel *GraphPlugin::createSlotModel(
        QWidget *widget,
        SlotModelCollection &parent,
        WidgetContainer &container) const
{
    auto *pdwidget = qobject_cast<Pd::Graph *>(widget);
    if (!pdwidget) {
        return nullptr;
    }
    return new GraphModel(parent, container, *pdwidget);
}

/****************************************************************************/

QWidget *GraphPlugin::createWidget(QWidget *parent) const
{
    return new Pd::Graph(parent);
}

/****************************************************************************/

#include "GraphPlugin.moc"
