/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2018 - 2023  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "TableViewPluginDialog.h"

#include "TableViewPluginPrivate.h"
#include "SlotModelCollection.h"

#include <QAbstractTableModel>

#include <algorithm>  // std::sort()

/****************************************************************************/

class TableViewPluginDialog::Model: public QAbstractTableModel
{
    public:
        enum { HeaderColumn, DecimalsColumn, ColumnCount };

        Model(TableViewSlotModel *priv):
            priv(priv),
            tableCategory(static_cast<TableViewTableCategory*>(
                    priv->getChildNode(0)))
        {
        }

        /********************************************************************/

        int rowCount(const QModelIndex &index = {}) const
        {
            if (index.isValid()) {
                return 0;
            }
            else {
                return tableCategory->getChildCount();
            }
        }

        /********************************************************************/

        int columnCount(const QModelIndex &) const { return ColumnCount; }

        /********************************************************************/

        QVariant data(const QModelIndex &index, int role) const
        {
            if (not index.isValid() or index.row() < 0
                or index.row() >= rowCount()) {
                return QVariant();
            }

            switch (index.column()) {
                case HeaderColumn:
                    switch (role) {
                        case Qt::DisplayRole:
                        case Qt::EditRole:
                            return getColumn(index.row())->column->getHeader();
                        default:
                            return QVariant();
                    }
                    break;

                case DecimalsColumn:
                    switch (role) {
                        case Qt::DisplayRole:
                        case Qt::EditRole:
                            return getColumn(index.row())->column->getDecimals();
                        default:
                            return QVariant();
                    }
                    break;

                default:
                    return QVariant();
            }
        }

        /********************************************************************/

        bool
        setData(const QModelIndex &index, const QVariant &value, int role)
        {
            if (role != Qt::EditRole or not index.isValid() or index.row() < 0
                or index.row() >= rowCount() or index.column() < HeaderColumn
                or index.column() >= ColumnCount) {
                return false;
            }

            switch (index.column()) {
                case HeaderColumn:
                    getColumn(index.row())->column->setHeader(value.toString());
                    return true;

                case DecimalsColumn: {
                    bool ok;
                    int num = value.toUInt(&ok);
                    if (!ok) {
                        return false;
                    }
                    getColumn(index.row())->column->setDecimals(num);
                    return true;
                }
            }

            return false;
        }

        /********************************************************************/

        QVariant headerData(int section, Qt::Orientation o, int role) const
        {
            if (role == Qt::DisplayRole && o == Qt::Horizontal) {
                switch (section) {
                    case HeaderColumn:
                        return TableViewPluginDialog::tr("Header");

                    case DecimalsColumn:
                        return TableViewPluginDialog::tr("Decimals");

                    default:
                        return QVariant();
                }
            }
            else {
                return QVariant();
            }
        }

        /********************************************************************/

        Qt::ItemFlags flags(const QModelIndex &index) const
        {
            if (!index.isValid()) {
                return Qt::NoItemFlags;
            }

            return Qt::ItemIsEnabled | Qt::ItemIsEditable
                    | Qt::ItemIsSelectable;
        }

        /********************************************************************/

        void addRow()
        {
            const auto count = rowCount();
            beginInsertRows(QModelIndex(), count, count);
            new TableViewColumnCategory(*tableCategory, count);
            endInsertRows();
        }

        /********************************************************************/

        void remove(const QModelIndexList &indexes)
        {
            QList<QPersistentModelIndex> persistentIndexes;

            foreach (QModelIndex index, indexes) {
                persistentIndexes << index;
            }

            auto &root = *static_cast<SlotModelCollection*>(priv->getParentNode());
            const auto tableIndex = tableCategory->getIndex();

            foreach (const QPersistentModelIndex &index, persistentIndexes) {
                if (index.isValid()) {
                    const auto *col = getColumn(index.row());
                    Q_ASSERT(col != nullptr);
                    beginRemoveRows(QModelIndex(), index.row(), index.row());
                    root.removeRows(index.row(), 1, tableIndex);
                    endRemoveRows();
                }
            }
        }

        /********************************************************************/

    private:
        TableViewSlotModel *const priv;
        TableViewTableCategory *const tableCategory;

        TableViewColumnCategory *getColumn(int idx) const
        {
            if (idx < 0 || idx >= tableCategory->getChildCount())
                return nullptr;
            return static_cast<TableViewColumnCategory*>(tableCategory->getChildNode(idx));
        }
};

/****************************************************************************/

TableViewPluginDialog::TableViewPluginDialog(
        TableViewSlotModel *priv,
        QWidget *parent):
    QDialog(parent),
    priv(priv),
    model(new Model(priv))
{
    setupUi(this);

    tableView->setModel(model);

    connect(tableView->selectionModel(),
            SIGNAL(selectionChanged(
                    const QItemSelection &,
                    const QItemSelection &)),
            this,
            SLOT(selectionChanged(
                    const QItemSelection &,
                    const QItemSelection &)));
}

/****************************************************************************/

TableViewPluginDialog::~TableViewPluginDialog()
{
    tableView->setModel(nullptr);
    delete model;
}

/****************************************************************************/

void TableViewPluginDialog::on_toolButtonAdd_clicked()
{
    model->addRow();
}

/****************************************************************************/

void TableViewPluginDialog::on_toolButtonRemove_clicked()
{
    model->remove(tableView->selectionModel()->selectedIndexes());
}

/****************************************************************************/

void TableViewPluginDialog::selectionChanged(
        const QItemSelection &,
        const QItemSelection &)
{
    toolButtonRemove->setEnabled(tableView->selectionModel()->hasSelection());
}

/****************************************************************************/
