/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2019  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef SVGPLUGIN_H
#define SVGPLUGIN_H

/****************************************************************************/

#include "Plugin.h"

#include <QCoreApplication>

/****************************************************************************/

class SvgPlugin: public Plugin
{
        Q_DECLARE_TR_FUNCTIONS(SvgPlugin)

    public:
        // Plugin information
        QString name() const override;
        QString type() const override { return "QtPdWidgets::Svg"; }
        QIcon icon() const override;

        // Private data
        void *createPrivateData(QWidget *) const override;
        void readPrivateData(const QJsonObject &, QWidget *, void *)
                const override;
        QJsonObject writePrivateData(QWidget *, const void *) const override;
        void deletePrivateData(QWidget *, void *) const override;

        void openEditor(QWidget *, void *, SlotModel *, QWidget *) override;

        // Process data connection

        struct Row
        {
                QString id;
                bool available;
        };

    private:
        QWidget *createWidget(QWidget *) const override;
        static QStringList getIdList(const QList<Row> &);
};

/****************************************************************************/

#endif  // SVGPLUGIN_H
