/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2018  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "XYGraphPlugin.h"

#include "DataSlot.h"
#include "DataModel.h"
#include "DataNode.h"
#include "SlotModel.h"
#include "SlotModelCollection.h"

#include <QtPdWidgets2/XYGraph.h>

#include <QJsonArray>
#include <QJsonObject>
#include <QDebug>

namespace {

class XYGraphSlotModel: public SlotModel
{
    Q_OBJECT

    public:
        XYGraphSlotModel(
                SlotModelCollection &parent,
                WidgetContainer &container,
                Pd::XYGraph &widget);

        void clear() override;
        void fromJson(const QJsonValue &value) override;
        QJsonValue toJson() const override;

        AcceptedDataConnectionModes canInsertDataNodes(
                QList<DataNode *> const &nodes,
                int row,
                QString &err,
                bool replace) const override;

        bool insertDataNodes(
                QList<DataNode *> const &nodes,
                const Selector &selector,
                int row,
                QString &err,
                bool replace) override;

        enum class DropMode {
            None,
            OnlyX,
            OnlyY,
            BothAxes,
        };

        DropMode
        checkDropMode(QList<DataNode *> const &nodes, QString &err, bool replace) const;

        void nodeFlags(Qt::ItemFlags &flags, int section) const override
        {
            SlotModel::nodeFlags(flags, section);
            if (section == UrlColumn
                && (getChildNode(0)->getChildCount() == 0
                    || getChildNode(1)->getChildCount() == 0)) {
                flags |= Qt::ItemIsDropEnabled;
            }
        }

        void reconnectVariables();
        void appendDataSources(DataModel *model) override
        {
            SlotModel::appendDataSources(model);
            reconnectVariables();
        }
        void disconnectDataSources() override { widget.clearVariables(); }

        Pd::XYGraph &widget;
};

class XYSlot: public DataSlot
{
    public:
        XYSlot(SlotNode *parent, int pos = -1):
            DataSlot(parent, pos, DataSlot::DoesNotSupportTau)
        {}
        bool supportsRemovingByUser() const override { return true; }

        ~XYSlot()
        {
            auto *model = static_cast<XYGraphSlotModel *>(
                    getParentNode()->getParentNode());
            model->widget.clearVariables();
        }

        bool nodeSetData(int col, const QVariant &val) override
        {
            if (DataSlot::nodeSetData(col, val)) {
                static_cast<XYGraphSlotModel *>(
                        getParentNode()->getParentNode())
                        ->reconnectVariables();
                return true;
            }
            return false;
        }
};

class XYCategory: public SingleVariableCategory
{
    public:
        using SingleVariableCategory::SingleVariableCategory;

        bool insertDataNodes(
                QList<DataNode *> const &nodes,
                const Selector &selector,
                int row,
                QString &err,
                bool replace) override;

        void fromJson(const QJsonValue &value) override;
};

XYGraphSlotModel::XYGraphSlotModel(
        SlotModelCollection &parent,
        WidgetContainer &container,
        Pd::XYGraph &widget):
    SlotModel(parent, container),
    widget(widget)
{
    new XYCategory(this, tr("X Axis"));
    new XYCategory(this, tr("Y Axis"));
}

void XYGraphSlotModel::clear()
{
    for (int i = 0; i < getChildCount(); ++i) {
        static_cast<XYCategory *>(getChildNode(i))->clear();
    }
}

void XYGraphSlotModel::fromJson(const QJsonValue &value)
{
    const auto obj = value.toObject();
    getChildNode(0)->fromJson(obj["xAxis"]);
    getChildNode(1)->fromJson(obj["yAxis"]);
}

QJsonValue XYGraphSlotModel::toJson() const
{
    if (getChildCount() != 2)
        return {};
    QJsonObject ans;
    ans["xAxis"] = getChildNode(0)->toJson();
    ans["yAxis"] = getChildNode(1)->toJson();
    return ans;
}

SlotNode::AcceptedDataConnectionModes XYGraphSlotModel::canInsertDataNodes(
        QList<DataNode *> const &nodes,
        int row,
        QString &err,
        bool replace) const
{
    switch (row) {
        case 0:
        case 1:
            return getChildNode(0)->canInsertDataNodes(nodes, -1, err, replace);
        case 2:
            return getChildNode(1)->canInsertDataNodes(nodes, -1, err, replace);
        default:
            break;
    }
    if (checkDropMode(nodes, err, replace) == DropMode::None) {
        return AcceptedDataConnectionModes::None;
    }
    return AcceptedDataConnectionModes::Scalar;
}

bool XYGraphSlotModel::insertDataNodes(
        QList<DataNode *> const &nodes,
        const Selector &selector,
        int row,
        QString &err,
        bool replace)
{
    switch (row) {
        case 0:
        case 1:
            return getChildNode(0)->insertDataNodes(nodes, selector, -1, err, replace);
        case 2:
            return getChildNode(1)->insertDataNodes(nodes, selector, -1, err, replace);
        default:
            break;
    }
    switch (checkDropMode(nodes, err, replace)) {
        case DropMode::OnlyX:
            return getChildNode(0)->insertDataNodes(nodes, selector, -1, err, replace);
        case DropMode::OnlyY:
            return getChildNode(1)->insertDataNodes(nodes, selector, -1, err, replace);
        case DropMode::BothAxes:
            return getChildNode(0)
                           ->insertDataNodes({nodes[0]}, selector, -1, err, replace)
                    && getChildNode(1)->insertDataNodes(
                            {nodes[1]},
                            selector,
                            -1,
                            err,
                            replace);
        default:
            return false;
    }
}

XYGraphSlotModel::DropMode XYGraphSlotModel::checkDropMode(
        QList<DataNode *> const &nodes,
        QString &err,
        bool replace) const
{
    if (nodes.size() == 2) {
        for (int i = 0; i < 2; ++i) {
            if (getChildNode(i)->canInsertDataNodes({nodes[i]}, -1, err, replace)
                != AcceptedDataConnectionModes::Scalar) {
                return DropMode::None;
            }
        }
        return DropMode::BothAxes;
    }
    else if (nodes.size() != 1) {
        return DropMode::None;
    }
    if (getChildNode(0)->canInsertDataNodes(nodes, -1, err, replace)
        == AcceptedDataConnectionModes::Scalar) {
        return DropMode::OnlyX;
    }
    if (getChildNode(1)->canInsertDataNodes(nodes, -1, err, replace)
        == AcceptedDataConnectionModes::Scalar) {
        return DropMode::OnlyY;
    }
    return DropMode::None;
}

void XYGraphSlotModel::reconnectVariables()
{
    widget.clearVariables();
    auto *model = getRoot().dataModel;
    for (int i = 0; i < getChildCount(); ++i) {
        const auto node = getChildNode(i);
        if (node->getChildCount() == 1) {
            auto slot = static_cast<XYSlot *>(node->getChildNode(0));
            auto var = model->findDataNode(slot->url);
            if (var && !var->getVariable().empty()) {
                widget.addVariable(
                        var->getVariable(),
                        slot->selector.toPdComSelector(),
                        slot->period,
                        slot->scale,
                        slot->offset);
            }
        }
    }
}

bool XYCategory::insertDataNodes(
        QList<DataNode *> const &nodes,
        const Selector &selector,
        int row,
        QString &err,
        bool replace)
{
    if (canInsertDataNodes(nodes, row, err, replace)
        != AcceptedDataConnectionModes::Scalar) {
        return false;
    }

    if (replace)
        clear();

    auto slot = new XYSlot(this);
    slot->url = nodes[0]->nodeUrl();
    if (!nodes[0]->getVariable().isWriteable()) {
        slot->period = std::chrono::milliseconds {100};
    }
    slot->selector = selector;
    static_cast<XYGraphSlotModel *>(getParentNode())->reconnectVariables();
    return true;
}

void XYCategory::fromJson(const QJsonValue &value)
{
    const auto array = value.toArray();
    if (array.size() == 1) {
        auto node = new XYSlot(this);
        node->fromJson(array[0]);
    }
}

}  // namespace

/****************************************************************************/

QString XYGraphPlugin::name() const
{
    return tr("XY Graph");
}

/****************************************************************************/

QIcon XYGraphPlugin::icon() const
{
    return QIcon(":/images/plugin-xygraph.svg");
}

/****************************************************************************/

SlotModel *XYGraphPlugin::createSlotModel(
        QWidget *widget,
        SlotModelCollection &parent,
        WidgetContainer &container) const
{
    auto pdwidget = qobject_cast<Pd::XYGraph *>(widget);
    if (!pdwidget) {
        return nullptr;
    }

    return new XYGraphSlotModel(parent, container, *pdwidget);
}

/****************************************************************************/


/****************************************************************************/

QWidget *XYGraphPlugin::createWidget(QWidget *parent) const
{
    return new Pd::XYGraph(parent);
}

/****************************************************************************/

#include "XYGraphPlugin.moc"
