/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2019-2023  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "TableViewPlugin.h"

#include "TableViewPluginPrivate.h"
#include "TableViewPluginDialog.h"

#include "DataSlot.h"
#include "DataModel.h"
#include "DataNode.h"
#include "SlotModel.h"

#include <QtPdWidgets2/TableView.h>

#include <QDebug>
#include <QJsonObject>
#include <QJsonArray>

/****************************************************************************/

QString TableViewPlugin::name() const
{
    return tr("Table View");
}

/****************************************************************************/

QIcon TableViewPlugin::icon() const
{
    return QIcon(":/images/plugin-table.svg");
}

/****************************************************************************/

void TableViewPlugin::openEditor(
        QWidget *widget,
        void *,
        SlotModel *slotModel,
        QWidget *parent)
{
    auto table(dynamic_cast<Pd::TableView *>(widget));
    if (!table) {
        return;
    }
    auto model(static_cast<TableViewSlotModel *>(slotModel));

    auto dialog = new TableViewPluginDialog(model, parent);
    connect(dialog, &QDialog::finished, dialog, &QObject::deleteLater);
    dialog->open();
}

/****************************************************************************/

SlotModel *TableViewPlugin::createSlotModel(
        QWidget *widget,
        SlotModelCollection &parent,
        WidgetContainer &container) const
{
    auto *pdwidget = qobject_cast<Pd::TableView*>(widget);
    if (!widget)
        return nullptr;
    return new TableViewSlotModel(parent, container, pdwidget);
}

/****************************************************************************/

QWidget *TableViewPlugin::createWidget(QWidget *parent) const
{
    return new Pd::TableView(parent);
}

/****************************************************************************/
