/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2018  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "DialPlugin.h"

#include "DialPluginDialog.h"

#include "DataSlot.h"
#include "SlotModel.h"
#include "DataModel.h"
#include "DataNode.h"

#include <QtPdWidgets2/Dial.h>

#include <QDebug>
#include <QJsonObject>
#include <QJsonArray>

/****************************************************************************/

QString DialPlugin::name() const
{
    return tr("Dial / Analog Gauge");
}

/****************************************************************************/

QIcon DialPlugin::icon() const
{
    return QIcon(":/images/plugin-dial.svg");
}

/****************************************************************************/

class ValueSlotCategory: public SimpleSlotModelBase<SlotNode>
{
        Q_DECLARE_TR_FUNCTIONS(ValueSlotCategory)
    public:
        ValueSlotCategory(QtPdCom::ScalarSubscriber &sub, SlotNode *parent):
            SimpleSlotModelBase(sub, parent, tr("Display"))
        {}
};

/****************************************************************************/

class SetpointSlotCategory: public SimpleSlotModelBase<SlotNode>
{
        Q_DECLARE_TR_FUNCTIONS(SetpointSlotCategory)

    public:
        SetpointSlotCategory(
                QtPdCom::ScalarSubscriber &sub,
                SlotNode *parent):
            SimpleSlotModelBase(sub, parent, tr("Setpoint"))
        {}

        SlotModel::AcceptedDataConnectionModes canInsertDataNodes(
                QList<DataNode *> const &nodes,
                int row,
                QString &err,
                bool replace) const override
        {
            if (SimpleSlotModelBase::canInsertDataNodes(nodes, row, err, replace)
                != SlotModel::AcceptedDataConnectionModes::Scalar) {
                return SlotModel::AcceptedDataConnectionModes::None;
            }

            const auto var = nodes[0]->getVariable();
            if (!var.isWriteable()) {
                err = tr("Please select a writeable variable!");
                return SlotModel::AcceptedDataConnectionModes::None;
            }
            return AcceptedDataConnectionModes::Scalar;
        }
};

/****************************************************************************/

class DialSlotModel: public SlotModel
{
        Q_DECLARE_TR_FUNCTIONS(DialSlotModel)

    public:
        DialSlotModel(
                SlotModelCollection &parent,
                WidgetContainer &container,
                Pd::Dial &dial):
            SlotModel(parent, container)
        {
            new ValueSlotCategory(dial.currentValue, this);
            new SetpointSlotCategory(dial.setpointValue, this);
        }

        void clear() override
        {
            if (getChildCount() != 2) {
                qFatal("Invalid number of SlotNodes");
            }

            static_cast<ValueSlotCategory *>(getChildNode(0))->clear();
            static_cast<SetpointSlotCategory *>(getChildNode(1))->clear();
        }

        // default handlers, when DataNode is dropped onto the widget
        AcceptedDataConnectionModes canInsertDataNodes(
                QList<DataNode *> const &nodes,
                int row,
                QString &err,
                bool replace) const override
        {
            if (getChildCount() != 2) {
                qFatal("Invalid number of SlotNodes");
            }

            if (row != -1) {
                err = tr("Please drop the variables onto the widget itself "
                         "or onto one of its categories.");
                return AcceptedDataConnectionModes::None;
            }
            // item itself
            if (nodes.size() == 2) {
                // two variables
                const auto ans1 = getChildNode(0)->canInsertDataNodes(
                        {nodes[0]},
                        0,
                        err, replace);
                if (ans1 != AcceptedDataConnectionModes::Scalar) {
                    return AcceptedDataConnectionModes::None;
                }
                return getChildNode(1)->canInsertDataNodes(
                        {nodes[1]},
                        0,
                        err, replace);
            }
            else if (nodes.size() != 1) {
                err = tr("This widget accepts up to two variables.");
                return AcceptedDataConnectionModes::None;
            }
            const auto ans1 =
                    getChildNode(0)->canInsertDataNodes(nodes, 0, err, replace);
            if (ans1 != AcceptedDataConnectionModes::None) {
                return ans1;
            }
            return getChildNode(1)->canInsertDataNodes(nodes, 0, err, replace);
        }


        bool insertDataNodes(
                QList<DataNode *> const &nodes,
                const Selector &selector,
                int row,
                QString &err,
                bool replace) override
        {
            if (getChildCount() != 2) {
                qFatal("Invalid number of SlotNodes");
            }

            if (canInsertDataNodes(nodes, row, err, replace)
                != AcceptedDataConnectionModes::Scalar) {
                return false;
            }

            if (nodes.size() == 2) {
                return getChildNode(0)->insertDataNodes(
                               {nodes[0]},
                               selector,
                               row,
                               err, replace)
                        && getChildNode(1)->insertDataNodes(
                                {nodes[1]},
                                selector,
                                0,
                                err, replace);
            }

            if (getChildNode(0)->canInsertDataNodes(nodes, 0, err, replace)
                == AcceptedDataConnectionModes::Scalar) {
                return getChildNode(0)
                        ->insertDataNodes(nodes, selector, 0, err, replace);
            }
            return getChildNode(1)->insertDataNodes(nodes, selector, 0, err, replace);
        }

        void fromJson(const QJsonValue &val) override
        {
            const auto obj = val.toObject();
            getChildNode(0)->fromJson(obj["value"]);
            getChildNode(1)->fromJson(obj["setpoint"]);
        }

        QJsonValue toJson() const override
        {
            if (getChildCount() != 2)
                return {};
            QJsonObject ans;
            ans["value"] = getChildNode(0)->toJson();
            ans["setpoint"] = getChildNode(1)->toJson();
            return ans;
        }

        void nodeFlags(Qt::ItemFlags &flags, int section) const override
        {
            SlotModel::nodeFlags(flags, section);
            if (section == UrlColumn && getChildCount() == 2
                && (getChildNode(0)->getChildCount() == 0
                    || getChildNode(1)->getChildCount() == 0)) {
                flags |= Qt::ItemIsDropEnabled;
            }
        }
};


/****************************************************************************/

void *DialPlugin::createPrivateData(QWidget *widget) const
{
    Pd::Dial *dial = dynamic_cast<Pd::Dial *>(widget);
    if (!dial) {
        qWarning() << __func__ << "failed to cast";
        return NULL;
    }

    QGradientStops *stops = new QGradientStops();
    dial->setGradientStops(*stops);
    return stops;
}

/****************************************************************************/

void DialPlugin::readPrivateData(
        const QJsonObject &json,
        QWidget *widget,
        void *data) const
{
    Pd::Dial *dial = dynamic_cast<Pd::Dial *>(widget);
    if (!dial) {
        qWarning() << __func__ << "failed to cast";
        return;
    }

    auto stops = static_cast<QGradientStops *>(data);

    stops->clear();

    auto array(json["gradientStops"].toArray());

    foreach (const QJsonValue &value, array) {
        QJsonObject obj(value.toObject());
        stops->append(QGradientStop(
                obj["value"].toDouble(),
                QColor(obj["color"].toString())));
    }

    dial->setGradientStops(*stops);
}

/****************************************************************************/

QJsonObject DialPlugin::writePrivateData(QWidget *, const void *data) const
{
    auto stops = static_cast<const QGradientStops *>(data);

    QJsonArray stopsArray;

    foreach (auto stop, *stops) {
        QJsonObject value;
        value["value"] = stop.first;
        value["color"] = stop.second.name(QColor::HexArgb);
        stopsArray.append(value);
    }

    QJsonObject ret;
    ret["gradientStops"] = stopsArray;
    return ret;
}

/****************************************************************************/

void DialPlugin::deletePrivateData(QWidget *widget, void *data) const
{
    Pd::Dial *dial = dynamic_cast<Pd::Dial *>(widget);
    if (dial) {
        dial->setGradientStops(QGradientStops());
    }

    auto stops = static_cast<QGradientStops *>(data);
    delete stops;
}

/****************************************************************************/

void DialPlugin::openEditor(
        QWidget *widget,
        void *data,
        SlotModel *,
        QWidget *parent)
{
    Pd::Dial *dial = dynamic_cast<Pd::Dial *>(widget);
    if (!dial) {
        return;
    }

    auto stops = static_cast<QGradientStops *>(data);

    auto dialog = new DialPluginDialog(stops, parent);
    connect(dialog, &QDialog::finished, dialog, &QObject::deleteLater);
    connect(dialog, &QDialog::accepted, dial, [stops, dial]() {
        dial->setGradientStops(*stops);
    });

    dialog->open();
}

/****************************************************************************/

SlotModel *DialPlugin::createSlotModel(
        QWidget *widget,
        SlotModelCollection &parent,
        WidgetContainer &container) const
{
    auto *dial = qobject_cast<Pd::Dial *>(widget);
    if (!dial) {
        qFatal("Widget is not an instance of Pd::Dial");
    }

    return new DialSlotModel(parent, container, *dial);
}

/****************************************************************************/

QWidget *DialPlugin::createWidget(QWidget *parent) const
{
    return new Pd::Dial(parent);
}

/****************************************************************************/
