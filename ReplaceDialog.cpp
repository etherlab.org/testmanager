/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2018-2019  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "ReplaceDialog.h"

#include "DataSource.h"

/****************************************************************************/

ReplaceDialog::ReplaceDialog(
        DataModel *dataModel,
        DataSource *dataSource,
        QWidget *parent):
    ConnectDialog(dataModel, parent),
    oldDataSource(dataSource)
{
    QString url(dataSource->getUrl().toString());
    labelAddress->setText(tr("New URL for datasource %1:").arg(url));

    radioButtonSession->setVisible(true);
    radioButtonPermanent->setVisible(true);
    setWindowTitle(tr("Replace data source..."));
}

/****************************************************************************/

ReplaceDialog::Mode ReplaceDialog::getMode() const
{
    return radioButtonPermanent->isChecked() ? Permanent : Temporary;
}

/****************************************************************************/

DataSource *ReplaceDialog::createDataSource() const
{
    return new DataSource(dataModel, oldDataSource->getUrl(), getUrl());
}

/****************************************************************************/
