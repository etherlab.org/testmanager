/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2023  Bjarne von Horn <vh at igh dot de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include <QTreeView>

#include "ColorDelegate.h"
#include "ScaleDelegate.h"
#include "SelectorDelegate.h"

class MainWindow;

class SlotView : public QTreeView
{
    Q_OBJECT

    public:
        explicit SlotView(QWidget *parent = nullptr);

        void keyPressEvent(QKeyEvent *event) override;

        void setMainWindow(MainWindow *window);

    protected:
        void dropEvent(QDropEvent *event) override;
        void dragMoveEvent(QDragMoveEvent *event) override;

    private:

        ScaleDelegate scaleDelegate;
        SelectorDelegate selectorDelegate;
        ColorDelegate colorDelegate;
        MainWindow *mainWindow;

        void contextMenu(const QPoint &);
};
