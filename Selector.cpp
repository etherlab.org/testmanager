/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2023 Bjarne von Horn <vh at igh dot de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "Selector.h"

#include <QDebug>
#include <QJsonArray>

bool Selector::fromJson(const QJsonValue &val)
{
    parameters_ = val.toObject();
    if (parameters_.isEmpty())
        // Default selector
        return true;
    if (parameters_["type"] == "Scalar" && parameters_["indices"].isArray())
        return true;
    return false;
}

PdCom::Selector Selector::toPdComSelector() const
{
    if (parameters_.isEmpty())
        return {};
    if (parameters_["type"] == "Scalar") {
        std::vector<int> indices;
        for (const auto val : parameters_["indices"].toArray())
            indices.push_back(val.toInt());
        return PdCom::ScalarSelector(indices);
    }
    qFatal("Unknown Selector type.");
}

Selector Selector::scalarSelector(const std::vector<int> &indices)
{
    Selector ans;
    ans.parameters_["type"] = "Scalar";
    QJsonArray a;
    for (const auto i : indices)
        a.append(i);
    ans.parameters_["indices"] = a;
    return ans;
}

QVariant Selector::itemData(Qt::ItemDataRole role) const
{
    if (parameters_["type"] == "Scalar") {
        if (role == Qt::DisplayRole) {
            return getScalarIndices();
        } else if (role == Qt::ToolTipRole) {
            return tr("Scalar, Index: ") + getScalarIndices();
        }
    } else if (parameters_.isEmpty() && role == Qt::ToolTipRole) {
        return tr("Default Selection");
    }
    return QVariant();
}

QString Selector::getScalarIndices() const
{
    const auto a = parameters_["indices"].toArray();
    if (a.isEmpty())
        return "[]";
    QString ans = QStringLiteral("[%1").arg(a[0].toInt());
    for (int i = 1; i < a.size(); ++i) {
        ans.append(QStringLiteral(", %1").arg(a[i].toInt()));
    }
    ans.append("]");
    return ans;
}
