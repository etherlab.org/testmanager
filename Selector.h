/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2023 Bjarne von Horn <vh at igh dot de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef SELECTOR_H
#define SELECTOR_H

#include <QCoreApplication>
#include <QJsonObject>
#include <QJsonValue>
#include <QVariant>

#include <pdcom5/Selector.h>

class Selector
{
    Q_DECLARE_TR_FUNCTIONS(Selector)

public:

    QJsonObject toJson() const { return parameters_; }
    bool fromJson(const QJsonValue &val);

    PdCom::Selector toPdComSelector() const;

    static Selector scalarSelector(const std::vector<int> &indices);

    QVariant itemData(Qt::ItemDataRole role) const;

private:
    QJsonObject parameters_;

    QString getScalarIndices() const;
};

#endif // SELECTOR_H
