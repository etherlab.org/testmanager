#!/bin/bash

BUILD_DIR=build-leap
REPO=$(realpath .)

docker run --rm \
    -v $REPO:$REPO \
    --workdir $REPO/$BUILD_DIR \
    --user $(id -u):$(id -g) \
    docker:5000/testmanager-leap \
    make -j$(nproc)
