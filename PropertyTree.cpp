/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2019  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "PropertyTree.h"

#include "PropertyModel.h"

#include <QEvent>

/****************************************************************************/

PropertyTree::PropertyTree(QWidget *parent):
    QTreeView(parent)
{
    updateBrightness();
}

/****************************************************************************/

void PropertyTree::updateBrightness()
{
    QColor c(palette().color(QPalette::Text));
    bool brightForeground =
        qRound(0.3 * c.redF() + 0.59 * c.greenF() + 0.11 * c.blueF());

    PropertyModel *m = dynamic_cast<PropertyModel *>(model());
    if (m) {
        m->setBrightForeground(brightForeground);
    }
}

/****************************************************************************/

bool PropertyTree::event(QEvent *event)
{
    if (event->type() == QEvent::PaletteChange) {
        updateBrightness();
    }

    return QTreeView::event(event);
}

/****************************************************************************/
