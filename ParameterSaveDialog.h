/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2023  Daniel Ramirez <dr@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "ui_ParameterSaveDialog.h"

#include "Parameter.h"
#include "ParameterModel.h"

#include <QDialog>
#include <QPersistentModelIndex>

/****************************************************************************/

class MainWindow;
class ParameterModel;
class Parameter;

class ParameterSaveDialog:
    public QDialog,
    public Ui::ParameterSaveDialog
{
    Q_OBJECT

    public:
        ParameterSaveDialog(ParameterModel *, QWidget *parent = 0);
        ~ParameterSaveDialog();

    private:
        ParameterModel * model;
        QPersistentModelIndex contextIndex;

    private slots:
        void openSaveDialog();

};

/****************************************************************************/
