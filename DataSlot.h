/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2018  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef DATASLOT_H
#define DATASLOT_H

#include "Selector.h"
#include "SlotNode.h"

#include <QtPdCom1/Transmission.h>
#include <pdcom5/Selector.h>

#include <QUrl>
#include <QColor>
#include <chrono>

/****************************************************************************/

namespace QtPdCom {
class ScalarSubscriber;
}

class BasicDataSlot : public SlotNode
{
    public:
        using SlotNode::SlotNode;

        QUrl url;
        QString alias;

        QIcon getIcon(const DataModel *) const;

        // virtual from SlotNode
        void fromJson(const QJsonValue &);
        QJsonValue toJson() const;
        QVariant nodeData(int, int, const DataModel *) const;
        void nodeFlags(Qt::ItemFlags &, int) const;
        bool nodeSetData(int, const QVariant &);

        void replaceUrl(const QUrl &, const QUrl &);

    protected:
        void appendDataSources(DataModel *) override;
};

class DataSlot:
    public BasicDataSlot
{
    public:
        enum Option {
            NoFlags = 0,
            SupportsColor = 1,
            DoesNotSupportTau = 2,
            DoesNotSupportSelector = 4,
        };
        Q_DECLARE_FLAGS(Flags, Option)

        DataSlot(SlotNode *, int pos = -1, Flags flags = NoFlags);

        QtPdCom::Transmission period;
        Selector selector;
        double scale;
        double offset;
        double tau;
        QColor color;


        // virtual from SlotNode
        void fromJson(const QJsonValue &);
        QJsonValue toJson() const;
        QVariant nodeData(int, int, const DataModel *) const;
        void nodeFlags(Qt::ItemFlags &, int) const;
        bool nodeSetData(int, const QVariant &);


    private:
        Flags flags_;

        DataSlot();

        // virtual from SlotNode
        void countColors(QMap<QRgb, unsigned int> &) const;

};


class ScalarSubscriberDataSlot final: public DataSlot
{
        Q_DECLARE_TR_FUNCTIONS(ScalarSubscriberDataSlot)

    public:
        ScalarSubscriberDataSlot(
                SlotNode *parent,
                QtPdCom::ScalarSubscriber &sub):
            DataSlot(parent),
            subscriber(sub)
        {}

        bool nodeSetData(int col, const QVariant &data) override;

        bool supportsRemovingByUser() const;

        ~ScalarSubscriberDataSlot();

        void disconnectDataSources() override;
        void appendDataSources(DataModel *dataModel) override;

    private:
        QtPdCom::ScalarSubscriber &subscriber;
        QUrl url_backup;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(DataSlot::Flags)

/****************************************************************************/

#endif
