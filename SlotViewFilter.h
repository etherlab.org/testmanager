/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2023  Bjarne von Horn <vh@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef SLOTVIEWFILTER_H
#define SLOTVIEWFILTER_H

#include <QSortFilterProxyModel>
#include <QSet>

class MainWindow;
class WidgetContainer;

class SlotViewFilter : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    SlotViewFilter(MainWindow &mw, QObject *parent = nullptr);

public slots:
    void widgetSelectionChanged();

protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;

    MainWindow *const mainWindow;
    QSet<WidgetContainer*> selectedWidgets;
};

#endif // SLOTVIEWFILTER_H
