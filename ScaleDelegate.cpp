/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2019  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "ScaleDelegate.h"

#include <QDebug>
#include <QComboBox>
#include <QLineEdit>

/****************************************************************************/

double ScaleDelegate::constants[] = {
    1.0, // unity
    9.549296585513721, // 30.0 / M_PI ~= rpm
    57.29577951308232, // 180.0 / M_PI ~= °
    1e-5, // bar
};

/****************************************************************************/

QString ScaleDelegate::name(unsigned int idx)
{
    switch (idx) {
        case 0:
            return tr("1");
        case 1:
            return tr("rpm");
        case 2:
            return tr("°");
        case 3:
            return tr("bar");
        default:
            return QString();
    }
}

/****************************************************************************/

ScaleDelegate::ScaleDelegate(QObject *parent):
    QStyledItemDelegate(parent)
{
}

/****************************************************************************/

QWidget *ScaleDelegate::createEditor(QWidget *parent,
        const QStyleOptionViewItem &,
        const QModelIndex &) const
{
    QComboBox *cb = new QComboBox(parent);
    cb->setEditable(true);
    cb->setFrame(false);
    cb->setFocusPolicy(Qt::StrongFocus);
    cb->setInsertPolicy(QComboBox::NoInsert);
    connect(cb, SIGNAL(activated(int)), this, SLOT(activated(int)));

    unsigned int numConstants = sizeof(constants) / sizeof(constants[0]);
    for (unsigned int i = 0; i < numConstants; i++) {
        cb->addItem(name(i), constants[i]);
    }

    cb->model()->sort(0);

    return cb;
}

/****************************************************************************/

void ScaleDelegate::setEditorData(QWidget *editor,
        const QModelIndex &index) const
{
    QComboBox *cb = qobject_cast<QComboBox*>(editor);
    if (!cb) {
        qWarning() << "no combo box";
        return;
    }

    QVariant scale = index.data(Qt::UserRole);
    int idx = cb->findData(scale);
    if (idx >= 0) {
        cb->setCurrentIndex(idx);
    }
    else {
        QString editText = index.data(Qt::EditRole).toString();
        cb->setEditText(editText);
    }

    cb->lineEdit()->selectAll();
}

/****************************************************************************/

void ScaleDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
        const QModelIndex &index) const
{
    QComboBox *cb = qobject_cast<QComboBox*>(editor);
    if (!cb) {
        qWarning() << "no combo box";
        return;
    }

    int idx = cb->findText(cb->currentText());
    if (idx >= 0) {
        double scale = cb->itemData(idx).toDouble();
        QString val = QLocale().toString(scale, 'g', 16);
        model->setData(index, val, Qt::EditRole);
    }
    else {
        model->setData(index, cb->currentText(), Qt::EditRole);
    }
}

/****************************************************************************/

void ScaleDelegate::updateEditorGeometry(QWidget *editor,
        const QStyleOptionViewItem &option, const QModelIndex &) const
{
    QComboBox *cb = qobject_cast<QComboBox*>(editor);
    if (!cb) {
        qWarning() << "no combo box";
        return;
    }

    cb->setGeometry(option.rect);
}

/****************************************************************************/

QSize ScaleDelegate::sizeHint(const QStyleOptionViewItem &,
                const QModelIndex &) const
{
    return QSize(30, 25);
}

/****************************************************************************/

QString ScaleDelegate::text(double value)
{
    unsigned int i, numConstants = sizeof(constants) / sizeof(constants[0]);
    for (i = 0; i < numConstants; i++) {
        if (constants[i] == value) {
            return name(i);
        }
    }

    return QString();
}

/****************************************************************************/

void ScaleDelegate::activated(int idx)
{
    QComboBox *cb = qobject_cast<QComboBox*>(sender());

    if (idx >= 0) {
        emit commitData(cb);
        emit closeEditor(cb);
    }
}

/****************************************************************************/
