/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2023  Bjarne von Horn <vh@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "BroadcastDialog.h"

#include "DataModel.h"
#include "DataSource.h"
#include "ui_BroadcastDialog.h"

#include <QtPdCom1/BroadcastModel.h>

BroadcastDialog::BroadcastDialog(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BroadcastDialog),
    broadcasts(new QtPdCom::BroadcastModel(this))
{
    ui->setupUi(this);
    ui->tvReceived->setModel(broadcasts);
}

BroadcastDialog::~BroadcastDialog()
{
    delete ui;
}

void BroadcastDialog::setDataModel(DataModel &m)
{
    ui->cbSource->setModel(&m);
    model = &m;
}

void BroadcastDialog::on_btClear_clicked()
{
    broadcasts->clear();
}

void BroadcastDialog::on_cbSource_currentIndexChanged(int int_idx)
{
    const auto model_idx = model->index(int_idx, 0, {});
    source               = model->getDataSource(model_idx);
    if (source) {
        broadcasts->connectProcess(&source->getProcess());
        ui->sendBroadcastWidget->setProcess(&source->getProcess());
    }
    else {
        broadcasts->connectProcess(nullptr);
        ui->sendBroadcastWidget->setProcess(nullptr);
    }
}
