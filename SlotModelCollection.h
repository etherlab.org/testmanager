/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2018  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef SLOT_MODEL_COLLECTION_H
#define SLOT_MODEL_COLLECTION_H

/****************************************************************************/

class MainWindow;
class SlotModel;

#include "SlotNode.h"

#include <QAbstractItemModel>
#include <memory>

/****************************************************************************/

class SlotModelCollection:
    public QAbstractItemModel,
    public SlotNode
{
    Q_OBJECT

    public:
        SlotModelCollection(const DataModel *, MainWindow *);
        ~SlotModelCollection();

        void clear();

        Qt::DropActions supportedDropActions() const override
        {
            return Qt::CopyAction | Qt::MoveAction;
        }

        Qt::DropActions supportedDragActions() const override
        {
            return Qt::CopyAction;
        }

        QStringList mimeTypes() const override;

        bool canDropMimeData(
                const QMimeData *data,
                Qt::DropAction action,
                int row,
                int column,
                const QModelIndex &parent) const override;
        bool dropMimeData(
                const QMimeData *data,
                Qt::DropAction action,
                int row,
                int column,
                const QModelIndex &parent);


        void remove(const QModelIndex &);
        bool removeRows(
                int row,
                int count,
                const QModelIndex &parent = QModelIndex()) override;

        QModelIndex index(int row, int column, const QModelIndex &parent) const;
        QModelIndex parent(const QModelIndex &) const;
        int rowCount(const QModelIndex &) const;
        int columnCount(const QModelIndex &) const;
        QVariant data(const QModelIndex &, int) const;
        QVariant headerData(int, Qt::Orientation, int) const;
        Qt::ItemFlags flags(const QModelIndex &) const;
        bool setData(const QModelIndex &, const QVariant &, int);

        const DataModel * const dataModel;
        MainWindow * const mainWindow;


        struct SlotModelDeleter
        {
                void operator()(SlotModel *) const;
        };

        using SlotModelPointer = std::unique_ptr<SlotModel, SlotModelDeleter>;

    private:
        friend class SlotNode;

        SlotModelCollection();

        SlotNode::AcceptedDataConnectionModes parseMimeData(
                const QMimeData *data,
                Qt::DropAction action,
                int& row,
                int column,
                const QModelIndex &parent,
                QString &err,
                QList<DataNode *>& nodes) const;

        void deleteRecursive(SlotNode *node);
};

/****************************************************************************/

#endif
