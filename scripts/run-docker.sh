#!/bin/bash

# ---------------------------------------------------------------------------
#
# Testmanager - Graphical Automation and Visualisation Tool
#
# Copyright (C) 2022  Jonathan Grahl <gn@igh.de>
#               2023  Florian Pose <fp@igh.de>
#
# This file is part of Testmanager.
#
# Testmanager is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Testmanager is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with Testmanager. If not, see <http://www.gnu.org/licenses/>.
#
# ---------------------------------------------------------------------------

function usage() {
    echo -e "    Usage: $(basename -- "$0") [OPTIONS]
    Options:
    -p,  --prepare     Additional prepare the build directory before make.
    -b,  --build       Build the gui executable.
    -r,  --run         Run the executable in the docker environment.
    -s,  --shell       Start a shell in the docker environment.
        --stop        Stop and remove all corresponding container.
    -h,  --help        Show this help."
}

# ---------------------------------------------------------------------------

function removeDocker() {
    # In case an old instance is running jet...
    docker container rm -f $NAME >/dev/null 2>&1
}

# ---------------------------------------------------------------------------

function prepare() {
    mkdir -p "${TOP}"/build
    docker run \
        --user $(id -u):$(id -g) \
        --volume "${TOP}":/git \
        --workdir /git/build \
        --name $NAME \
        --rm \
        $IMAGE \
        qmake-qt5 DISABLE_CSS=1 CONFIG+=debug ../testmanager.pro
}

# ---------------------------------------------------------------------------

function build() {
    docker run \
        --user $(id -u):$(id -g) \
        --volume "${TOP}":/git \
        --workdir /git/build \
        --name $NAME \
        --rm \
        $IMAGE \
        make -j$(nproc)

    exit $?
}

# ---------------------------------------------------------------------------

function run() {
    xhost +
    docker run \
        --user $(id -u):$(id -g) \
        --volume "${TOP}":/git \
        --workdir /git/build \
        --volume /tmp/.X11-unix:/tmp/.X11-unix:rw \
        --env DISPLAY=$DISPLAY \
        --env QT_GRAPHICSSYSTEM=native \
        --env XDG_CONFIG_HOME=/git/build/xdg \
        --add-host host.docker.internal:host-gateway \
        --name $NAME \
        --rm \
        $IMAGE \
        ./testmanager
}

# ---------------------------------------------------------------------------s

function shell() {
    xhost +
    docker run \
        --interactive \
        --tty \
        --rm \
        --user $(id -u):$(id -g) \
        --volume "${TOP}":/git \
        --workdir /git/build \
        --volume /tmp/.X11-unix:/tmp/.X11-unix:rw \
        --env DISPLAY=$DISPLAY \
        --env QT_GRAPHICSSYSTEM=native \
        --env XDG_CONFIG_HOME=/git/build/xdg \
        --add-host host.docker.internal:host-gateway \
        --name $NAME \
        $IMAGE
}

# ---------------------------------------------------------------------------

# container name
NAME=testmanager
IMAGE=registry.gitlab.com/etherlab.org/build-container-factory/leap-15.3:qt5pdwidgets

# toplevel path of this git repository
TOP=$(git rev-parse --show-toplevel)

# ---------------------------------------------------------------------------

if [ $# -lt 1 ]; then
    set -- "$@" '-h'
fi

VALID_ARGS=$(getopt \
    -o pbrsh \
    --long prepare,build,run,shell,stop,help \
    -- "$@")

eval set -- "$VALID_ARGS"

while [ : ]; do
    case "$1" in
    '-p' | '--prepare')
        removeDocker && prepare && build
        shift
        ;;
    '-b' | '--build')
        removeDocker && build
        shift
        ;;
    '-r' | '--run')
        removeDocker && run
        shift
        ;;
    '-s' | '--shell')
        removeDocker && shell
        shift
        ;;
    '--stop')
        removeDocker
        shift
        ;;
    '-h' | '--help' | : | ?)
        usage
        exit 1
        ;;
    --)
        shift
        break
        ;;
    esac
done

# ---------------------------------------------------------------------------
