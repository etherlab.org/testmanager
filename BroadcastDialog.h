/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2023  Bjarne von Horn <vh@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef BROADCASTDIALOG_H
#define BROADCASTDIALOG_H

namespace QtPdCom {
  class BroadcastModel;
}
class DataModel;
class DataSource;

#include <QWidget>

namespace Ui {
class BroadcastDialog;
}

class BroadcastDialog : public QWidget
{
    Q_OBJECT

  public:
    explicit BroadcastDialog(QWidget *parent = nullptr);
    ~BroadcastDialog();

    void setDataModel(DataModel &);

  private slots:
    void on_cbSource_currentIndexChanged(int);
    void on_btClear_clicked();

  private:
    Ui::BroadcastDialog *ui;
    DataModel *model;
    DataSource *source = nullptr;
    QtPdCom::BroadcastModel *broadcasts;
};

#endif  // BROADCASTDIALOG_H
