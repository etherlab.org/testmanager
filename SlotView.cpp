/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2023  Bjarne von Horn <vh at igh dot de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "SlotView.h"

#include <QItemSelectionModel>
#include <QKeyEvent>
#include <QList>
#include <QMenu>
#include <QPersistentModelIndex>
#include <QHeaderView>

#include "MainWindow.h"
#include "SlotNode.h"

SlotView::SlotView(QWidget *parent):
    QTreeView(parent),
    scaleDelegate(this),
    selectorDelegate(this),
    colorDelegate(this)
{
    setItemDelegateForColumn(SlotNode::SelectorColumn, &selectorDelegate);
    setItemDelegateForColumn(SlotNode::ScaleColumn, &scaleDelegate);
    setItemDelegateForColumn(SlotNode::ColorColumn, &colorDelegate);

    connect(this, &QWidget::customContextMenuRequested,
            this, &SlotView::contextMenu);
}

void SlotView::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
        case Qt::Key_Delete:
            break;
        default:
            QTreeView::keyPressEvent(event);
            return;
    }
    const auto sm = selectionModel();
    if (!sm) {
        return;
    }

    QList<QPersistentModelIndex> indices;
    for (const auto &index : sm->selectedRows()) {
        indices.append(QPersistentModelIndex(index));
    }

    const auto m = model();
    for (const auto &index : indices) {
        if (!index.isValid()) {
            continue;
        }
        m->removeRows(index.row(), 1, index.parent());
    }

    event->accept();
}

void SlotView::setMainWindow(MainWindow *window)
{
    mainWindow = window;
    selectorDelegate.dataModel = mainWindow->getDataModel();

    header()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
}

static QVariant findUrl(QAbstractItemModel *model, QModelIndex index)
{
    index = model->index(index.row(), SlotNode::UrlColumn, index.parent());
    return model->data(index, Qt::ToolTipRole);
}

static void applyRecursive(QAbstractItemModel *model, QVariant data, const QModelIndex& src, QModelIndex dst)
{
    if (src != dst && model->flags(dst) & Qt::ItemIsEditable) {
        model->setData(dst, data, Qt::EditRole);
    }
    for(int row = 0; row < model->rowCount(dst); ++row)
    {
        applyRecursive(model, data, src, model->index(row, src.column(), dst));
    }

}

static void applyToSelected(QAbstractItemModel *model, const QModelIndex& src, QModelIndexList const& selected)
{
    const auto data = model->data(src, Qt::EditRole);
    for (const auto& idx : selected) {
        applyRecursive(model, data, src, idx);
    }
}

void SlotView::contextMenu(const QPoint &point)
{
    const auto index = indexAt(point);

    bool applyEnabled = SlotNode::AliasColumn <= index.column() && index.column() <= SlotNode::ColorColumn;
    if (!(model()->flags(index) & Qt::ItemIsEditable))
        applyEnabled = false;
    const auto url = findUrl(model(), index);
    const auto sm = selectionModel();
    const auto selected = sm ? sm->selectedRows(index.isValid() ? index.column() : 0) : QModelIndexList();

    const auto menu = new QMenu(this);
    if (applyEnabled && selected.size() <= 1) {
        const QString header(model()->headerData(index.column(),
                        Qt::Horizontal, Qt::DisplayRole).toString());
        const auto action = menu->addAction(tr("Apply %1 to all").arg(header));
        connect(action, &QAction::triggered, menu, [index,this]() {
            const auto data = model()->data(index, Qt::EditRole);
            applyRecursive(model(), data, index, {});
        });
    }
    else if (applyEnabled && selected.size() > 1) {
        const QString header(model()->headerData(index.column(),
                        Qt::Horizontal, Qt::DisplayRole).toString());
        const auto action = menu->addAction(tr("Apply %1 to selected and their children").arg(header));
        connect(action, &QAction::triggered, menu, [index, selected, this]() {
            applyToSelected(model(), index, selected);
        });
    }
    if (mainWindow->getEditMode() && selected.size() > 0) {
        const auto action = menu->addAction(tr("Remove selected"));
        action->setIcon(QIcon(":/images/list-remove.svg"));
        connect(action, &QAction::triggered, menu, [selected,this]() {
            QList<QPersistentModelIndex> indices;
            for (const auto &index : selected) {
                indices.append(QPersistentModelIndex(index));
            }

            const auto m = model();
            for (const auto &index : indices) {
                if (!index.isValid()) {
                    continue;
                }
                m->removeRows(index.row(), 1, index.parent());
            }
        });
    }
    if (index.isValid() && url.canConvert<QUrl>() && selected.size() <= 1) {
        const auto action = menu->addAction(tr("Jump to variable"));
        action->setIcon(QIcon(":/images/go-jump.svg"));
        connect(action, &QAction::triggered, menu, [url, this]{
            QModelIndex index(mainWindow->getDataModel()->indexFromUrl(url.toUrl()));
            mainWindow->sourceTree->selectionModel()->setCurrentIndex(index,
            QItemSelectionModel::SelectCurrent);
        });
    }

    if (menu->actions().empty()) {
        menu->deleteLater();
    } else {
        menu->setAttribute(Qt::WA_DeleteOnClose);
        menu->popup(mapToGlobal(point));
    }
}

void SlotView::dropEvent(QDropEvent *event)
{
    if (event->keyboardModifiers() & Qt::ShiftModifier) {
        event->setDropAction(Qt::MoveAction);
    }

    QTreeView::dropEvent(event);
}

void SlotView::dragMoveEvent(QDragMoveEvent *event)
{
    if (event->keyboardModifiers() & Qt::ShiftModifier) {
        event->setDropAction(Qt::MoveAction);
    }

    QTreeView::dragMoveEvent(event);
}
