/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2023 Bjarne von Horn <vh at igh dot de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/


#pragma once

#include <QDialog>
#include <QList>
#include <QSpinBox>

#include "Selector.h"
#include "SlotNode.h"

namespace Ui {
class SelectorDialog;
}

class DataNode;

class SelectorDialog: public QDialog
{
        Q_OBJECT

    public:
        SelectorDialog(
                QList<DataNode *> nodes,
                SlotNode::AcceptedDataConnectionModes requestedMode,
                QWidget *parent = nullptr);
        explicit SelectorDialog(QWidget *parent = nullptr);
        ~SelectorDialog();

        void changeEvent(QEvent *event) override;

        void setData(DataNode &, QJsonObject selector);

        Selector getSelector() const { return selector; }
        QList<DataNode *> getNodes() const { return nodes; }

        static bool needsToShow(
                const QList<DataNode *> &nodes,
                SlotNode::AcceptedDataConnectionModes requestedMode);

    signals:
        void opened();

    public slots:

        void accept() override;
        void open() override;

    private slots:
        void scalarRadioButtonToggled(bool state);

    private:
        Ui::SelectorDialog *ui;
        Selector selector;
        QList<DataNode *> nodes;
        QVector<QSpinBox *> spinBoxes;

        void prepareScalar();
};
