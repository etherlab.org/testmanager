#!/bin/sh
set -e

if [ -z "$1" -o -z "$2" ]; then
	echo "Usage: $0 <mingw root dir> <destdir>"
	exit 1
fi

LIBDIRS="python*"

MINGW_ROOT=$1
DESTDIR=$2

mkdir -p $DESTDIR/{bin,lib}
cp testmanager.exe $DESTDIR/bin
cp $MINGW_ROOT/bin/*.dll $DESTDIR/bin/
# platform plugins are required to be next to the executable
cp -r $MINGW_ROOT/lib/qt5/plugins/platforms $DESTDIR/bin/
# styles as well
cp -r $MINGW_ROOT/lib/qt5/plugins/styles $DESTDIR/bin/
# and icon engines too
cp -r $MINGW_ROOT/lib/qt5/plugins/iconengines $DESTDIR/bin/
# imageformats are needed for stylesheets
cp -r $MINGW_ROOT/lib/qt5/plugins/imageformats $DESTDIR/bin/
for LIBDIR in $LIBDIRS; do
        cp -r $MINGW_ROOT/lib/$LIBDIR $DESTDIR/lib/
done

# workaround for High DPI issue
# https://gitlab.com/etherlab.org/testmanager/-/issues/29
echo -e "[Platforms]\nWindowsArguments = dpiawareness=0" > $DESTDIR/bin/qt.conf
