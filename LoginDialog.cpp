/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2022  Bjarne von Horn <vh@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "LoginDialog.h"

#include <QtPdCom1/LoginManager.h>

LoginDialog::LoginDialog(QtPdCom::LoginManager& manager, QWidget *parent):
    QDialog(parent),
    manager_(&manager)
{
    setupUi(this);
}

void LoginDialog::accept()
{
    QDialog::accept();
    manager_->setPassword(passwordEdit->text());
    manager_->setAuthName(usernameEdit->text());
    manager_->login();
}

void LoginDialog::reject()
{
    QDialog::reject();
    manager_->clearCredentials();
}
