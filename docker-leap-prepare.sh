#!/bin/bash

BUILD_DIR=build-leap
REPO=$(realpath .)

rm -rf $BUILD_DIR
mkdir $BUILD_DIR

docker run --rm \
    -v $REPO:$REPO \
    --workdir $REPO/$BUILD_DIR \
    --user $(id -u):$(id -g) \
    docker:5000/testmanager-leap \
    cmake ..
