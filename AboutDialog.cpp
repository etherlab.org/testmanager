/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2018  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "AboutDialog.h"

#include <QtPdWidgets2.h>

#include <QtPdCom1.h>

#include <pdcom5.h>

// literate macro
#define TM_LIT(X) #X
#define TM_STR(X) TM_LIT(X)

/****************************************************************************/

/** Constructor.
 */
AboutDialog::AboutDialog(
        QWidget *parent
        ):
    QDialog(parent)
{
    setupUi(this);

    labelCopyright->setText(
            QString::fromUtf8(
                "Copyright © 2006 – %1 Florian Pose <fp@igh.de>")
            .arg(QString(__DATE__).right(4)));

    QString version;

#ifdef VERSION
    version = QString::fromUtf8("Version %1").arg(TM_STR(VERSION));
#endif

#ifdef COMMIT_DATE
    if (not version.isEmpty()) {
        version += " – ";
    }
    version += QString::fromUtf8("Committed on %1").arg(TM_STR(COMMIT_DATE));
#endif
    version += QString::fromUtf8("\nPdCom Version: %1").arg(PdCom::pdcom_full_version);
    version += QString::fromUtf8("\nQtPdCom Version: %1").arg(QtPdCom::qtpdcom_full_version);
    version += QString::fromUtf8("\nQtPdWidgets Version: %1").arg(QtPdWidgets2::qtpdwidgets_full_version);
    labelVersion->setText(version);
}

/****************************************************************************/

/** Destructor.
 */
AboutDialog::~AboutDialog()
{
}

/****************************************************************************/
