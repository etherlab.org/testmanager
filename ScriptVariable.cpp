/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2019  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION

#include <Python.h>
#include <numpy/ndarrayobject.h>

#include "ScriptVariable.h"
#include <pdcom5/Exception.h>
#include <pdcom5/Subscriber.h>
#include <pdcom5/Subscription.h>

#include "ScriptVariableModel.h"
#include "WidgetContainer.h"
#include "TabPage.h"
#include "MainWindow.h"
#include "DataNode.h"
#include "DataModel.h"

#include <QLocale>
#include <QJsonObject>
#include <QDebug>
#include <algorithm>

/****************************************************************************/

typedef struct {
    PyObject_HEAD
    ScriptVariable *ptr;
} PyScriptVariable;

/****************************************************************************/

static PyObject *getValue(PyObject *self, PyObject * /* args */)
{
    ScriptVariable *var(((PyScriptVariable *) self)->ptr);
    if (!var) {
        PyErr_SetString(PyExc_ValueError, "Script Variable has been deleted");
        return NULL;
    }
    return var->getValuePyObject();
}

/****************************************************************************/

static PyMethodDef pyScriptVariableMethods[] = {
    { "getValue", getValue, METH_NOARGS,
        "Query the current value" },
    {NULL, NULL, 0, NULL}
};

/****************************************************************************/

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"

static PyTypeObject pyScriptVariableType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    "ScriptVariable",              /* tp_name */
    sizeof(PyScriptVariable),      /* tp_basicsize */
    0,                             /* tp_itemsize */
    0,                             /* tp_dealloc */
    0,                             /* tp_print */
    0,                             /* tp_getattr */
    0,                             /* tp_setattr */
    0,                             /* tp_reserved */
    0,                             /* tp_repr */
    0,                             /* tp_as_number */
    0,                             /* tp_as_sequence */
    0,                             /* tp_as_mapping */
    0,                             /* tp_hash */
    0,                             /* tp_call */
    0,                             /* tp_str */
    0,                             /* tp_getattro */
    0,                             /* tp_setattro */
    0,                             /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT,            /* tp_flags */
    "Script variables",            /* tp_doc */
    0,                             /* tp_traverse */
    0,                             /* tp_clear */
    0,                             /* tp_richcompare */
    0,                             /* tp_weaklistoffset */
    0,                             /* tp_iter */
    0,                             /* tp_iternext */
    pyScriptVariableMethods,       /* tp_methods */
    0,                             /* tp_members */
    0,                             /* tp_getset */
    0,                             /* tp_base */
    0,                             /* tp_dict */
    0,                             /* tp_descr_get */
    0,                             /* tp_descr_set */
    0,                             /* tp_dictoffset */
    0,                             /* tp_init */
    0,                             /* tp_alloc */
    0,                             /* tp_new */
};

#pragma GCC diagnostic pop

/****************************************************************************/

static int init_numpy()
{
    import_array1(-1);
    return 0;
}

/****************************************************************************/

void ScriptVariable::registerPythonType()
{
    if (init_numpy())
        qFatal("Could not initialize numpy");

    if (PyType_Ready(&pyScriptVariableType) < 0) {
        qWarning() << "Failed to finalize Python type ScriptVariable!";
    }
}

/*****************************************************************************
 * public
 ****************************************************************************/


struct ScriptVariable::Subscription : PdCom::Subscriber
{
    ScriptVariable *parent_;
    PdCom::Subscription subscription_;

    Subscription(ScriptVariable& parent, PdCom::Transmission t, PdCom::Variable const& var)
        : PdCom::Subscriber(t), parent_(&parent), subscription_(*this, var)
    {}

    void stateChanged(PdCom::Subscription const &subscription) override
    {
        if (subscription.getState() == PdCom::Subscription::State::Invalid) {
            parent_->variable = {};
            subscription_ = {};

            if (parent_->dataNode) {
                parent_->dataNode->notifyUnsubscribed();
                parent_->dataNode = nullptr;
            }

            parent_->dataPresent = false;
            parent_-> model->notify(parent_);
        }else if (subscription.getState() == PdCom::Subscription::State::Active
                    and getTransmission() == PdCom::event_mode)
        {
            try {
                subscription_.poll();
            }
            catch (PdCom::Exception &e) {
                MainWindow *mainWindow(
                    parent_->model->getContainer()->getTabPage()->getMainWindow());
                QString msg(tr("Script variable poll failed: %1")
                        .arg(e.what()));
                mainWindow->statusBar()->showMessage(msg, 5000);
                qWarning() << msg;
                subscription_ = {};
                parent_->variable = {};
                parent_->dataNode->notifyUnsubscribed();
                parent_->dataNode = nullptr;
            }
        }
    }
    void newValues(std::chrono::nanoseconds time_ns) override
    {
        Q_UNUSED(time_ns);
        parent_->values.resize(parent_->variable.getSizeInfo().totalElements());
        subscription_.getValue(parent_->values);
        parent_->dataPresent = true;
        parent_->model->notify(parent_);
        parent_->model->getContainer()->executeScript();
    }
};

ScriptVariable::ScriptVariable(ScriptVariableModel *model):
    period(0.0),
    model(model),
    dataNode(nullptr),
    dataPresent(false),
    pyObject((PyObject *) PyObject_New(PyScriptVariable,
                &pyScriptVariableType))
{
    ((PyScriptVariable *) pyObject)->ptr = this;
}

/****************************************************************************/

ScriptVariable::~ScriptVariable()
{
    subscription.reset();
    if (dataNode) {
        dataNode->notifyUnsubscribed();
    }

    ((PyScriptVariable *) pyObject)->ptr = nullptr;
    // scripts may still have a reference to this object.
    Py_CLEAR(pyObject);
}

/****************************************************************************/

QIcon ScriptVariable::getIcon() const
{
    if (dataNode) {
        return dataNode->getIcon();
    }
    else {
        return QIcon();
    }
}

/****************************************************************************/

void ScriptVariable::fromJson(const QJsonValue &value)
{
    QJsonObject obj(value.toObject());

    url = obj["url"].toString();
    period = std::chrono::duration<double>(obj["period"].toDouble());
    name = obj["name"].toString();
}

/****************************************************************************/

QJsonValue ScriptVariable::toJson() const
{
    QJsonObject obj;

    obj["url"] = url.toString();

    if (period.count()) {
        obj["period"] = period.count();
    }

    obj["name"] = name;

    return obj;
}

/****************************************************************************/

void ScriptVariable::replaceUrl(const QUrl &oldUrl, const QUrl &newUrl)
{
    QUrl dataSourceUrl(url.adjusted(QUrl::RemovePassword |
                QUrl::RemovePath | QUrl::RemoveQuery |
                QUrl::RemoveFragment));
    if (dataSourceUrl != oldUrl) {
        return;
    }

    url.setScheme(newUrl.scheme());
    url.setAuthority(newUrl.authority()); // user, pass, host, port
}

/****************************************************************************/

PyObject *ScriptVariable::getValuePyObject() const
{
    if (variable.empty()) {
        PyErr_SetString(PyExc_RuntimeError,
            "Variable is empty.");
        return nullptr;
    }
    const auto sizeInfo = variable.getSizeInfo();
    if (sizeInfo.isScalar()) {
        if (values.size() == 1)
            return Py_BuildValue("d", values.at(0));
        else
            return Py_BuildValue("d", 0.0);
    }

    // convert size information
    const std::vector<npy_intp> shape{sizeInfo.begin(), sizeInfo.end()};
    // initialize fresh array
    PyObject *ans = PyArray_NewFromDescr(&PyArray_Type,
            PyArray_DescrFromType(NPY_DOUBLE),
            shape.size(), const_cast<npy_intp *>(shape.data()),
            nullptr, nullptr, 0, nullptr);
    if (!ans)
        return ans;
    auto *array_obj = reinterpret_cast<PyArrayObject*>(ans);

    // memory must be contiguous for use memcpy
    if (!(PyArray_FLAGS(array_obj) & NPY_ARRAY_C_CONTIGUOUS)) {
        PyErr_SetString(PyExc_RuntimeError,
                "Freshly created numpy array is not C contiguous.");
        Py_DECREF(ans);
        return NULL;
    }
    // copy vector
    auto *dst = reinterpret_cast<double*>(PyArray_DATA(array_obj));
    if (!dst) {
        PyErr_SetString(PyExc_RuntimeError, "Array has no memory for data");
        Py_DECREF(ans);
        return NULL;
    }
    if (values.empty()) {
        // no data has arrived yet, zero initialize answer.
        std::fill(dst, dst + sizeInfo.totalElements(), 0.0);
    } else {
        std::copy(values.begin(), values.end(), dst);
    }
    return ans;
}

/****************************************************************************/

QString ScriptVariable::getValueString() const
{
    if (values.empty())
        return "";
    if (values.size() == 1)
        return QLocale().toString(values[0]);
    QString ans = "[" + QLocale().toString(values[0]);
    for (auto it = (values.begin() + 1); it != values.end(); ++it)
        ans += ", " + QLocale().toString(*it);
    ans += "]";
    return ans;
}

/****************************************************************************/

void ScriptVariable::connectVariables()
{
    subscription.reset();
    if (dataNode) {
        dataNode->notifyUnsubscribed();
        dataNode = nullptr;
    }

    MainWindow *mainWindow(
            model->getContainer()->getTabPage()->getMainWindow());
    DataModel *dataModel(mainWindow->getDataModel());
    dataNode = dataModel->findDataNode(url);

    if (dataNode) {
        variable = dataNode->getVariable();
        const auto transmission = period.count() == 0 ?
                QtPdCom::Transmission{QtPdCom::event_mode} : QtPdCom::Transmission{period};
        if (!variable.empty()) {
            try {
                subscription.reset(new Subscription(*this, transmission.toPdCom(), variable));
            }
            catch (PdCom::Exception &e) {
                QString msg(tr("Script variable subscription failed: %1")
                        .arg(e.what()));
                mainWindow->statusBar()->showMessage(msg, 5000);
                qWarning() << msg;
                variable = {};
                dataNode = nullptr;
                return;
            }
            dataNode->notifySubscribed(transmission);
        }
    }
    else {
        QString msg(tr("URL not found: %1").arg(url.toString()));
        mainWindow->statusBar()->showMessage(msg, 5000);
        qWarning() << msg;
    }
}

/****************************************************************************/

void ScriptVariable::appendToPythonDict(PyObject *dict) const
{
    if (name.isEmpty()) {
        return;
    }

    PyDict_SetItemString(dict, name.toUtf8().constData(), pyObject);
}

/****************************************************************************/
