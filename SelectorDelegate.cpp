/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2018  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "SelectorDelegate.h"

#include <QDebug>

#include "DataModel.h"
#include "DataNode.h"
#include "DataSlot.h"
#include "SelectorDialog.h"
#include "SlotModelCollection.h"

/****************************************************************************/

SelectorDelegate::SelectorDelegate(QObject *parent):
    QStyledItemDelegate(parent)
{}

/****************************************************************************/

QWidget *SelectorDelegate::createEditor(
        QWidget *parent,
        const QStyleOptionViewItem &,
        const QModelIndex &) const
{
    SelectorDialog *selectorDialog = new SelectorDialog(parent);
    selectorDialog->setFocusPolicy(Qt::StrongFocus);
    connect(selectorDialog,
            SIGNAL(finished(int)),
            this,
            SLOT(editingFinished(int)));
    connect(selectorDialog,
            &QDialog::finished,
            selectorDialog,
            &QObject::deleteLater);
    return selectorDialog;
}

/****************************************************************************/

void SelectorDelegate::setEditorData(
        QWidget *editor,
        const QModelIndex &index) const
{
    auto *model = index.model();
    if (!model) {
        return;
    }

    const auto url = model->data(model->index(
                                         index.row(),
                                         SlotNode::UrlColumn,
                                         index.parent()),
                                 Qt::DisplayRole)
                             .toUrl();

    const auto selector =
            QJsonValue::fromVariant(model->data(index, Qt::EditRole))
                    .toObject();

    auto dataNode = dataModel->findDataNode(url);
    if (!dataNode) {
        return;
    }

    SelectorDialog *selectorDialog = static_cast<SelectorDialog *>(editor);
    selectorDialog->setData(*dataNode, selector);
}

/****************************************************************************/

void SelectorDelegate::setModelData(
        QWidget *editor,
        QAbstractItemModel *model,
        const QModelIndex &index) const
{
    SelectorDialog *selectorDialog = static_cast<SelectorDialog *>(editor);
    if (selectorDialog->result() != QDialog::Accepted)
        return;
    Selector selector = selectorDialog->getSelector();

    model->setData(
            index,
            QJsonValue(selector.toJson()).toVariant(),
            Qt::EditRole);
}

/****************************************************************************/

void SelectorDelegate::updateEditorGeometry(
        QWidget *,
        const QStyleOptionViewItem &,
        const QModelIndex &) const
{}

/****************************************************************************/

void SelectorDelegate::editingFinished(int result)
{
    SelectorDialog *selectorDialog = dynamic_cast<SelectorDialog *>(sender());
    if (result == QDialog::Accepted) {
        emit commitData(selectorDialog);
    }
    emit closeEditor(selectorDialog);
}

/****************************************************************************/
