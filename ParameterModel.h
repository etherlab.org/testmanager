/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2023 Daniel Ramirez <dr@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PARAMETER_MODEL_H
#define PARAMETER_MODEL_H

/****************************************************************************/

#include <pdcom5/SizeTypeInfo.h>

#include <QAbstractTableModel>
#include <QJsonArray>
#include <QList>
#include <QUrl>

/****************************************************************************/

class MainWindow;
class Parameter;
class DataModel;
class DataNode;

/****************************************************************************/

class ParameterModel:
    public QAbstractTableModel
{
    Q_OBJECT

    public:
        ParameterModel(MainWindow *);
        ~ParameterModel();

        enum ItemFlag {
            // an uncommon flag is used to differentiate the inactive sub
            SubInactive = Qt::ItemFlag::ItemIsUserTristate 
        };

        MainWindow *getMainWindow() const { return mainWindow; }
        
        void clear();
        bool isEmpty() const { return parameters.isEmpty(); }

        void fromJson(const QJsonArray &);
        QJsonArray toJson(bool) const;

        void appendDataSources(DataModel *) const;

        int rowCount(const QModelIndex &) const;
        int columnCount(const QModelIndex &) const;
        QVariant data(const QModelIndex &, int) const;
        QVariant headerData(int, Qt::Orientation, int) const;
        Qt::ItemFlags flags(const QModelIndex &) const;
        bool setData(const QModelIndex &, const QVariant &, int);

        QStringList mimeTypes() const;
        bool dropMimeData(const QMimeData *, Qt::DropAction, int, int,
                const QModelIndex &);
        bool canDropMimeData(const QMimeData *, Qt::DropAction, int, int,
                const QModelIndex &) const;

        void addParameters(QList<QUrl> urls, int insertPos);
        void connectParameters();
        void saveParameters();
        void updateParameters();
        void replaceUrl(const QUrl &, const QUrl &);
        QSet<QUrl> getUrls() const;

        void notify(Parameter *);

        Parameter *getParameter(const QModelIndex &) const;

        QString getPath() const { return parameterPath; }
        void setPath(QString p) { parameterPath = p; }

        QString getDescription() const { return description; }
        void setDescription(QString d) { description = d; }
        
        QString getCreator() const { return creator; }
        QString getUser() const { return user; }
        QString getDateString() const { return date; }

        void remove(Parameter *);

    protected:
        QList<Parameter *> parameters;
        MainWindow *mainWindow;
        DataNode *dataNode;
        QString parameterPath;
        QString description;
        QString creator;
        QString user;
        QString date;
        
        static QString buildVectorString(const QVector<double> &);
        static QVector<double> parseVectorString(const QString &);
        QString dimensionString(PdCom::SizeInfo dim) const;

    private:
        ParameterModel();
};

/****************************************************************************/

#endif
