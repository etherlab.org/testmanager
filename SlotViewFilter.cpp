/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2023  Bjarne von Horn <vh@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "SlotViewFilter.h"

#include "MainWindow.h"
#include "SlotModel.h"
#include "SlotModelCollection.h"

SlotViewFilter::SlotViewFilter(MainWindow &mw, QObject *parent)
    : QSortFilterProxyModel(parent),
      mainWindow(&mw)
{

}

void SlotViewFilter::widgetSelectionChanged()
{
    selectedWidgets = mainWindow->containers(MainWindow::SelectedOrCurrentTab);
    invalidateFilter();
}

bool SlotViewFilter::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    if (!source_parent.isValid()) {
        // just below root node
        const auto model = dynamic_cast<SlotModel*>(
                mainWindow->slotModelCollection.getChildNode(source_row));
        if (model)
            return selectedWidgets.contains(model->container);
        return false;
    }
    // children of not visible parents are never shown, so we can cut some corners here.
    return true;
}
