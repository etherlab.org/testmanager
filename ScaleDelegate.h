/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2019  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef SCALE_DELEGATE_H
#define SCALE_DELEGATE_H

#include <QStyledItemDelegate>

/****************************************************************************/

class ScaleDelegate:
    public QStyledItemDelegate
{
    Q_OBJECT

    public:
        ScaleDelegate(QObject *);

        QWidget *createEditor(QWidget *,
                const QStyleOptionViewItem &,
                const QModelIndex &) const;

        void setEditorData(QWidget *, const QModelIndex &) const;
        void setModelData(QWidget *, QAbstractItemModel *,
                const QModelIndex &) const;

        void updateEditorGeometry(QWidget *,
                const QStyleOptionViewItem &, const QModelIndex &) const;
        QSize sizeHint(const QStyleOptionViewItem &,
                const QModelIndex &) const;

        static QString text(double);

    private:
        static double constants[];

        ScaleDelegate();
        static QString name(unsigned int);

    private slots:
        void activated(int);
};

/****************************************************************************/

#endif
