/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2018  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "SlotDialog.h"

#include "SlotModel.h"
#include "DataSlot.h"
#include "Plugin.h"
#include "MainWindow.h"

#include <QMenu>
#include <QDebug>

/****************************************************************************/

SlotDialog::SlotDialog(
        MainWindow *mainWindow,
        SlotModel *slotModel,
        const Plugin *plugin,
        QWidget *parent
        ):
    QDialog(parent),
    mainWindow(mainWindow),
    scaleDelegate(this),
    selectorDelegate(this),
    colorDelegate(this),
    contextMenuDataSlot(NULL),
    applyColumn(-1)
{
    setupUi(this);

    connect(treeView, SIGNAL(customContextMenuRequested(const QPoint &)),
            this, SLOT(contextMenu(const QPoint &)));

    auto *collection = static_cast<SlotModelCollection*>(slotModel->getParentNode());
    treeView->setModel(collection);
    treeView->setRootIndex(collection->index(collection->getChildIndex(slotModel), 0, {}));

    treeView->header()->setSectionResizeMode(SlotModel::UrlColumn,
            QHeaderView::Stretch);
    treeView->header()->resizeSection(SlotModel::AliasColumn, 100);

    treeView->setItemDelegateForColumn(SlotModel::ScaleColumn,
            &scaleDelegate);

    treeView->setItemDelegateForColumn(SlotModel::SelectorColumn,
            &selectorDelegate);
    selectorDelegate.dataModel = mainWindow->getDataModel();

    if (plugin->colorsSupported()) {
        treeView->header()->resizeSection(SlotModel::ColorColumn, 100);
        treeView->setItemDelegateForColumn(SlotModel::ColorColumn,
                &colorDelegate);
    }
    else {
        treeView->header()->hideSection(SlotModel::ColorColumn);
    }

    treeView->expandAll();
}

/****************************************************************************/

SlotDialog::~SlotDialog()
{
}

/****************************************************************************/

void SlotDialog::contextMenu(const QPoint &point)
{
    contextIndex = treeView->indexAt(point);

    SlotNode *slotNode = (SlotNode *) contextIndex.internalPointer();
    contextMenuDataSlot = dynamic_cast<DataSlot *>(slotNode);
    if (!contextMenuDataSlot) {
        return;
    }

    applyColumn = contextIndex.column();
    if (applyColumn < SlotModel::AliasColumn || applyColumn > SlotModel::ColorColumn) {
        applyColumn = -1;
    }

    QMenu menu(this);

    QAction applyAction(this);

    if (applyColumn != -1) {
        QString header(treeView->model()->headerData(applyColumn,
                    Qt::Horizontal, Qt::DisplayRole).toString());
        applyAction.setText(tr("Apply %1 to all").arg(header));
        connect(&applyAction, SIGNAL(triggered()),
                this, SLOT(slotApplyToAll()));
        menu.addAction(&applyAction);
    }

    QAction removeAction(this);
    removeAction.setText(tr("Remove variable"));
    removeAction.setIcon(QIcon(":/images/list-remove.svg"));
    connect(&removeAction, SIGNAL(triggered()), this, SLOT(slotRemove()));
    menu.addAction(&removeAction);

    QAction jumpAction(this);
    jumpAction.setText(tr("Jump to variable"));
    jumpAction.setIcon(QIcon(":/images/go-jump.svg"));
    connect(&jumpAction, SIGNAL(triggered()), this, SLOT(slotJump()));
    menu.addAction(&jumpAction);

    menu.exec(treeView->mapToGlobal(point));
}

/****************************************************************************/

void SlotDialog::slotApplyToAll()
{
    SlotModelCollection *slotModelCollection =
        dynamic_cast<SlotModelCollection *>(treeView->model());
    if (!slotModelCollection) {
        return;
    }

    SlotNode *slotModel =
        slotModelCollection->getChildNode(treeView->rootIndex().row());

    if (slotModel)
        slotModel->applyToAll(contextMenuDataSlot, applyColumn);
}

/****************************************************************************/

void SlotDialog::slotJump()
{
    SlotModelCollection *slotModel = dynamic_cast<SlotModelCollection *>(treeView->model());
    if (!slotModel) {
        return;
    }

    QUrl url(contextMenuDataSlot->url);
    QModelIndex index(mainWindow->getDataModel()->indexFromUrl(url));
    mainWindow->sourceTree->selectionModel()->setCurrentIndex(index,
            QItemSelectionModel::SelectCurrent);
}

/****************************************************************************/

void SlotDialog::slotRemove()
{
    SlotModelCollection *slotModel = dynamic_cast<SlotModelCollection *>(treeView->model());
    if (!slotModel) {
        return;
    }

    slotModel->remove(contextIndex);
}

/****************************************************************************/
