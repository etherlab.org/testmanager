[![pipeline status](https://gitlab.com/etherlab.org/testmanager/badges/master/pipeline.svg)](https://gitlab.com/etherlab.org/testmanager/-/commits/master)

# Testmanager

Testmanager is a graphical application, that was made for visualizing
parameters and signals of an EtherLab control process. The application is
usually connected to the control process via the MSR protocol (2345/tcp) and
the [PdCom](https://gitlab.com/etherlab.org/pdcom) /
[PdServ](https://gitlab.com/etherlab.org/pdserv) libraries.

![overview testmanager](doc/figures/overview_testmanager.png "Overview Testmanager")

Parameters and signals of the control process are registered within the
application. These values can be dynamically linked to operation- and
visualization instruments. For that, Testmanager gets a flexible and
user-friendly operator's application.

## Installation

To use the Testmanager, the libraries
[PdCom](https://gitlab.com/etherlab.org/pdcom) and
[QtPdWidgets](https://gitlab.com/etherlab.org/qtpdwidgets) are required. These
can be installed manually or via [OBS
packages](https://build.opensuse.org/project/show/science:EtherLab).

Testmanager can be built and installed locally. To do this, execute the two
commands `qmake-qt5` and `make install` in the Testmanager directory.

For development purposes, scripts are available to compile and start the test
manager in a Docker container. The script `./scripts/run-docker.sh` can be used
to execute various tasks in Docker. The options are displayed with `--help`.
Alternatively, developers can use VSCodium to use the stored tasks.

## Quickstart

Let's say there is a control process running on your PC. Start Testmanager and
click on 'Add Connection'. Enter an appropriate hostname (e. g.  `localhost`).
On the left panel, a tree with all process parameters and signals will appear.
If PdServ is installed, the command `pdserv-example-st` serves as a sample
process to connect to.

Then, click on 'Edit', which makes the middle panel editable. Do a right click
on the canvas and add a widget. The connection between a variable and a widget
can be created by dragging the variable from the tree onto the widget.

## Development

There are a few [Pre-Commit](https://pre-commit.com/) hooks available. To
register them, please enter the following command after cloning the repository:

```bash
pre-commit install
```

## Icon file

For the generation of the images/testmanager.ico file, the following procedure can be used:
Export a 256x256 PNG of the images/testmanager.svg. Then let ImageMagick to to the rest:

```
magick convert images/testmanager.png \
    -background transparent \
    -define icon:auto-resize="256,128,96,64,48,32,16" \
    images/testmanager.ico
```
