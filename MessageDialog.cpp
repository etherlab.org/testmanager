/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2019  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "MessageDialog.h"

#include "DataSource.h"

#include <QFileDialog>

/****************************************************************************/

MessageDialog::MessageDialog(DataSource *dataSource, QWidget *parent):
    QDialog(parent),
    dataSource(dataSource)
{
    setupUi(this);

    lineEditPath->setText(dataSource->getMessagePath());
}

/****************************************************************************/

void MessageDialog::on_buttonBox_accepted()
{
    dataSource->setMessagePath(lineEditPath->text());
    accept();
}

/****************************************************************************/

void MessageDialog::on_pushButtonSelect_clicked()
{
    auto *dialog = new QFileDialog(this);
    dialog->setAcceptMode(QFileDialog::AcceptOpen);

    QStringList paths(QDir::searchPaths("layout"));
    QDir dir;
    if (paths.size()) {
        dir.setPath(paths[0]);
        dialog->setDirectory(dir);
    }

    QStringList filters;
    filters
        << tr("EtherLab PlainMessages files (*.xml)")
        << tr("Any files (*)");

    dialog->setNameFilters(filters);
    dialog->setDefaultSuffix("xml");

    connect(dialog, &QDialog::finished, dialog, &QObject::deleteLater);
    connect(dialog, &QDialog::accepted, this, [this, dialog, dir]()
            {
                QString path = dialog->selectedFiles()[0];
                lineEditPath->setText(dir.relativeFilePath(path));
            });

    dialog->open();
}

/****************************************************************************/
