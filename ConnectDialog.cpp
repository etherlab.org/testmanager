/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2018  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "ConnectDialog.h"

#include "DataSource.h"
#include "LoginDialog.h"

#include <QtPdCom1/LoginManager.h>

#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <QSettings>

/****************************************************************************/

ConnectDialog::ConnectDialog(DataModel *dataModel, QWidget *parent):
    QDialog(parent),
    dataSource(nullptr),
    loginDialogShowed(false),
    dataModel(dataModel)
{
    setupUi(this);

    using SslCaMode = QtPdCom::Process::SslCaMode;
    cbTlsMode->addItem(
            tr("Use system-wide CAs"),
            static_cast<int>(SslCaMode::DefaultCAs));
    cbTlsMode->addItem(
            tr("Use custom CA"),
            static_cast<int>(SslCaMode::CustomCAs));
    cbTlsMode->addItem(
            tr("Ignore certificates"),
            static_cast<int>(SslCaMode::IgnoreCertificate));

    QSettings settings;
    recentSources = settings.value("recentSources").toStringList();

    setWindowTitle(tr("Connect..."));
    labelAddress->setText(tr("Data so&urce URL:"));

    if (recentSources.size()) {
        foreach (QString source, recentSources) {
            addressComboBox->addItem(source);
        }
    }
    else {
        QString defaultUrl("msr://localhost");
#if QT_VERSION < 0x050000
        addressComboBox->lineEdit()->setText(defaultUrl);
#else
        addressComboBox->setCurrentText(defaultUrl);
#endif
    }
}

/****************************************************************************/

ConnectDialog::~ConnectDialog()
{
    if (dataSource) {
        dataSource->disconnectFromHost();
        delete dataSource;
    }
}

/****************************************************************************/

DataSource *ConnectDialog::adoptSource()
{
    DataSource *ret = dataSource;
    dataSource = nullptr;  // forget
    return ret;
}

/****************************************************************************/

#if QT_VERSION < 0x050000
void ConnectDialog::on_addressComboBox_textChanged()
#else
void ConnectDialog::on_addressComboBox_currentTextChanged()
#endif
{
    okButton->setEnabled(!addressComboBox->currentText().isEmpty());
}

/****************************************************************************/

void ConnectDialog::on_okButton_clicked()
{
    using QtPdCom::LoginManager;
    using SslCaMode = QtPdCom::Process::SslCaMode;
    loginDialogShowed = false;

    if (dataSource) {
        disconnect(
                dataSource,
                &DataSource::variableTreeBuilt,
                this,
                &ConnectDialog::connected);
        disconnect(dataSource, SIGNAL(error()), this, SLOT(error()));
        dataSource->disconnectFromHost();
        delete dataSource;
        dataSource = nullptr;
    }

    url = QUrl(addressComboBox->currentText());
    if (!(url.scheme() == "msr" || url.scheme() == "msrs")) {
        labelMessage->setText(tr("Unsupported scheme."));
        return;
    }
    if (url.port() == -1) {
        int port {2345};
        if (url.scheme() == "msrs") {
            port = 4523;
        }
        url.setPort(port);
    }

    dataSource = createDataSource();

    if (url.scheme() == "msrs"
        && cbTlsMode->currentData().canConvert<int>()) {
        const int mode = cbTlsMode->currentData().toInt();
        switch (mode) {
            case static_cast<int>(SslCaMode::CustomCAs):
                dataSource->process->setCustomCAs({customCA});
                /* FALLTHRU */
            case static_cast<int>(SslCaMode::DefaultCAs):
            case static_cast<int>(SslCaMode::IgnoreCertificate):
                dataSource->process->setCaMode(static_cast<SslCaMode>(mode));
                break;
            default:
                dataSource->process->setCaMode(SslCaMode::NoTLS);
        }
    }
    else {
        dataSource->process->setCaMode(SslCaMode::NoTLS);
    }

    connect(dataSource,
            &DataSource::variableTreeBuilt,
            this,
            &ConnectDialog::connected);
    connect(dataSource, SIGNAL(error()), this, SLOT(error()));
    dataSource->credentialsNeededAction =
            connect(dataSource->getLoginManager(),
                    &LoginManager::needCredentials,
                    this,
                    &ConnectDialog::credentialsNeeded);
    connect(dataSource->getLoginManager(),
            &LoginManager::loginFailed,
            this,
            &ConnectDialog::onLoginFailed);
    connect(dataSource->getLoginManager(),
            &LoginManager::loginSuccessful,
            this,
            [this] {
                if (cbUseAuth->isChecked() && loginDialogShowed) {
                    accept();
                }
            });

    setWidgetsEnabled(false);
    labelMessage->setText(tr("Connecting to %1...").arg(url.toString()));

    dataSource->connectToHost();
}

/****************************************************************************/

void ConnectDialog::on_cancelButton_clicked()
{
    if (dataSource) {
        dataSource->disconnectFromHost();
        dataSource->deleteLater();
        dataSource = nullptr;
    }

    reject();
}

/****************************************************************************/

void ConnectDialog::connected()
{
    QSettings settings;

    recentSources.removeAll(url.toString());
    recentSources.prepend(url.toString());

    settings.setValue("recentSources", recentSources);

    dataSource->writeSslSettings(settings, customCaPath);

    if (cbUseAuth->isChecked() && !loginDialogShowed) {
        dataSource->getLoginManager()->login();
    }
    else {
        accept();
    }
}

/****************************************************************************/

void ConnectDialog::error()
{
    labelMessage->setText("");

    auto *mb = new QMessageBox(
            QMessageBox::Critical,
            "Connection failed",
            tr("Failed to connect to %1: %2.")
                    .arg(url.toString(), 1)
                    .arg(dataSource->errorString(), 2),
            QMessageBox::Ok,
            this);
    connect(mb, &QDialog::finished, mb, &QObject::deleteLater);
    mb->open();

    setWidgetsEnabled(true);
    loginDialogShowed = false;
}
/****************************************************************************/

void ConnectDialog::onLoginFailed()
{
    labelMessage->setText("");

    auto mb = new QMessageBox(QMessageBox::Critical,
            tr("Login failed"),
            dataSource->getLoginManager()->getErrorMessage(),
            QMessageBox::Ok,
            this);

    mb->setAttribute(Qt::WA_DeleteOnClose);
    connect(mb, &QDialog::finished, this, [this]{
        setWidgetsEnabled(true);
    });
    mb->open();
}

/****************************************************************************/

void ConnectDialog::credentialsNeeded()
{
    auto dialog = new LoginDialog(*dataSource->getLoginManager(), this);
    connect(dialog, &QDialog::finished, dialog, &QObject::deleteLater);
    connect(dialog, &QDialog::rejected, this, &QDialog::reject);
    dialog->show();
    loginDialogShowed = true;
}

/****************************************************************************/

void ConnectDialog::on_cbUseTls_stateChanged(int state)
{
    QUrl url(addressComboBox->currentText());

    if (state == Qt::Checked) {
        cbTlsMode->setEnabled(true);
        pbLoadCAs->setEnabled(
                cbTlsMode->currentData().toInt()
                == static_cast<int>(QtPdCom::Process::SslCaMode::CustomCAs));

        if (url.isValid()) {
            if (url.scheme() == "msr") {
                url.setScheme("msrs");
            }
            if (url.port() == 2345) {
                url.setPort(4523);
            }
        }
    }
    else {
        cbTlsMode->setEnabled(false);
        pbLoadCAs->setEnabled(false);

        if (url.isValid()) {
            if (url.scheme() == "msrs") {
                url.setScheme("msr");
            }
            if (url.port() == 4523) {
                url.setPort(2345);
            }
        }
    }

    if (url.isValid()) {
        addressComboBox->setCurrentText(url.toString());
    }
}

/****************************************************************************/

void ConnectDialog::on_cbTlsMode_currentIndexChanged(int)
{
    const auto sender = qobject_cast<QComboBox *>(QObject::sender());
    if (sender) {
        int idx = -1;
        if (sender->currentData().canConvert<int>()) {
            idx = sender->currentData().toInt();
        }
        pbLoadCAs->setEnabled(
                idx
                == static_cast<int>(QtPdCom::Process::SslCaMode::CustomCAs));
    }
}

/****************************************************************************/

void ConnectDialog::on_addressComboBox_currentTextChanged(const QString &text)
{
    using CAMode = QtPdCom::Process::SslCaMode;
    if (text.startsWith("msrs")) {
        cbTlsMode->setEnabled(true);
        cbUseTls->setCheckState(Qt::Checked);

        QSettings settings;
        const QUrl tmp_url(text);
        const auto tls_settings = DataSource::readSslSettings(
                settings,
                tmp_url.host(),
                tmp_url.port());
        if (tls_settings.mode_ != CAMode::NoTLS) {
            switch (tls_settings.mode_) {
                case CAMode::CustomCAs:
                    tryLoadCustomCA(tls_settings.customCaPath);
                    /* FALLTHRU */
                case CAMode::DefaultCAs:
                case CAMode::IgnoreCertificate:
                    // QComboBox::findData() is broken.
                    for (int i = 0; i < cbTlsMode->count(); ++i) {
                        if (cbTlsMode->itemData(i)
                            == QVariant::fromValue(tls_settings.mode_)) {
                            cbTlsMode->setCurrentIndex(i);
                            break;
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }
    else {
        cbTlsMode->setEnabled(false);
        pbLoadCAs->setEnabled(false);
        cbUseTls->setCheckState(Qt::Unchecked);
    }
}

/****************************************************************************/

void ConnectDialog::on_pbLoadCAs_clicked()
{
    const auto path = QFileDialog::getOpenFileName(
            this,
            tr("Select CA"),
            customCaPath,
            tr("Certificates") + "(*.pem *.crt)");
    if (path.isNull()) {
        return;
    }
    tryLoadCustomCA(path);
}

/****************************************************************************/

void ConnectDialog::setWidgetsEnabled(bool enabled)
{
    okButton->setEnabled(enabled);
    addressComboBox->setEnabled(enabled);
    cbUseAuth->setEnabled(enabled);
    cbUseTls->setEnabled(enabled);

    if (radioButtonPermanent->isVisible())
        radioButtonPermanent->setEnabled(enabled);
    if (radioButtonSession->isVisible())
        radioButtonSession->setEnabled(enabled);

    if (enabled && cbUseTls->isChecked()) {
        cbTlsMode->setEnabled(true);
        const auto cd = cbTlsMode->currentData();
        if (cd.canConvert<int>()
            && cd.toInt()
                    == static_cast<int>(
                            QtPdCom::Process::SslCaMode::CustomCAs)) {
            pbLoadCAs->setEnabled(true);
        }
    }
    else {
        cbTlsMode->setEnabled(false);
        pbLoadCAs->setEnabled(false);
    }
}

/****************************************************************************/

bool ConnectDialog::tryLoadCustomCA(QString path)
{
    customCA.clear();
    customCaPath.clear();
    const bool isPem = path.endsWith("pem");
    QFile cert {path};
    if (cert.open(QIODevice::ReadOnly)) {
        customCA = QSslCertificate {&cert, isPem ? QSsl::Pem : QSsl::Der};
        customCaPath = path;
        if (!customCA.isNull()) {
            return true;
        }
    }
    auto mb = new QMessageBox(QMessageBox::Critical,
            tr("Certificate import failed"),
            tr("Loading certificate from file %1 failed").arg(path),
            QMessageBox::Ok,
            this);
    mb->setAttribute(Qt::WA_DeleteOnClose);
    mb->open();
    return false;
}

/****************************************************************************/

DataSource *ConnectDialog::createDataSource() const
{
    return new DataSource(dataModel, url);
}

/****************************************************************************/
