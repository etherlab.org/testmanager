/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2023 Daniel Ramirez <dr@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "ParameterDialog.h"

#include "SlotModelCollection.h"
#include "DataSlot.h"
#include "MainWindow.h"
#include "ParameterTableModel.h"
#include "Plugin.h"
#include "SlotModel.h"

#include <QDebug>
#include <QFileInfo>
#include <QMenu>
#include <QTextEdit>

/****************************************************************************/

ParameterDialog::ParameterDialog(
        ParameterTableModel *model,
        QWidget *parent
        ):
    QDialog(parent),
    model(model)
{
    setupUi(this);

    tableViewParameters->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(tableViewParameters,
            SIGNAL(customContextMenuRequested(const QPoint &)),
            this, SLOT(tableViewParametersCustomContextMenu(const QPoint &)));
    connect(saveButton, SIGNAL(clicked()),
            this, SLOT(saveParameters()));
    connect(saveAsButton, SIGNAL(clicked()),
            this, SLOT(saveParametersAs()));
    connect(applyButton, SIGNAL(clicked()),
            this, SLOT(applyParameters()));
    connect(textEditDescription, &QTextEdit::textChanged, this,
            [this, model]() 
            {
                model->setDescription(textEditDescription->toPlainText());
            });

    tableViewParameters->setModel(
        dynamic_cast<QAbstractTableModel*> (model));

    labelCreator->setText(model->getCreator());
    labelCreatedBy->setText(model->getUser());
    labelCreationDate->setText(model->getDateString());
    textEditDescription->setText(model->getDescription());

    QString title = "Parameter File - ";
    QFileInfo fi(model->getPath());
    title.append(fi.fileName());
    setWindowTitle(title);

    model->connectParameters();

    tableViewParameters->resizeColumnsToContents();
}

/****************************************************************************/

ParameterDialog::~ParameterDialog()
{
}

/****************************************************************************/

void ParameterDialog::saveParameters()
{
    model->saveParameters();
}

/****************************************************************************/

void ParameterDialog::saveParametersAs()
{
    model->saveParametersAs();
    QString title = "Parameter File - ";
    QFileInfo fi(model->getPath());
    title.append(fi.fileName());
    setWindowTitle(title);
}

/****************************************************************************/

void ParameterDialog::applyParameters()
{
    model->applyParameters();
    accept();
    close();
}

/****************************************************************************/

void ParameterDialog::tableViewParametersCustomContextMenu(
        const QPoint &point)
{
    QMenu *menu = new QMenu(this);
    menu->setAttribute(Qt::WA_DeleteOnClose);
    QModelIndex index(tableViewParameters->indexAt(point));
    parameter = model->getParameter(index);

    QAction *action;
    action = menu->addAction(tr("Remove parameter"),
            this, SLOT(removeParameter()));
    action->setEnabled(parameter);
    action->setIcon(QIcon(":/images/list-remove.svg"));

    menu->addSeparator(); // -------------------------------

    action = menu->addAction(tr("Clear parameter list"),
            this, SLOT(clearParameters()));
    action->setEnabled(not model->isEmpty());
    action->setIcon(QIcon(":/images/edit-clear.svg"));

    menu->exec(tableViewParameters->viewport()->mapToGlobal(point));
}

/****************************************************************************/

void ParameterDialog::removeParameter()
{
    const QModelIndexList indexes =
        tableViewParameters->selectionModel()->selectedIndexes();
    QList<Parameter *> parameters;

    for (const QModelIndex &index : indexes) {
        if (not parameters.contains(model->getParameter(index))) {
            parameter = model->getParameter(index);
            parameters.append(parameter);
        }
    }
    foreach (Parameter *parameter, parameters) {
        ((ParameterModel *) model)->remove(parameter);
    }
}

/****************************************************************************/

void ParameterDialog::clearParameters()
{
    model->clear();
}

/****************************************************************************/
