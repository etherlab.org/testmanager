/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2023  Daniel Ramirez <dr@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "ui_ParameterDialog.h"

#include "Parameter.h"
#include "ParameterTableModel.h"

#include <QDialog>
#include <QPersistentModelIndex>

/****************************************************************************/

class MainWindow;
class ParameterTableModel;
class Parameter;

class ParameterDialog:
    public QDialog,
    public Ui::ParameterDialog
{
    Q_OBJECT

    public:
        ParameterDialog(ParameterTableModel *, QWidget *parent = 0);
        ~ParameterDialog();

    private:
        ParameterTableModel * model;
        QPersistentModelIndex contextIndex;
        Parameter * parameter;

    private slots:
        void tableViewParametersCustomContextMenu(const QPoint &);
        void saveParameters();
        void saveParametersAs();
        void applyParameters();
        void removeParameter();
        void clearParameters();

};

/****************************************************************************/
