/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2023  Bjarne von Horn <vh@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "MultiDimParameterDelegate.h"

#include "MultiDimParameterDelegate_p.h"
#include "DataModel.h"
#include "DataNode.h"

#include <pdcom5/Process.h>

#include <QDialog>
#include <QDialogButtonBox>
#include <QEvent>
#include <QLocale>
#include <QVBoxLayout>
#include <QTableView>

MultiDimParameterModel::MultiDimParameterModel(
        PdCom::Variable var,
        QObject *parent):
    QAbstractTableModel(parent),
    PdCom::Subscriber(PdCom::event_mode),
    subscription(*this, var)
{
    var.getProcess()->callPendingCallbacks();
}

void MultiDimParameterModel::newValues(std::chrono::nanoseconds)
{
    hasData = true;
    dataChanged(index(0, 0), index(rowCount() - 1, columnCount() - 1));
}

void MultiDimParameterModel::stateChanged(PdCom::Subscription const &)
{
    if (subscription.getState() == PdCom::Subscription::State::Active
        && !hasData) {
        subscription.poll();
    }
}

QVariant
MultiDimParameterModel::data(const QModelIndex &index, int role) const
{
    const auto var = subscription.getVariable();
    if (!hasData || var.empty()
        || (role != Qt::DisplayRole && role != Qt::EditRole)) {
        return {};
    }
    const auto si = var.getSizeInfo();
    if (index.row() < 0 || index.row() >= (int)si.rows()) {
        return {};
    }
    if (index.column() < 0 || index.column() >= (int)si.columns()) {
        return {};
    }
    double ans;
    if (!subscription.getData()) {
        return {};
    }
    PdCom::details::copyData(
            &ans,
            PdCom::TypeInfo::double_T,
            subscription.getData(),
            var.getTypeInfo().type,
            1,
            offset(index));
    return ans;
}

bool MultiDimParameterModel::setData(
        const QModelIndex &index,
        const QVariant &value,
        int role)
{
    if (role != Qt::EditRole) {
        return false;
    }

    const auto var = subscription.getVariable();
    if (var.empty() || !var.isWriteable()) {
        return false;
    }
    const auto si = var.getSizeInfo();
    if (index.row() < 0 || index.row() >= (int)si.rows()) {
        return false;
    }
    if (index.column() < 0 || index.column() >= (int)si.columns()) {
        return false;
    }
    const auto val = value.toDouble();
    var.setValue(&val, PdCom::TypeInfo::double_T, 1, offset(index));
    return true;
}

MultiDimParameterDialog::MultiDimParameterDialog(QWidget *parent):
    QDialog(parent)
{
    auto layout = new QVBoxLayout(this);
    setLayout(layout);
    label = new QLabel(this);
    label->setText(tr("Variable: "));
    layout->addWidget(label);
    tableView = new QTableView(this);
    layout->addWidget(tableView);
    auto bbox = new QDialogButtonBox(QDialogButtonBox::Close, this);
    connect(bbox, &QDialogButtonBox::rejected, this, &QDialog::reject);
    layout->addWidget(bbox);
    setWindowTitle(tr("Set Parameter"));
}

void MultiDimParameterDialog::setVariable(PdCom::Variable var)
{
    auto oldModel = tableView->model();
    auto model = new MultiDimParameterModel(var, this);
    tableView->setModel(model);
    oldModel->deleteLater();
    label->setText(tr("Variable: ") + QString::fromStdString(var.getPath()));
}


MultiDimParameterDelegate::MultiDimParameterDelegate(QObject *parent):
    QStyledItemDelegate(parent)
{}

bool MultiDimParameterDelegate::editorEvent(
        QEvent *event,
        QAbstractItemModel *_model,
        const QStyleOptionViewItem &,
        const QModelIndex &index)
{
    if (event->type() != QEvent::MouseButtonDblClick) {
        return false;
    }
    auto model = qobject_cast<DataModel *>(_model);
    if (!model) {
        return false;
    }
    auto node = model->getDataNode(index);
    if (!node) {
        return false;
    }
    const auto var = node->getVariable();
    if (var.empty() || !var.isWriteable() || var.getSizeInfo().isScalar()) {
        return false;
    }
    auto dialog = new MultiDimParameterDialog();
    connect(dialog, &QDialog::finished, dialog, &QObject::deleteLater);
    dialog->setModal(false);
    dialog->setVariable(var);
    dialog->show();
    return true;
}
