/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2023  Bjarne von Horn <vh@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include "MultiDimParameterDelegate.h"

#include <QDialog>
#include <QTableView>
#include <QAbstractTableModel>
#include <QLabel>

#include <pdcom5/Variable.h>
#include <pdcom5/Subscriber.h>
#include <pdcom5/Subscription.h>

class MultiDimParameterModel:
    public QAbstractTableModel,
    private PdCom::Subscriber
{
        Q_OBJECT

        PdCom::Subscription subscription;
        bool hasData = false;

    public:
        explicit MultiDimParameterModel(
                PdCom::Variable var,
                QObject *parent = nullptr);

        int rowCount(const QModelIndex &parent = QModelIndex()) const override
        {
            const auto var = subscription.getVariable();
            if (var.empty() || parent.isValid()) {
                return 0;
            }
            const auto si = var.getSizeInfo();
            return si.rows();
        }
        int
        columnCount(const QModelIndex &parent = QModelIndex()) const override
        {
            const auto var = subscription.getVariable();
            if (var.empty() || parent.isValid()) {
                return 0;
            }
            const auto si = var.getSizeInfo();
            return si.columns();
        }

        Qt::ItemFlags flags(const QModelIndex &index) const override
        {
            return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
        }

        QVariant
        data(const QModelIndex &index,
             int role = Qt::DisplayRole) const override;

        size_t offset(const QModelIndex &index) const
        {
            return index.row()
                    * subscription.getVariable().getSizeInfo().columns()
                    + index.column();
        }

        bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;


    private:
        void newValues(std::chrono::nanoseconds) override;
        void stateChanged(PdCom::Subscription const &subscription) override;
};

class MultiDimParameterDialog: public QDialog
{
        Q_OBJECT

        QLabel *label;
        QTableView *tableView;

    public:
        explicit MultiDimParameterDialog(QWidget *parent = nullptr);

        void setVariable(PdCom::Variable var);
};
