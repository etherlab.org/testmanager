/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2018  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef SLOT_NODE_H
#define SLOT_NODE_H

/****************************************************************************/

#include <QObject>
#include <QColor>
#include <QUrl>
#include <QCoreApplication>
#include <QModelIndex>

/****************************************************************************/

class DataModel;
class SlotModelCollection;
class SlotModel;
class DataSlot;
class DataNode;
class Selector;

/****************************************************************************/

class SlotNode
{
        Q_DECLARE_TR_FUNCTIONS(SlotNode)

    public:
        enum ColumnNames {
            UrlColumn,
            AliasColumn,
            SelectorColumn,
            PeriodColumn,
            ScaleColumn,
            OffsetColumn,
            TauColumn,
            ColorColumn,

            _columnCount, // keep this as last item!
        };

        SlotNode(SlotNode *, const QString & = QString(), int pos = -1);

        virtual void fromJson(const QJsonValue &);
        virtual QJsonValue toJson() const;

        virtual QVariant nodeData(int, int, const DataModel * = NULL) const;
        virtual void nodeFlags(Qt::ItemFlags &, int) const;
        virtual bool nodeSetData(int, const QVariant &);

        SlotNode *getParentNode() const { return parent; }
        const SlotModel *getSlotModel() const;
        SlotNode *getChildNode(int) const;
        int getChildIndex(const SlotNode *child) const {
            for (int i = 0; i < children.size(); ++i)
                if (children[i] == child)
                    return i;
            return -1;
        }
        int getChildCount() const {
            return children.size();
        }

        void dump(const QString & = QString()) const;

        void applyToAll(const SlotNode *, int);

        virtual void countColors(QMap<QRgb, unsigned int> &) const;

        virtual void appendDataSources(DataModel *);
        virtual void disconnectDataSources();

        // Node can be deleted by user of application
        virtual bool supportsRemovingByUser() const;

        enum class AcceptedDataConnectionModes {
            None,
            VariableButNoSelector,
            Scalar,
            MatrixOrScalar,
            Process,
        };

        virtual AcceptedDataConnectionModes canInsertDataNodes(
                QList<DataNode *> const &,
                int row,
                QString &err,
                bool replace) const;
        virtual bool insertDataNodes(
                QList<DataNode *> const &,
                const Selector &selector,
                int row,
                QString &err,
                bool replace);

        void collectDataSlots(QList<const DataSlot *> &) const;
        void collectUrls(QSet<QUrl> &) const;

        void replaceSlotUrls(const QUrl &, const QUrl &);

        bool slotsUse(const QUrl &) const;

        QList<QUrl> getUrls() const;

        QModelIndex getIndex();

    protected:
        SlotModelCollection& getRoot();
        virtual ~SlotNode();

    private:
        SlotNode * const parent;
        const QString nodeName;
        QList<SlotNode *> children;

        SlotNode();
        void removeChild(int);
        void clearChildren();

        friend class SlotModelCollection;
};

class SingleVariableCategory : public SlotNode
{
    public:
        using SlotNode::SlotNode;

        AcceptedDataConnectionModes canInsertDataNodes(
        QList<DataNode *> const &nodes,
        int /* row */,
        QString &err, bool replace) const override;

        virtual void clear();

        void nodeFlags(Qt::ItemFlags &flags, int section) const override;
};

/****************************************************************************/

#endif
