/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2018  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef SLOT_MODEL_H
#define SLOT_MODEL_H

/****************************************************************************/

#include "SlotNode.h"

namespace QtPdCom {
class ScalarSubscriber;
}

#include <QCoreApplication>
#include <QObject>
#include <QSet>
#include <QUrl>
#include <memory>

class Selector;
class SlotModel;
class SlotModelCollection;
class WidgetContainer;


class SlotModel:
    public QObject,
    public SlotNode
{
    Q_OBJECT

    public:
        SlotModel(SlotModelCollection &parent, WidgetContainer&);

        static void initCustomColors();
        QColor nextColor() const;

        QList<const DataSlot *> getDataSlots() const;
        QSet<QUrl> getUrls() const;

        virtual void clear() {} // = 0;

        QVariant nodeData(int, int, const DataModel *) const override;
        void nodeFlags(Qt::ItemFlags &flags, int section) const override;
        bool nodeSetData(int section, const QVariant &data) override;

        static const QColor colorList[];
        WidgetContainer *const container;
        QWidget *getWidget() const;

        virtual void clearWidgetData() const {}
};

template <class BaseClass> class SimpleSlotModelBase: public BaseClass
{
    public:
        template <typename... Args>
        SimpleSlotModelBase(QtPdCom::ScalarSubscriber &sub, Args &&...args):
            BaseClass(std::forward<Args>(args)...),
            subscriber(sub)
        {}


        void fromJson(const QJsonValue &val) override;

        SlotModel::AcceptedDataConnectionModes canInsertDataNodes(
                QList<DataNode *> const &,
                int row,
                QString &err,
                bool replace) const override;
        bool insertDataNodes(
                QList<DataNode *> const &,
                const Selector &selector,
                int row,
                QString &err,
                bool replace) override;

        void clear();

        void nodeFlags(Qt::ItemFlags &, int) const override;

    protected:
        QtPdCom::ScalarSubscriber &subscriber;
};


/**
 * SlotModel with one Slot which is assumed to be writeable.
 * The widget must derive from QtPdCom::ScalarSubscriber.
 */
class SimpleRWSlotModel: public SimpleSlotModelBase<SlotModel>
{
        Q_DECLARE_TR_FUNCTIONS(SimpleRWSlotModel)

    public:
        SimpleRWSlotModel(
                SlotModelCollection &parent,
                WidgetContainer &container,
                QtPdCom::ScalarSubscriber &sub):
            SimpleSlotModelBase(sub, parent, container)
        {}

        AcceptedDataConnectionModes canInsertDataNodes(
                QList<DataNode *> const &,
                int row,
                QString &err, bool replace) const override;
};

/**
 * SlotModel with one Slot which is not assumed to be writeable.
 * The widget must derive from QtPdCom::ScalarSubscriber.
 */
class SimpleROSlotModel: public SimpleSlotModelBase<SlotModel>
{
    public:
        SimpleROSlotModel(
                SlotModelCollection &parent,
                WidgetContainer &container,
                QtPdCom::ScalarSubscriber &sub):
            SimpleSlotModelBase(sub, parent, container)
        {}
};


#endif  // SLOT_MODEL_H
