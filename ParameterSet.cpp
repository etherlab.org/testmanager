/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2023  Daniel Ramirez <dr@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "ParameterSet.h"

#include "ParameterSetModel.h"
#include "ParameterModel.h"
#include "MainWindow.h"
#include "DataNode.h"

#include <QJsonObject>
#include <QDebug>

/*****************************************************************************
 * public
 ****************************************************************************/

ParameterSet::ParameterSet(ParameterSetModel *model):
    model(model)
{
}

/****************************************************************************/

ParameterSet::~ParameterSet()
{
}

/****************************************************************************/

void ParameterSet::setParameterModel(ParameterModel *model)
{
    parameterModel = model;
}

/****************************************************************************/

void ParameterSet::fromJson(const QJsonValue &value)
{
    QJsonObject obj(value.toObject());

    if (obj.contains("name")) {
        setName(obj["name"].toString());
    }

    if (obj.contains("set")) {
        QJsonArray parameterSetArray(obj["set"].toArray());
        parameterModel->fromJson(parameterSetArray);
    }
}

/****************************************************************************/

QJsonValue ParameterSet::toJson() const
{
    QJsonObject obj;

    obj["name"] = name;

    bool withValue = false;

    QJsonArray parameterArray = parameterModel->toJson(withValue);

    obj["set"] = parameterArray;

    return obj;
}

/****************************************************************************/

void ParameterSet::appendDataSources(DataModel *dataModel) const
{
    parameterModel->appendDataSources(dataModel);
}

/****************************************************************************/

void ParameterSet::connectSets()
{
    parameterModel->connectParameters();
}

/****************************************************************************/
