/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2023 Daniel Ramirez <dr@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "ParameterSetModel.h"
#include "ParameterModel.h"

#include "MainWindow.h"

#include <QMimeData>
#include <QDebug>

/****************************************************************************/

ParameterSetModel::ParameterSetModel(MainWindow *mainWindow):
    QAbstractListModel(mainWindow),
    mainWindow(mainWindow),
    created(0)
{
}

/****************************************************************************/

ParameterSetModel::~ParameterSetModel()
{
    clear();
}

/****************************************************************************/

void ParameterSetModel::clear()
{
    if (parameterSets.isEmpty()) {
        return;
    }

    beginResetModel();
    foreach (ParameterSet *set, parameterSets) {
        delete set;
    }
    parameterSets.clear();
    endResetModel();
}

/****************************************************************************/

void ParameterSetModel::fromJson(const QJsonArray &array)
{
    // do not use current model, since this is reserved for visualization
    // of empty model.
    if (array.isEmpty()) {
        return;
    }

    beginInsertRows(QModelIndex(), 0, array.size() - 1);
    foreach (QJsonValue arrayValue, array) {
        ParameterSet *set = new ParameterSet(this);

        ParameterModel *model = new ParameterModel(mainWindow);
        set->setParameterModel(model);
        
        set->fromJson(arrayValue);

        parameterSets.append(set);
    }
    endInsertRows();
}

/****************************************************************************/

QJsonArray ParameterSetModel::toJson() const
{
    QJsonArray array;

    foreach (const ParameterSet *set, parameterSets) {
        array.append(set->toJson());
    }

    return array;
}

/****************************************************************************/

void ParameterSetModel::appendDataSources(DataModel *dataModel) const
{
    foreach (const ParameterSet *parameterSet, parameterSets) {
        parameterSet->appendDataSources(dataModel);
    }
}

/****************************************************************************/

int ParameterSetModel::rowCount(const QModelIndex &parent) const
{
    int ret = 0;

    if (parent.isValid()) {
        qWarning() << __func__ << parent << "inval";
    }
    else {
        ret = parameterSets.size();
    }

#ifdef DEBUG_MODEL
    qDebug() << __func__ << parent << ret;
#endif

    return ret;
}

/****************************************************************************/

int ParameterSetModel::columnCount(const QModelIndex &) const
{
    return 1; // name
}

/****************************************************************************/

QVariant ParameterSetModel::data(const QModelIndex &index, int role) const
{
    QVariant ret;

    if (index.isValid()) {
        const ParameterSet *set = parameterSets[index.row()];

        switch (role) {

            case Qt::DisplayRole:
            case Qt::EditRole:
                switch (index.column()) {
                    case 0:
                        ret = set->getName();
                        break;
                }
                break;
            default:
                break;
        }
    }

#ifdef DEBUG_MODEL
    if (role <= 1) {
        qDebug() << __func__ << index << role << ret;
    }
#endif

    return ret;
}

/****************************************************************************/

Qt::ItemFlags ParameterSetModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags ret = QAbstractListModel::flags(index);

    if (index.isValid()) {
        ret |= Qt::ItemIsEditable;
    }
    else {
        ret |= Qt::ItemIsDropEnabled;
    }

#ifdef DEBUG_MODEL
    qDebug() << __func__ << index << ret;
#endif

    return ret;
}

/****************************************************************************/

bool ParameterSetModel::setData(const QModelIndex &index,
        const QVariant &value, int role)
{
    bool ret = false;

    if (index.isValid() && role == Qt::EditRole) {
        ParameterSet *set = parameterSets[index.row()];

        switch (index.column()) {
            case 0:
                set->setName(value.toString());
                ret = true;
                break;
            default:
                break;
        }
    }

    return ret;
}

/****************************************************************************/

void ParameterSetModel::connectSets()
{
    for (int row = 0; row < parameterSets.size(); row++) {
        parameterSets[row]->connectSets();
        emit dataChanged(index(row), index(row));
    }
}

/****************************************************************************/

void ParameterSetModel::replaceUrl(const QUrl &oldUrl, const QUrl &newUrl)
{
    for (int row = 0; row < parameterSets.size(); row++) {
        parameterSets[row]->getParameterModel()->replaceUrl(oldUrl, newUrl);
    }
}

/****************************************************************************/

ParameterSet *ParameterSetModel::getParameterSet(
        const QModelIndex &index) const
{
    if (index.row() < 0 or index.row() >= parameterSets.size()) {
        return nullptr;
    }

    return parameterSets[index.row()];
}

/****************************************************************************/

void ParameterSetModel::remove(ParameterSet *set)
{
    for (int row = 0; row < parameterSets.size(); row++) { // FIXME
        if (parameterSets[row] == set) {
            beginRemoveRows(QModelIndex(), row, row);
            parameterSets.removeAt(row);
            endRemoveRows();
            delete set;
            return;
        }
    }
}

/****************************************************************************/

int ParameterSetModel::newParameterSet()
{
    int insertPos;
    if (parameterSets.isEmpty()) {
        insertPos = 0;
    }
    else {
        insertPos = parameterSets.size();
    }

    ParameterSet *parameterSet = new ParameterSet(this);
    parameterSet->setName(
            tr("Parameter Set %1").arg(QString::number(++created)));

    parameterSet->setParameterModel(new ParameterModel(mainWindow));

    beginInsertRows(QModelIndex(), insertPos, insertPos);
    parameterSets.insert(insertPos, parameterSet);
    endInsertRows();
    
    return insertPos;
}

/****************************************************************************/
