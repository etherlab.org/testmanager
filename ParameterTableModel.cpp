/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2023 Daniel Ramirez <dr@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "ParameterTableModel.h"

#include "ParameterDialog.h"
#include "MainWindow.h"

#include <QDebug>
#include <QDialog>
#include <QDir>
#include <QFileDialog>
#include <QFileInfo>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMessageBox>

/****************************************************************************/

ParameterTableModel::ParameterTableModel(MainWindow *mainWindow):
    ParameterModel(mainWindow)
{
}

/****************************************************************************/

ParameterTableModel::~ParameterTableModel()
{
    clear();
}

/****************************************************************************/

int ParameterTableModel::columnCount(const QModelIndex &) const
{
    return 4; // name, current value, saved value, url
}

/****************************************************************************/

QVariant ParameterTableModel::data(const QModelIndex &index, int role) const
{
    QVariant ret;

    if (index.isValid()) {
        Parameter *parameter = parameters[index.row()];

        switch (role) {
            case Qt::DisplayRole:
            case Qt::EditRole:
                switch (index.column()) {
                    case 0: // Name
                        ret = parameter->getName();
                        break;
                    case 1: // Current value
                        if (parameter->hasData()) {
                            ret = buildVectorString(
                                    parameter->getCurrentValue());
                        }
                        break;
                    case 2: // Saved value
                        if (parameter->hasSavedData()) {
                            ret = buildVectorString(
                                    parameter->getSavedValue());
                        }
                        break;
                    case 3: // Variable URL
                        ret = parameter->getUrl();
                        break;
                }
                break;
            case Qt::BackgroundRole:
                if (index.flags() & static_cast<Qt::ItemFlags>(SubInactive)) {
                    QBrush grayBrush(QColor(192, 192, 192)); // Adjust the color as needed
                    ret = grayBrush;
                }
                break;

            case Qt::DecorationRole:
                switch (index.column()) {
                    case 2: // Saved Value
                        if (parameter->hasData() and
                                (parameter->getCurrentValue() !=
                                 parameter->getSavedValue()
                                 or not parameter->hasSavedData())) {
                            ret = QIcon(":images/dialog-warning.svg");
                        }
                        break;
                    case 3: // Variable URL
                        ret = parameter->getIcon();
                        break;
                }
                break;
            case Qt::ToolTipRole:
                {
                    QString toolTip;
                    if (not parameter->getUrl().isEmpty()) {
                        toolTip += tr("Path: %1").arg(
                            parameter->getUrl().
                            adjusted(QUrl::RemoveAuthority
                            | QUrl::RemoveScheme).toString());
                    }
                    QString dimStr;
                    if (not parameter->getVariable()->empty()) {
                        dimStr = dimensionString(
                            parameter->getVariable()->getSizeInfo());
                        if (!dimStr.isEmpty()) {
                            if (!toolTip.isEmpty()) {
                                toolTip += "\n";
                            }
                            toolTip += tr("Dimension: %1").arg(dimStr);
                        }
                        if (!toolTip.isEmpty()) {
                            toolTip += "\n";
                        }
                    }

                    return toolTip;
                }
                break;

            default:
                break;
        }
    }

#ifdef DEBUG_MODEL
    if (role <= 1) {
        qDebug() << __func__ << index << role << ret;
    }
#endif

    return ret;
}

/****************************************************************************/

QVariant ParameterTableModel::headerData(
        int section,
        Qt::Orientation orientation,
        int role
        ) const
{
    QVariant ret;

    if (orientation == Qt::Horizontal && role == Qt::DisplayRole) {
        switch (section) {
            case 0:
                ret = tr("Name");
                break;
            case 1:
                ret = tr("Current value");
                break;
            case 2:
                ret = tr("Stored value");
                break;
            case 3:
                ret = tr("Variable");
                break;
        }
    }

    return ret;
}

/****************************************************************************/

Qt::ItemFlags ParameterTableModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags ret = QAbstractTableModel::flags(index);

    if (index.isValid()) {
        Parameter *parameter = parameters[index.row()];
        if (not parameter->getSubscription()->empty()) {
            if (index.column() == 0 or index.column() == 2) {
                ret |= Qt::ItemIsEditable;
            }
        }
        else {
            ret |= static_cast<Qt::ItemFlags>(SubInactive);
        }
    }
    else {
        ret |= Qt::ItemIsDropEnabled;
    }

#ifdef DEBUG_MODEL
    qDebug() << __func__ << index << ret;
#endif

    return ret;
}

/****************************************************************************/

bool ParameterTableModel::setData(const QModelIndex &index,
        const QVariant &value, int role)
{
    bool ret = false;

    if (index.isValid() && role == Qt::EditRole) {
        Parameter *parameter = parameters[index.row()];
        PdCom::Variable *pv{parameter->getVariable()};

        switch (index.column()) {
            case 0:
                parameter->setName(value.toString());
                ret = true;
                break;
            case 2:
                if (not value.isNull() and not pv->empty()) {
                    QVector<double> values =
                        parseVectorString(value.toString());
                    if (values.empty()) {
                        return false;
                    }
                    return parameter->setSavedValue(values);
                }
                break;
            default:
                break;
        }
    }

    return ret;
}

/****************************************************************************/

void ParameterTableModel::applyParameters()
{
    for (int row = 0; row < parameters.size(); row++) {
        parameters[row]->setCurrentValue(parameters[row]->getSavedValue());
    }
}

/****************************************************************************/

void ParameterTableModel::saveParametersAs()
{
    auto dialog = new QFileDialog(mainWindow);

    QStringList filters;
    filters
        << tr("Parameter Files (*.par)")
        << tr("Any files (*)");

    dialog->setNameFilters(filters);
    dialog->setDefaultSuffix("par");

    connect(dialog, &QDialog::finished, dialog, &QObject::deleteLater);

    connect(dialog, &QDialog::accepted, this, [this, dialog]
            {
                setPath(dialog->selectedFiles()[0]);

                // store directory for opening the dialog next time
                QFileInfo file(getPath());
                QString searchPath(file.absoluteDir().path());
                QDir::setSearchPaths("parameters", QStringList(searchPath));

                saveParameters();
            });

    dialog->open();
}

/****************************************************************************/

void ParameterTableModel::loadParameters()
{
    QFileDialog fileDialog(mainWindow);

    QStringList filters;
    filters
        << tr("Parameter Files (*.par)")
        << tr("Any files (*)");

    fileDialog.setNameFilters(filters);
    fileDialog.setDefaultSuffix("par");

    connect(&fileDialog, &QDialog::accepted, this, [this, &fileDialog]
            {
                QString path = fileDialog.selectedFiles()[0];
                QFile file(path);
                if (!file.open(QIODevice::ReadOnly)) {
                    fileError(tr("Failed to open %1.").arg(path));
                    return;
                }

                QByteArray ba = file.readAll();
                file.close();

                QJsonParseError err;
                QJsonDocument doc(QJsonDocument::fromJson(ba, &err));

                if (err.error != QJsonParseError::NoError) {
                    fileError(
                            tr("Parameter file parsing error"
                                " (%1) at offset %2: %3")
                            .arg(err.error, 1)
                            .arg(err.offset, 2)
                            .arg(err.errorString(), 3));
                    return;
                }

                parameterPath = path;
                QFileInfo fi(parameterPath);
                QString searchPath(fi.absoluteDir().path());
                QDir::setSearchPaths("parameters", QStringList(searchPath));

                QJsonObject mainObj(doc.object());

                user = mainObj.contains("created by") ?
                    mainObj["created by"].toString() : QString();
                creator = mainObj.contains("creator") ?
                    mainObj["creator"].toString() : QString();
                date = mainObj.contains("creation date") ?
                    mainObj["creation date"].toString() : QString();
                description = mainObj.contains("description") ?
                    mainObj["description"].toString() : QString();

                QJsonArray parameterArray(
                        mainObj["parameters"].toArray());
                clear();

                try {
                    fromJson(parameterArray);
                }
                catch (std::exception &e) {
                    fileError(
                            tr("Invalid content in parameter file %1: %2")
                            .arg(path, 1)
                            .arg(e.what(), 2));
                    return;
                }

                ParameterDialog pDialog(this);
                pDialog.exec();
            });

    fileDialog.exec();
}

/****************************************************************************/

void ParameterTableModel::fileError(const QString &msg)
{
    auto *messageBox = new QMessageBox(
            tr("Parameter file error"),
            msg,
            QMessageBox::Critical,
            QMessageBox::Ok,
            QMessageBox::NoButton,
            QMessageBox::NoButton,
            mainWindow);
    connect(messageBox, &QDialog::finished,
            messageBox, &QObject::deleteLater);
    messageBox->open();
}

/****************************************************************************/
