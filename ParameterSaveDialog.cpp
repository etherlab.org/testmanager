/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2023 Daniel Ramirez <dr@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "ParameterSaveDialog.h"

#include "DataSlot.h"
#include "MainWindow.h"
#include "ParameterModel.h"
#include "Plugin.h"
#include "SlotModel.h"

#include <QDebug>
#include <QDialogButtonBox>
#include <QFileDialog>
#include <QMenu>

/****************************************************************************/

ParameterSaveDialog::ParameterSaveDialog(
        ParameterModel *model,
        QWidget *parent
        ):
    QDialog(parent),
    model(model)
{
    setupUi(this);

    connect(pushButtonSaveDialog, SIGNAL(clicked()),
            this, SLOT(openSaveDialog()));

    connect(buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);

    connect(textEditDescription, &QTextEdit::textChanged, this,
            [this, model]() 
            {
                model->setDescription(textEditDescription->toPlainText());
            });

    textEditDescription->setText(model->getDescription());
    buttonBox->setEnabled(true);
    buttonBox->button(QDialogButtonBox::Cancel)->setEnabled(true);
    buttonBox->button(QDialogButtonBox::Save)->setEnabled(false);
}

/****************************************************************************/

ParameterSaveDialog::~ParameterSaveDialog()
{
}

/****************************************************************************/

void ParameterSaveDialog::openSaveDialog()
{
    auto dialog = new QFileDialog(this);

    QStringList filters;
    filters
        << tr("Parameter Files (*.par)")
        << tr("Any files (*)");

    dialog->setNameFilters(filters);
    dialog->setDefaultSuffix("par");
    
    connect(dialog, &QDialog::finished, dialog, &QObject::deleteLater);

    connect(dialog, &QDialog::accepted, this, [this, dialog]
            {
                model->setPath(dialog->selectedFiles()[0]);
                lineEditSavePath->setText(model->getPath());
                buttonBox->button(QDialogButtonBox::Save)->
                    setEnabled(not model->getPath().isEmpty());
            });

    dialog->open();
}
