#-----------------------------------------------------------------------------
#
# Testmanager - Graphical Automation and Visualisation Tool
#
# Copyright (C) 2018-2023  Florian Pose <fp@igh.de>
#                    2023  Bjarne von Horn <vh@igh.de>
#
# This file is part of Testmanager.
#
# Testmanager is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Testmanager is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with Testmanager. If not, see <http://www.gnu.org/licenses/>.
#
#-----------------------------------------------------------------------------

cmake_minimum_required(VERSION 3.10)

project(testmanager VERSION 1.1.5)

option(MODEL_TEST "enable model tests" OFF)
option(USE_QT6 "Use Qt6 instead of Qt5" OFF)
option(PUT_QT_MAJOR_INTO_LIBNAME "Put Qt Major Version into Library Name" OFF)


if(USE_QT6)
    set(Qt Qt6)
    find_package(${Qt} REQUIRED COMPONENTS Core5Compat)
else()
    set(Qt Qt5)
endif()

# load Qt
find_package(${Qt} REQUIRED COMPONENTS Gui Network Svg Widgets Xml LinguistTools)
if (MODEL_TEST)
    find_package(${Qt} REQUIRED COMPONENTS Test)
endif()

if (PUT_QT_MAJOR_INTO_LIBNAME)
    set(QtPdWidgets "${Qt}PdWidgets2")
    set(QtPdCom "${Qt}PdCom1")
else()
    set(QtPdWidgets "QtPdWidgets2")
    set(QtPdCom "QtPdCom1")
endif()

# load PdWidgets
find_package(${QtPdCom} REQUIRED)
find_package(${QtPdWidgets} REQUIRED)

# find numpy include path
if (NOT NUMPY_INCLUDE_DIR)
    find_package(PythonInterp REQUIRED)
    execute_process(COMMAND ${PYTHON_EXECUTABLE} -c "import numpy; print(numpy.get_include())"
        OUTPUT_VARIABLE NUMPY_INCLUDE_DIR
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
endif()
find_package(PythonLibs REQUIRED)

if (NOT VERSION)
    execute_process(COMMAND sh revision.sh .
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
        OUTPUT_VARIABLE VERSION
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
endif()

add_compile_options(-Wall -Wextra)

include(GNUInstallDirs)

set(TS_FILES
    testmanager_de.ts
)

configure_file(testmanager-lang.qrc "${CMAKE_CURRENT_BINARY_DIR}" COPYONLY)
if (USE_QT6)
    qt6_add_translation(QM_FILES ${TS_FILES})
else()
    qt5_add_translation(QM_FILES ${TS_FILES})
endif()

set(HEADERS
    AboutDialog.h
    BroadcastDialog.h
    ColorDelegate.h
    ConnectDialog.h
    DataModel.h
    DataNode.h
    DataSlot.h
    DataSource.h
    HandleWidget.h
    Legend.h
    LoginDialog.h
    MainWindow.h
    MessageDialog.h
    MessageWidget.h
    MultiDimParameterDelegate.h
    MultiDimParameterDelegate_p.h
    Parameter.h
    ParameterModel.h
    ParameterTableModel.h
    ParameterSet.h
    ParameterSetModel.h
    ParameterDialog.h
    ParameterItemDelegate.h
    ParameterSaveDialog.h
    Plugin.h
    PropertyDelegate.h
    PropertyFlag.h
    PropertyGroup.h
    Property.h
    PropertyModel.h
    PropertyNode.h
    PropertyTree.h
    ReplaceDialog.h
    ScalarSubscriberPluginImpl.h
    ScaleDelegate.h
    ScriptVariable.h
    ScriptVariableModel.h
    Selector.h
    SelectorDelegate.h
    SelectorDialog.h
    SlotDialog.h
    SlotModel.h
    SlotModelCollection.h
    SlotNode.h
    SlotView.h
    SlotViewFilter.h
    SourceDelegate.h
    SourceTree.h
    StyleDialog.h
    TabDialog.h
    TabPage.h
    TooltipMatrix.h
    WidgetContainer.h

    plugins/BarPlugin.h
    plugins/CheckBoxPlugin.h
    plugins/DialPluginDialog.h
    plugins/DialPlugin.h
    plugins/DigitalPlugin.h
    plugins/DoubleSpinBoxPlugin.h
    plugins/GraphPlugin.h
    plugins/LedPlugin.h
    plugins/ParameterSetWidgetPlugin.h
    plugins/PushButtonPlugin.h
    plugins/QLabelPlugin.h
    plugins/RadioButtonPlugin.h
    plugins/SendBroadcastPlugin.h
    plugins/SvgPluginDialog.h
    plugins/SvgPlugin.h
    plugins/TableViewPluginDialog.h
    plugins/TableViewPlugin.h
    plugins/TableViewPluginPrivate.h
    plugins/TextPluginDialog.h
    plugins/TextPlugin.h
    plugins/TouchEditPlugin.h
    plugins/XYGraphPlugin.h
)

set(SOURCES
    AboutDialog.cpp
    BroadcastDialog.cpp
    ColorDelegate.cpp
    ConnectDialog.cpp
    DataModel.cpp
    DataNode.cpp
    DataSlot.cpp
    DataSource.cpp
    HandleWidget.cpp
    Legend.cpp
    LoginDialog.cpp
    main.cpp
    MainWindow.cpp
    MessageDialog.cpp
    MessageWidget.cpp
    MultiDimParameterDelegate.cpp
    Parameter.cpp
    ParameterModel.cpp
    ParameterTableModel.cpp
    ParameterSet.cpp
    ParameterSetModel.cpp
    ParameterDialog.cpp
    ParameterItemDelegate.cpp
    ParameterSaveDialog.cpp
    Plugin.cpp
    Property.cpp
    PropertyDelegate.cpp
    PropertyFlag.cpp
    PropertyGroup.cpp
    PropertyModel.cpp
    PropertyNode.cpp
    PropertyTree.cpp
    ReplaceDialog.cpp
    ScaleDelegate.cpp
    ScriptVariable.cpp
    ScriptVariableModel.cpp
    Selector.cpp
    SelectorDelegate.cpp
    SelectorDialog.cpp
    SlotDialog.cpp
    SlotModel.cpp
    SlotModelCollection.cpp
    SlotNode.cpp
    SlotView.cpp
    SlotViewFilter.cpp
    SourceDelegate.cpp
    SourceTree.cpp
    StyleDialog.cpp
    TabDialog.cpp
    TabPage.cpp
    TooltipMatrix.cpp
    WidgetContainer.cpp

    plugins/BarPlugin.cpp
    plugins/CheckBoxPlugin.cpp
    plugins/DialPlugin.cpp
    plugins/DialPluginDialog.cpp
    plugins/DigitalPlugin.cpp
    plugins/DoubleSpinBoxPlugin.cpp
    plugins/GraphPlugin.cpp
    plugins/LedPlugin.cpp
    plugins/ParameterSetWidgetPlugin.cpp
    plugins/PushButtonPlugin.cpp
    plugins/QLabelPlugin.cpp
    plugins/RadioButtonPlugin.cpp
    plugins/SendBroadcastPlugin.cpp
    plugins/SvgPlugin.cpp
    plugins/SvgPluginDialog.cpp
    plugins/TableViewPlugin.cpp
    plugins/TableViewPluginDialog.cpp
    plugins/TableViewPluginPrivate.cpp
    plugins/TextPlugin.cpp
    plugins/TextPluginDialog.cpp
    plugins/TouchEditPlugin.cpp
    plugins/XYGraphPlugin.cpp
)

set(UI_FILES
    AboutDialog.ui
    BroadcastDialog.ui
    LoginDialog.ui
    MainWindow.ui
    MessageDialog.ui
    MessageWidget.ui
    ParameterDialog.ui
    ParameterSaveDialog.ui
    ReplaceDialog.ui
    SelectorDialog.ui
    SlotDialog.ui
    StyleDialog.ui
    TabDialog.ui

    plugins/DialPluginDialog.ui
    plugins/SvgPluginDialog.ui
    plugins/TableViewPluginDialog.ui
    plugins/TextPluginDialog.ui
)

if (UNIX)
    list(APPEND HEADERS SignalReceiver.h)
    list(APPEND SOURCES SignalReceiver.cpp)
endif()

if (MODEL_TEST)
    list(APPEND HEADERS modeltest.h)
    list(APPEND SOURCES modeltest.cpp)
endif()

add_executable(${PROJECT_NAME}
    ${HEADERS}
    ${SOURCES}
    ${UI_FILES}

    testmanager.qrc
    testmanager.rc
    "${CMAKE_CURRENT_BINARY_DIR}/testmanager-lang.qrc"
    ${QM_FILES}
)

# replacement for lupdate-pro
# just run `make lupdate` in your build dir after running cmake
add_custom_target(lupdate
    ${Qt}::lupdate -locations none ${UI_FILES} ${SOURCES} ${HEADERS} -ts ${TS_FILES}
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
)

# enable moc, uic and rcc
set_target_properties(${PROJECT_NAME} PROPERTIES
    AUTOUIC 1
    AUTOMOC 1
    AUTORCC 1
    WIN32_EXECUTABLE 1
)

# link against Qt and PdWidgets
target_link_libraries(${PROJECT_NAME} PUBLIC
    EtherLab::${QtPdCom}
    EtherLab::${QtPdWidgets}
    ${Qt}::Gui
    ${Qt}::Network
    ${Qt}::Svg
    ${Qt}::Widgets
    ${PYTHON_LIBRARIES}
)

if (USE_QT6)
    target_link_libraries(${PROJECT_NAME} PUBLIC
        Qt6::Core5Compat
    )
endif()

target_include_directories(${PROJECT_NAME} PUBLIC
    "${CMAKE_CURRENT_SOURCE_DIR}"
    ${PYTHON_INCLUDE_DIRS}
    ${NUMPY_INCLUDE_DIR}
)

install(TARGETS ${PROJECT_NAME}
    ARCHIVE DESTINATION "${CMAKE_INSTALL_LIBDIR}"
    LIBRARY DESTINATION "${CMAKE_INSTALL_LIBDIR}"
    RUNTIME DESTINATION  "${CMAKE_INSTALL_BINDIR}"
)

if(COMMIT_DATE)
    target_compile_definitions(${PROJECT_NAME} PRIVATE COMMIT_DATE=${COMMIT_DATE})
endif()

target_compile_definitions(${PROJECT_NAME} PRIVATE VERSION=${VERSION})

if (MODEL_TEST)
    target_link_libraries(${PROJECT_NAME} PRIVATE ${Qt}::Test)
    target_compile_definitions(${PROJECT_NAME} PRIVATE TM_MODEL_TEST)
endif()

# Install example layout
install(FILES example.tml DESTINATION share/testmanager)
