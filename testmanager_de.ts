<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE" sourcelanguage="en_GB">
<context>
    <name>AboutDialog</name>
    <message>
        <source>About Testmanager</source>
        <translation>Über den Testmanager</translation>
    </message>
    <message>
        <source>Testmanager – Next Generation</source>
        <translation>Testmanager – Next Generation</translation>
    </message>
    <message>
        <source>EtherLab&apos;s Open-Source Automation and Visualisation Tool, based on the work of Dr.-Ing. Wilhelm Hagemeister.</source>
        <translation>Freies Automatisierungs- und Visualisierungs-Werkzeug der EtherLab-Technologie, basierend auf der Arbeit von Dr.-Ing. Wilhelm Hagemeister.</translation>
    </message>
</context>
<context>
    <name>BarPlugin</name>
    <message>
        <source>Bar</source>
        <translation>Balken</translation>
    </message>
</context>
<context>
    <name>BroadcastDialog</name>
    <message>
        <source>Data Source</source>
        <translation>Datenquelle</translation>
    </message>
    <message>
        <source>Received Broadcasts</source>
        <translation>Empfangene Broadcasts</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>Löschen</translation>
    </message>
</context>
<context>
    <name>BroadcastSlotModel</name>
    <message>
        <source>Exactly one connection possible.</source>
        <translation>Maximal eine Verbindung möglich.</translation>
    </message>
    <message>
        <source>Please select a Data Source</source>
        <translation>Datenquelle anstatt Variable erforderlich</translation>
    </message>
    <message>
        <source>Already connected. Press Shift to replace.</source>
        <translation>Verbindung bereits hergestellt. Durch Drücken von &apos;Shift&apos; ersetzen.</translation>
    </message>
</context>
<context>
    <name>CheckBoxPlugin</name>
    <message>
        <source>CheckBox</source>
        <translation>Markierungsfeld</translation>
    </message>
</context>
<context>
    <name>ConnectDialog</name>
    <message>
        <source>Connect...</source>
        <translation>Verbinden...</translation>
    </message>
    <message>
        <source>Data so&amp;urce URL:</source>
        <translation>&amp;Datenquellen-URL:</translation>
    </message>
    <message>
        <source>Use system-wide CAs</source>
        <translation>Verwende CAs des Systems</translation>
    </message>
    <message>
        <source>Use custom CA</source>
        <translation>Verwende spezielle CA</translation>
    </message>
    <message>
        <source>Ignore certificates</source>
        <translation>Ignoriere Zertifikate</translation>
    </message>
    <message>
        <source>Unsupported scheme.</source>
        <translation>Nicht unterstütztes URL-Schema.</translation>
    </message>
    <message>
        <source>Connecting to %1...</source>
        <translation>Verbinde mit %1...</translation>
    </message>
    <message>
        <source>Failed to connect to %1: %2.</source>
        <translation>Konnte nicht mit %1 verbinden: %2.</translation>
    </message>
    <message>
        <source>Login failed, invalid username or password.</source>
        <translation type="vanished">Anmelden fehlgeschlagen, Benutzername oder Passwort falsch.</translation>
    </message>
    <message>
        <source>Select CA</source>
        <translation>CA auswählen</translation>
    </message>
    <message>
        <source>Certificates</source>
        <translation>Zertifikate</translation>
    </message>
    <message>
        <source>Certificate import failed</source>
        <translation>Import fehlgeschlagen</translation>
    </message>
    <message>
        <source>Loading certificate from file %1 failed</source>
        <translation>Import des Zertifikats aus der Datei %1 fehlgeschlagen</translation>
    </message>
    <message>
        <source>Login failed</source>
        <translation>Anmeldung fehlgeschlagen</translation>
    </message>
</context>
<context>
    <name>DataModel</name>
    <message>
        <source>Sources</source>
        <translation>Datenquellen</translation>
    </message>
    <message>
        <source>Values</source>
        <translation>Werte</translation>
    </message>
</context>
<context>
    <name>DataNode</name>
    <message>
        <source>Variable subscription failed: %1</source>
        <translation>Abonnement der Variable fehlgeschlagen: %1</translation>
    </message>
    <message>
        <source>Path: %1</source>
        <translation>Pfad: %1</translation>
    </message>
    <message>
        <source>Period: </source>
        <translation>Periode: </translation>
    </message>
    <message>
        <source>%1&#x202f;s</source>
        <oldsource>%1â¯s</oldsource>
        <translation>%1&#x202f;s</translation>
    </message>
    <message>
        <source>%1&#x202f;ms</source>
        <oldsource>%1â¯ms</oldsource>
        <translation>%1&#x202f;ms</translation>
    </message>
    <message>
        <source>%1&#x202f;µs</source>
        <oldsource>%1â¯Âµs</oldsource>
        <translation>%1&#x202f;µs</translation>
    </message>
    <message>
        <source>Dimension: %1</source>
        <translation>Dimension: %1</translation>
    </message>
</context>
<context>
    <name>DataSource</name>
    <message>
        <source>%2 ⇄ %3 KB/s</source>
        <oldsource>%2 â %3 KB/s</oldsource>
        <translation>%2 ⇄ %3 KB/s</translation>
    </message>
    <message>
        <source>Server</source>
        <translation>Server</translation>
    </message>
    <message>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Verschlüsselung</translation>
    </message>
    <message>
        <source>none</source>
        <translation>keine</translation>
    </message>
    <message>
        <source>untrusted</source>
        <translation>nicht vertrauenswürdig</translation>
    </message>
    <message>
        <source>trusted</source>
        <translation>vertrauenswürdig</translation>
    </message>
    <message>
        <source>TestManager</source>
        <translation>Testmanager</translation>
    </message>
</context>
<context>
    <name>DialPlugin</name>
    <message>
        <source>Dial / Analog Gauge</source>
        <translation>Zeigerinstrument</translation>
    </message>
</context>
<context>
    <name>DialPluginDialog</name>
    <message>
        <source>Color Gradient Editor</source>
        <translation>Farbsverlaufs-Editor</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <source>Color</source>
        <translation>Farbe</translation>
    </message>
</context>
<context>
    <name>DialSlotModel</name>
    <message>
        <source>Please drop the variables onto the widget itself or onto one of its categories.</source>
        <translation>Bitte lassen Sie die Variablen auf das Widget oder auf einer der Kategorien fallen.</translation>
    </message>
    <message>
        <source>This widget accepts up to two variables.</source>
        <translation>Dieses Widget verwendet bis zu zwei Variablen.</translation>
    </message>
</context>
<context>
    <name>DigitalPlugin</name>
    <message>
        <source>Digital Display</source>
        <translation>Digitalanzeige</translation>
    </message>
</context>
<context>
    <name>DoubleSpinBoxPlugin</name>
    <message>
        <source>Parameter Editor</source>
        <translation>Parameter-Editor</translation>
    </message>
</context>
<context>
    <name>GraphModel</name>
    <message>
        <source>Graphs</source>
        <translation>Graphen</translation>
    </message>
    <message>
        <source>Trigger</source>
        <translation>Trigger</translation>
    </message>
</context>
<context>
    <name>GraphPlugin</name>
    <message>
        <source>Graph</source>
        <translation>Graph</translation>
    </message>
</context>
<context>
    <name>LedPlugin</name>
    <message>
        <source>LED Display</source>
        <translation>LED-Anzeige</translation>
    </message>
</context>
<context>
    <name>Legend</name>
    <message>
        <source>Hide</source>
        <translation>Verbergen</translation>
    </message>
    <message>
        <source>Del</source>
        <translation>Del</translation>
    </message>
    <message>
        <source>Text alignment</source>
        <translation>Textausrichtung</translation>
    </message>
    <message>
        <source>Left</source>
        <translation>Links</translation>
    </message>
    <message>
        <source>Center</source>
        <translation>Zentriert</translation>
    </message>
    <message>
        <source>Right</source>
        <translation>Rechts</translation>
    </message>
    <message>
        <source>Edit stylesheet...</source>
        <translation>Stylesheet editieren...</translation>
    </message>
</context>
<context>
    <name>LoginDialog</name>
    <message>
        <source>Login</source>
        <translation>Anmelden</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Benutzername</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Testmanager NG</source>
        <translation>Testmanager NG</translation>
    </message>
    <message>
        <source>He&amp;lp</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <source>&amp;Options</source>
        <translation>&amp;Optionen</translation>
    </message>
    <message>
        <source>Wi&amp;ndows</source>
        <translation>&amp;Fenster</translation>
    </message>
    <message>
        <source>&amp;Data Sources</source>
        <translation>&amp;Datenquellen</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <source>&amp;Recent files</source>
        <translation>&amp;Zuletzt bearbeitete Dateien</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <source>&amp;Tool Bar</source>
        <translation>&amp;Werkzeugleiste</translation>
    </message>
    <message>
        <source>Data So&amp;urces</source>
        <translation>Daten&amp;quellen</translation>
    </message>
    <message>
        <source>Filter expression</source>
        <translation>Filterausdruck</translation>
    </message>
    <message>
        <source>&amp;Property Editor</source>
        <translation>&amp;Eigenschafts-Editor</translation>
    </message>
    <message>
        <source>P&amp;ython Shell</source>
        <translation>P&amp;ython-Kommandozeile</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;fixed&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;fixed&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Python S&amp;cript</source>
        <translation>Python-S&amp;cript</translation>
    </message>
    <message>
        <source>Broadcasts</source>
        <translation>Broadcasts</translation>
    </message>
    <message>
        <source>&amp;New</source>
        <translation>&amp;Neu</translation>
    </message>
    <message>
        <source>New layout</source>
        <translation>Neues Layout</translation>
    </message>
    <message>
        <source>Ctrl+N</source>
        <translation>Strg+N</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;Speichern</translation>
    </message>
    <message>
        <source>Save layout</source>
        <translation>Layout speichern</translation>
    </message>
    <message>
        <source>Ctrl+S</source>
        <translation>Strg+S</translation>
    </message>
    <message>
        <source>Sa&amp;ve As...</source>
        <translation>Speichern &amp;als...</translation>
    </message>
    <message>
        <source>Save layout to another file</source>
        <translation>Layout in andere Datei speichern</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>S&amp;chließen</translation>
    </message>
    <message>
        <source>Close the application</source>
        <translation>Den Testmanager schließen</translation>
    </message>
    <message>
        <source>About &amp;Qt</source>
        <translation>Über &amp;Qt</translation>
    </message>
    <message>
        <source>&amp;About Testmanager</source>
        <translation>Über den &amp;Testmanager</translation>
    </message>
    <message>
        <source>&amp;Load...</source>
        <translation>&amp;Öffnen...</translation>
    </message>
    <message>
        <source>Load layout</source>
        <translation>Layout laden</translation>
    </message>
    <message>
        <source>Ctrl+O</source>
        <translation>Strg+O</translation>
    </message>
    <message>
        <source>&amp;Add data source...</source>
        <translation>Daten&amp;quelle hinzufügen...</translation>
    </message>
    <message>
        <source>Add a new tab</source>
        <translation>Einen neuen Reiter hinzufügen</translation>
    </message>
    <message>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation>&amp;Kopieren</translation>
    </message>
    <message>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation>&amp;Einfügen</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="obsolete">Einfügen</translation>
    </message>
    <message>
        <source>Ctrl+V</source>
        <translation>Ctrl+V</translation>
    </message>
    <message>
        <source>Add data source</source>
        <translation type="obsolete">Datenquelle hinzufügen</translation>
    </message>
    <message>
        <source>Add a data source</source>
        <translation>Eine Datenquelle hinzufügen</translation>
    </message>
    <message>
        <source>&amp;Test</source>
        <translation>&amp;Test</translation>
    </message>
    <message>
        <source>Start application test</source>
        <translation>Applikationstest starten</translation>
    </message>
    <message>
        <source>&amp;Edit Mode</source>
        <translation>&amp;Editiermodus</translation>
    </message>
    <message>
        <source>Toggle Edit Mode</source>
        <translation>Den Editiermodus umschalten</translation>
    </message>
    <message>
        <source>E</source>
        <translation>E</translation>
    </message>
    <message>
        <source>Add Tab</source>
        <translation>Reiter hinzufügen</translation>
    </message>
    <message>
        <source>Add a tab at the end</source>
        <translation type="obsolete">Fügt einen Karteireiter hinzu</translation>
    </message>
    <message>
        <source>A</source>
        <translation>A</translation>
    </message>
    <message>
        <source>&amp;Global stylesheet...</source>
        <translation>&amp;Globales Stylesheet...</translation>
    </message>
    <message>
        <source>Edit the global stylesheet</source>
        <translation>Das globale Stylesheet editieren</translation>
    </message>
    <message>
        <source>G</source>
        <translation>G</translation>
    </message>
    <message>
        <source>&amp;Connect to data sources</source>
        <translation>Datenquellen &amp;verbinden</translation>
    </message>
    <message>
        <source>Connect all data sources</source>
        <translation>Mit allen Datenquellen verbinden</translation>
    </message>
    <message>
        <source>Connect all referenced data sources</source>
        <translation>Mit allen Datenquellen verbinden</translation>
    </message>
    <message>
        <source>&amp;Parameter sets...</source>
        <translation>&amp;Parametersätze...</translation>
    </message>
    <message>
        <source>Choose from parameter sets</source>
        <translation>Wählen von Parametersätzen</translation>
    </message>
    <message>
        <source>P</source>
        <translation>P</translation>
    </message>
    <message>
        <source>Load &amp;Parameter File...</source>
        <translation>&amp;Parameterdatei laden...</translation>
    </message>
    <message>
        <source>Parameter Sets</source>
        <translation>Parametersätze</translation>
    </message>
    <message>
        <source>Parameters</source>
        <translation>Parameter</translation>
    </message>
    <message>
        <source>New set</source>
        <translation>Neuer Satz</translation>
    </message>
    <message>
        <source>Remove set</source>
        <translation>Satz entfernen</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>Leeren</translation>
    </message>
    <message>
        <source>Failed to open %1</source>
        <translation>Konnte %1 nicht öffnen</translation>
    </message>
    <message>
        <source>Testmanager Layouts (*.tml)</source>
        <translation>Testmanager-Layouts (*.tml)</translation>
    </message>
    <message>
        <source>Any files (*)</source>
        <translation>Alle Dateien (*)</translation>
    </message>
    <message numerus="yes">
        <source>Paste %Ln widgets</source>
        <translation>
            <numerusform>%Ln Widget einfügen</numerusform>
            <numerusform>%n Widgets einfügen</numerusform>
        </translation>
    </message>
    <message>
        <source>Failed to open %1.</source>
        <translation>Konnte %1 nicht öffnen.</translation>
    </message>
    <message>
        <source>New Tab</source>
        <translation>Neuer Reiter</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>Verbinden</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation>Trennen</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Anmelden</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Abmelden</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <source>Replace...</source>
        <oldsource>Replace</oldsource>
        <translation>Ersetzen...</translation>
    </message>
    <message>
        <source>Remove datasource</source>
        <translation type="obsolete">Datenquelle entfernen</translation>
    </message>
    <message>
        <source>Show messages...</source>
        <translation>Meldungen anzeigen...</translation>
    </message>
    <message>
        <source>Configure messages...</source>
        <translation>Meldungen konfigurieren...</translation>
    </message>
    <message>
        <source>Add datasource...</source>
        <translation>Datenquelle hinzufügen...</translation>
    </message>
    <message>
        <source>Copy variable URL</source>
        <translation>Variablen-URL kopieren</translation>
    </message>
    <message numerus="yes">
        <source>Select %Ln subscriber(s)</source>
        <oldsource>Select %Ln subscribers</oldsource>
        <translation>
            <numerusform>%n Abonnenten auswählen</numerusform>
            <numerusform>%n Abonnenten auswählen</numerusform>
        </translation>
    </message>
    <message>
        <source>Expand complete subtree</source>
        <translation>Alles ausklappen</translation>
    </message>
    <message>
        <source>Login failed, invalid username or password.</source>
        <translation type="vanished">Anmelden fehlgeschlagen, Benutzername oder Passwort falsch.</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>Zurücksetzen</translation>
    </message>
    <message>
        <source>Clear list</source>
        <translatorcomment>Liste leeren</translatorcomment>
        <translation>Liste leeren</translation>
    </message>
    <message>
        <source>Remove parameter</source>
        <translation>Parameter entfernen</translation>
    </message>
    <message>
        <source>Clear parameter list</source>
        <translation>Parameterliste leeren</translation>
    </message>
    <message>
        <source>%1&#x202f;KB/s</source>
        <oldsource>%1â¯KB/s</oldsource>
        <translation>%1&#x202f;KB/s</translation>
    </message>
    <message>
        <source>Data Slots</source>
        <translation>Datenverbindungen</translation>
    </message>
    <message>
        <source>Import error</source>
        <translation>Importfehler</translation>
    </message>
    <message>
        <source>Layout File version %1 is not implemented.</source>
        <translation>Version %1 der Layout-Datei ist nicht implementiert.</translation>
    </message>
    <message>
        <source>Layout File format update</source>
        <translation>Änderung des Layout-Dateiformats</translation>
    </message>
    <message>
        <source>The Layout file format will be updated to a newer Testmanager version. The file won&apos;t be readable with older Testmanager versions.</source>
        <translation>Das Dateiformat der Layout-Datei wird an eine neue Testmanager-Version angepasst. Die Datei kann dadurch von älteren Testmanager-Versionen nicht mehr gelesen werden.</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation>Unbekannter Fehler</translation>
    </message>
    <message>
        <source>Login failed</source>
        <translation>Anmeldung fehlgeschlagen</translation>
    </message>
    <message>
        <source>Parameter Set Management</source>
        <translation>Parametersatzverwaltung</translation>
    </message>
    <message>
        <source>Save current values in parameter file (.par).</source>
        <translation>Aktuelle Werte in Parameterdatei (.par) speichern.</translation>
    </message>
    <message>
        <source>Export...</source>
        <translation>Exportieren...</translation>
    </message>
</context>
<context>
    <name>MessageDialog</name>
    <message>
        <source>Configure Messages</source>
        <translation>Meldungen konfigurieren</translation>
    </message>
    <message>
        <source>Plain messages file:</source>
        <translation>Meldungsdatei:</translation>
    </message>
    <message>
        <source>Select...</source>
        <translation>Auswählen...</translation>
    </message>
    <message>
        <source>EtherLab PlainMessages files (*.xml)</source>
        <translation>EtherLab-Meldungen (*.xml)</translation>
    </message>
    <message>
        <source>Any files (*)</source>
        <translation>Alle Dateien (*)</translation>
    </message>
</context>
<context>
    <name>MessageWidget</name>
    <message>
        <source>Messages</source>
        <translation>Meldungen</translation>
    </message>
</context>
<context>
    <name>MultiDimParameterDialog</name>
    <message>
        <source>Variable: </source>
        <translation>Variable: </translation>
    </message>
    <message>
        <source>Set Parameter</source>
        <translation>Setze Parameter</translation>
    </message>
</context>
<context>
    <name>Parameter</name>
    <message>
        <source>Parameter poll failed: %1</source>
        <translation>Parameter Wertanfrage fehlgeschlagen: %1</translation>
    </message>
    <message>
        <source>URL not found: %1</source>
        <translation>URL nicht gefunden: %1</translation>
    </message>
    <message>
        <source>Fetching variable failed: %1</source>
        <translation>Anfrage zur Variable fehlgeschlagen: %1</translation>
    </message>
</context>
<context>
    <name>ParameterDialog</name>
    <message>
        <source>Remove parameter</source>
        <translation>Parameter Entfernen</translation>
    </message>
    <message>
        <source>Clear parameter list</source>
        <translation>Parameterliste leeren</translation>
    </message>
    <message>
        <source>Meta Data</source>
        <translation>Metadaten</translation>
    </message>
    <message>
        <source>Creator:</source>
        <translation>Ersteller:</translation>
    </message>
    <message>
        <source>Created By:</source>
        <translation>Erstellt von:</translation>
    </message>
    <message>
        <source>Creation Date:</source>
        <translation>Erstellungsdatum:</translation>
    </message>
    <message>
        <source>Description:</source>
        <translation>Beschreibung:</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <source>Save As...</source>
        <translation>Speichern als...</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>Anwenden</translation>
    </message>
    <message>
        <source>Load Parameter File</source>
        <translation>Parameterdatei Laden</translation>
    </message>
    <message>
        <source>Write Parameter Values to Data Source.</source>
        <translation>Parameterwerte zur Datenquelle schicken.</translation>
    </message>
</context>
<context>
    <name>ParameterModel</name>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <source>Variable</source>
        <translation>Variable</translation>
    </message>
    <message>
        <source>Url not found: %1</source>
        <translation>URL nicht gefunden: %1</translation>
    </message>
    <message>
        <source>Process variable %1 must be writable!</source>
        <translation>Prozessvariable %1 muss schreibbar sein!</translation>
    </message>
    <message>
        <source>Failed to open %1</source>
        <translation>Öffnen von %1 fehlgeschlagen</translation>
    </message>
    <message>
        <source>Path: %1</source>
        <translation>Pfad: %1</translation>
    </message>
    <message>
        <source>Dimension: %1</source>
        <translation>Dimension: %1</translation>
    </message>
    <message>
        <source>Please select variables only.</source>
        <translation>Bitte wählen sie ausschließlich Variablen aus.</translation>
    </message>
</context>
<context>
    <name>ParameterSaveDialog</name>
    <message>
        <source>Save Parameters...</source>
        <translation>Parameter speichern...</translation>
    </message>
    <message>
        <source>Select the saving destination</source>
        <translation>Wählen Sie den Speicherort</translation>
    </message>
    <message>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <source>Description of the parameter set values:</source>
        <translation>Beschreibung des Parametersatzes:</translation>
    </message>
    <message>
        <source>Parameter Files (*.par)</source>
        <translation>Parameterdateien (*.par)</translation>
    </message>
    <message>
        <source>Any files (*)</source>
        <translation>Alle Dateien (*)</translation>
    </message>
</context>
<context>
    <name>ParameterSetModel</name>
    <message>
        <source>Parameter Set %1</source>
        <translation>Parametersatz %1</translation>
    </message>
</context>
<context>
    <name>ParameterSetWidgetPlugin</name>
    <message>
        <source>Parameter Set Widget</source>
        <translation>Parametersatz-Auswahl</translation>
    </message>
</context>
<context>
    <name>ParameterTableModel</name>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>Current value</source>
        <translation>Aktueller Wert</translation>
    </message>
    <message>
        <source>Stored value</source>
        <translation>Gespeicherter Wert</translation>
    </message>
    <message>
        <source>Variable</source>
        <translation>Variable</translation>
    </message>
    <message>
        <source>Any files (*)</source>
        <translation>Alle Dateien (*)</translation>
    </message>
    <message>
        <source>Failed to open %1.</source>
        <translation>Konnte %1 nicht öffnen.</translation>
    </message>
    <message>
        <source>Path: %1</source>
        <translation>Pfad: %1</translation>
    </message>
    <message>
        <source>Dimension: %1</source>
        <translation>Dimension: %1</translation>
    </message>
    <message>
        <source>Parameter Files (*.par)</source>
        <translation>Parameterdateien (*.par)</translation>
    </message>
    <message>
        <source>Parameter file parsing error (%1) at offset %2: %3</source>
        <translation>Fehler beim Einlesen der Parameterdatei (%1) an Position %2: %3</translation>
    </message>
    <message>
        <source>Invalid content in parameter file %1: %2</source>
        <translation>Ungültiger Inhalt in der Parameterdatei %1: %2</translation>
    </message>
    <message>
        <source>Parameter file error</source>
        <translation>Fehler in Parameterdatei</translation>
    </message>
</context>
<context>
    <name>PropertyModel</name>
    <message>
        <source>Property</source>
        <translation>Eigenschaft</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
</context>
<context>
    <name>PushButtonPlugin</name>
    <message>
        <source>Push Button</source>
        <translation>Schaltfläche</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Exactly one connection possible.</source>
        <translation>Maximal eine Verbindung möglich.</translation>
    </message>
    <message>
        <source>Please select a Variable</source>
        <translation>Bitte wählen sie eine Variable aus</translation>
    </message>
    <message>
        <source>Error occured during subscribing to a variable: </source>
        <translation>Bei der Abbonierung einer Variable ist ein Fehler aufgetreten: </translation>
    </message>
    <message>
        <source>Please select exactly one variable.</source>
        <translation>Bitte wählen Sie genau eine Variable aus.</translation>
    </message>
    <message>
        <source>Stack %1</source>
        <translation>Stapel %1</translation>
    </message>
    <message>
        <source>Please select one or more variables.</source>
        <translation>Bitte wählen Sie eine oder mehrere Variablen aus.</translation>
    </message>
    <message>
        <source>Already connected. Press Shift to replace.</source>
        <translation>Verbindung bereits hergestellt. Durch Drücken von &apos;Shift&apos; ersetzen.</translation>
    </message>
</context>
<context>
    <name>RadioButtonPlugin</name>
    <message>
        <source>RadioButton</source>
        <translation>Auswahlfeld</translation>
    </message>
</context>
<context>
    <name>ReplaceDialog</name>
    <message>
        <source>Replace data source...</source>
        <translation>Datenquelle ersetzen...</translation>
    </message>
    <message>
        <source>Use Authentification</source>
        <translation>Authentifizierung aktivieren</translation>
    </message>
    <message>
        <source>Use TLS Encryption</source>
        <translation>Verwende TLS-Verschlüsselung</translation>
    </message>
    <message>
        <source>Load Certificate...</source>
        <translation>CA-Zertifikat laden...</translation>
    </message>
    <message>
        <source>Rep&amp;lace during this session only</source>
        <translation>Nur in dieser &amp;Sitzung ersetzen</translation>
    </message>
    <message>
        <source>Replace &amp;permanently</source>
        <translation>&amp;Dauerhaft ersetzen</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>New URL for datasource %1:</source>
        <translation>Neue URL für Datenquelle %1:</translation>
    </message>
</context>
<context>
    <name>ScalarSubscriberDataSlot</name>
    <message>
        <source>Url not found: %1</source>
        <translation>URL nicht gefunden: %1</translation>
    </message>
</context>
<context>
    <name>ScaleDelegate</name>
    <message>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <source>rpm</source>
        <translatorcomment>Umdrehungen pro Minute</translatorcomment>
        <translation>1/min</translation>
    </message>
    <message>
        <source>°</source>
        <translation>°</translation>
    </message>
    <message>
        <source>bar</source>
        <translation>bar</translation>
    </message>
</context>
<context>
    <name>ScriptVariable</name>
    <message>
        <source>Script variable subscription failed: %1</source>
        <translation>Abonnement der Script-Variable fehlgeschlagen: %1</translation>
    </message>
    <message>
        <source>Script variable poll failed: %1</source>
        <translation>Abruf der Script-Variable fehlgeschlagen: %1</translation>
    </message>
    <message>
        <source>URL not found: %1</source>
        <translation>URL nicht gefunden: %1</translation>
    </message>
</context>
<context>
    <name>ScriptVariableModel</name>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>Period [s]</source>
        <translation>Periode [s]</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <source>Variable</source>
        <translation>Variable</translation>
    </message>
    <message>
        <source>Url not found: %1</source>
        <translation>URL nicht gefunden: %1</translation>
    </message>
</context>
<context>
    <name>Selector</name>
    <message>
        <source>Scalar, Index: </source>
        <translation>Skalar, Index: </translation>
    </message>
    <message>
        <source>Default Selection</source>
        <translation>Standard-Auswahl</translation>
    </message>
</context>
<context>
    <name>SelectorDialog</name>
    <message>
        <source>Variable Selector</source>
        <translation>Variablen-Selektor</translation>
    </message>
    <message>
        <source>Use full vector</source>
        <translation>Verwende den gesamten Vektor</translation>
    </message>
    <message>
        <source>Select a scalar</source>
        <translation>Wähle einen Skalar</translation>
    </message>
    <message>
        <source>Index (zero based):</source>
        <translation>Index (bei Null beginnend):</translation>
    </message>
    <message>
        <source>Unknown error.</source>
        <translation>Unbekannter Fehler.</translation>
    </message>
    <message>
        <source>No Variables supplied to choose Selector.</source>
        <translation>Keine Variablen verfügbar um eine Auswahl zu treffen.</translation>
    </message>
    <message>
        <source>Supplied DataNode is not a Variable.</source>
        <translation>Datenquelle ist keine Variable.</translation>
    </message>
    <message>
        <source>Shapes of the Variables are too different. Please try to drop them separately.</source>
        <translation>Dimensionen der ausgewählten Variablen sind zu verschieden. Bitte versuchen Sie, die Variablen einzeln auf die Widgets zu ziehen.</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <source>Error during choosing a variable selector</source>
        <translation>Fehler bei der Auswahl einer Variablen</translation>
    </message>
    <message>
        <source>Your choices do not apply cleanly to all variables. Please reconsider your choices (e.g. the indices) or try to drop the variables one by one.</source>
        <translation>Ihre Auswahl ist nicht auf alle ausgewählten Variablen anwendbar. Bitte überdenken Sie ihre Auswahl oder versuchen Sie, die Variablen einzeln auf die Widgets zu ziehen.</translation>
    </message>
</context>
<context>
    <name>SendBroadcastPlugin</name>
    <message>
        <source>Send Broadcast</source>
        <translation>Broadcast senden</translation>
    </message>
</context>
<context>
    <name>SetpointSlotCategory</name>
    <message>
        <source>Setpoint</source>
        <translation>Sollwert</translation>
    </message>
    <message>
        <source>Please select a writeable variable!</source>
        <translation type="vanished">Bitte wählen Sie eine beschreibbare Variable aus!</translation>
    </message>
</context>
<context>
    <name>SimpleRWSlotModel</name>
    <message>
        <source>Variable is not writeable!</source>
        <translation>Variable ist schreibgeschützt!</translation>
    </message>
</context>
<context>
    <name>SlotDialog</name>
    <message>
        <source>Data connections</source>
        <translation>Datenverbindungen</translation>
    </message>
    <message>
        <source>Apply %1 to all</source>
        <translation>%1 auf alle anwenden</translation>
    </message>
    <message>
        <source>Remove variable</source>
        <translation>Variable entfernen</translation>
    </message>
    <message>
        <source>Jump to variable</source>
        <translation>Zur Variable springen</translation>
    </message>
</context>
<context>
    <name>SlotModel</name>
    <message>
        <source>&lt;%1 Widget at Tab &quot;%2&quot;&gt;</source>
        <translation>&lt;%1-Widget auf Tab &quot;%2&quot;&gt;</translation>
    </message>
    <message>
        <source>Please select a writeable variable!</source>
        <translation>Bitte wählen Sie eine beschreibbare Variable aus!</translation>
    </message>
</context>
<context>
    <name>SlotModelCollection</name>
    <message>
        <source>Variable</source>
        <translation>Variable</translation>
    </message>
    <message>
        <source>Alias</source>
        <translation>Alias</translation>
    </message>
    <message>
        <source>Period [s]</source>
        <oldsource>Period</oldsource>
        <translation>Periode [s]</translation>
    </message>
    <message>
        <source>Scale</source>
        <translation>Skalierung</translation>
    </message>
    <message>
        <source>Offset</source>
        <translation>Versatz</translation>
    </message>
    <message>
        <source>LPF [s]</source>
        <oldsource>LPF</oldsource>
        <translation>Filterkonstante [s]</translation>
    </message>
    <message>
        <source>Color</source>
        <translation>Farbe</translation>
    </message>
    <message>
        <source>Drop to establish connection.</source>
        <translation>Loslassen um eine Verbindung herzustellen.</translation>
    </message>
    <message>
        <source>Please drop Variables or Sources onto Widgets or its categories in the Connection Editor.</source>
        <translation>Variablen oder Datenquellen bitte auf die Widgets oder ihre Kategorien ziehen.</translation>
    </message>
    <message>
        <source>Process Variables or Data Sources required.</source>
        <translation>Es wird eine Variable oder eine Datenquelle benötigt.</translation>
    </message>
    <message>
        <source>Please use the first column for dragging.</source>
        <translation>Bitte die erste Spalte für Drag and drop verwenden.</translation>
    </message>
    <message>
        <source>Url not found: %1</source>
        <translation>URL nicht gefunden: %1</translation>
    </message>
    <message>
        <source>Selection</source>
        <translation>Feld</translation>
    </message>
    <message>
        <source>Field selection from multidimensional Variables.</source>
        <translation>Auswahl eines Feldes einer multidimensionalen Variablen.</translation>
    </message>
</context>
<context>
    <name>SlotNode</name>
    <message>
        <source>This Node does not support dropping</source>
        <translation>Dieser Knoten unterstützt kein Drag and Drop</translation>
    </message>
</context>
<context>
    <name>SlotView</name>
    <message>
        <source>Apply %1 to all</source>
        <translation>%1 auf alle anwenden</translation>
    </message>
    <message>
        <source>Apply %1 to selected and their children</source>
        <translation>%1 auf ausgewählte und deren Subelemente anwenden</translation>
    </message>
    <message>
        <source>Remove selected</source>
        <translation>Ausgewählte entfernen</translation>
    </message>
    <message>
        <source>Jump to variable</source>
        <translation>Zur Variable springen</translation>
    </message>
</context>
<context>
    <name>StyleDialog</name>
    <message>
        <source>Edit Stylesheet</source>
        <translation>Stylesheet editieren</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;See &lt;a href=&quot;http://doc.qt.io/qt-5/stylesheet-reference.html&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://doc.qt.io/qt-5/stylesheet-reference.html&lt;/span&gt;&lt;/a&gt; for a complete reference.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Use path prefix &amp;quot;layout:&amp;quot; to access paths relative to layout.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Tab page class name is &amp;quot;TabPage&amp;quot;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Siehe &lt;a href=&quot;http://doc.qt.io/qt-5/stylesheet-reference.html&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://doc.qt.io/qt-5/stylesheet-reference.html&lt;/span&gt;&lt;/a&gt; für die vollständige Referenz.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Das Pfad-Präfix „layout:“ erlaubt Pfade relativ zur Layout-Datei.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Der Klassenname des Reiter-Widgets ist „TabPage“.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Add Resource...</source>
        <translation>Ressource hinzufügen...</translation>
    </message>
    <message>
        <source>Add Color...</source>
        <translation>Farbe hinzufügen...</translation>
    </message>
    <message>
        <source>Add Font...</source>
        <translation>Schriftart hinzufügen...</translation>
    </message>
    <message>
        <source>Image files (*.png *.svg *.bmp)</source>
        <translation>Bild-Dateien (*.png *.svg *.bmp)</translation>
    </message>
    <message>
        <source>Any files (*)</source>
        <translation>Alle Dateien (*)</translation>
    </message>
</context>
<context>
    <name>SvgPlugin</name>
    <message>
        <source>Animated Vector Graphics</source>
        <translation>Animierte Vektorgrafik</translation>
    </message>
</context>
<context>
    <name>SvgPluginDialog</name>
    <message>
        <source>Svg Table Editor</source>
        <translation>SVG-Tabelleneditor</translation>
    </message>
    <message>
        <source>Path to svg file.</source>
        <translation>Pfad zu SVG-Datei.</translation>
    </message>
    <message>
        <source>Attribute ID</source>
        <translation>Attribut-ID</translation>
    </message>
</context>
<context>
    <name>TabDialog</name>
    <message>
        <source>Edit Tab</source>
        <translation>Reiter editieren</translation>
    </message>
</context>
<context>
    <name>TabPage</name>
    <message>
        <source>Unknown widget type %1</source>
        <translation>Unbekannter Widget-Typ %1</translation>
    </message>
    <message numerus="yes">
        <source>Paste %Ln widgets</source>
        <translation>
            <numerusform>Widget einfügen</numerusform>
            <numerusform>%n Widgets einfügen</numerusform>
        </translation>
    </message>
    <message>
        <source>Edit stylesheet...</source>
        <translation>Stylesheet editieren...</translation>
    </message>
</context>
<context>
    <name>TableViewPlugin</name>
    <message>
        <source>Table View</source>
        <translation>Tabellenansicht</translation>
    </message>
</context>
<context>
    <name>TableViewPluginDialog</name>
    <message>
        <source>Table View Editor</source>
        <translation>Tabellenansichts-Editor</translation>
    </message>
    <message>
        <source>Header</source>
        <translation>Titel</translation>
    </message>
    <message>
        <source>Decimals</source>
        <translation>Dezimalstellen</translation>
    </message>
</context>
<context>
    <name>TableViewSlotModel</name>
    <message>
        <source>Columns</source>
        <translation>Spalten</translation>
    </message>
    <message>
        <source>Highlighted row</source>
        <translation>Hervorgehobene Zeile</translation>
    </message>
    <message>
        <source>Number of Rows</source>
        <translation>Anzahl der Zeilen</translation>
    </message>
</context>
<context>
    <name>TextPlugin</name>
    <message>
        <source>Text Display</source>
        <translation>Textanzeige</translation>
    </message>
</context>
<context>
    <name>TextPluginDialog</name>
    <message>
        <source>Text Table Editor</source>
        <oldsource>Text Hash Editor</oldsource>
        <translation>Text-Tabelle bearbeiten</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <source>Text</source>
        <translation>Text</translation>
    </message>
</context>
<context>
    <name>TooltipMatrix</name>
    <message>
        <source>Variable subscription failed</source>
        <translation>Abonnement der Variable fehlgeschlagen</translation>
    </message>
    <message>
        <source>Variable subscription failed: %1</source>
        <translation>Abonnement der Variable fehlgeschlagen: %1</translation>
    </message>
    <message>
        <source>Variable polling failed: %1</source>
        <translation>Abfragen der Variable fehleschlagen: %1</translation>
    </message>
</context>
<context>
    <name>TouchEditPlugin</name>
    <message>
        <source>Parameter Touch Editor</source>
        <translation>Parameter-Touch-Editor</translation>
    </message>
</context>
<context>
    <name>ValueSlotCategory</name>
    <message>
        <source>Display</source>
        <translation>Anzeige</translation>
    </message>
</context>
<context>
    <name>WidgetContainer</name>
    <message>
        <source>Property %1 does not exist.</source>
        <translation>Eigenschaft %1 existiert nicht.</translation>
    </message>
    <message>
        <source>Failed to set property %1 to %2</source>
        <translation>Konnte Eigenschaft %1 nicht auf %2 setzen</translation>
    </message>
    <message>
        <source>Syntax error in line %1: %2</source>
        <translation>Syntax-Fehler in Zeile %1: %2</translation>
    </message>
    <message numerus="yes">
        <source>Copy %Ln widget(s)</source>
        <translation>
            <numerusform>Widget kopieren</numerusform>
            <numerusform>%n Widgets kopieren</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>Remove %Ln widget(s)</source>
        <translation>
            <numerusform>Widget entfernen</numerusform>
            <numerusform>%n Widgets entfernen</numerusform>
        </translation>
    </message>
    <message>
        <source>Clear process data</source>
        <translation type="obsolete">Prozessdaten entfernen</translation>
    </message>
    <message>
        <source>Process data connections...</source>
        <translation>Datenverbindungen...</translation>
    </message>
    <message>
        <source>Clear process data connections</source>
        <translation type="obsolete">Datenverbindungen entfernen</translation>
    </message>
    <message>
        <source>Jump to variable</source>
        <translation>Zur Variable springen</translation>
    </message>
    <message numerus="yes">
        <source>Clear process data connections of %Ln widget(s)</source>
        <translation>
            <numerusform>Datenverbindungen leeren</numerusform>
            <numerusform>Datenverbindungen von %n Widgets leeren</numerusform>
        </translation>
    </message>
    <message>
        <source>Align left</source>
        <translation>Linksbündig ausrichten</translation>
    </message>
    <message>
        <source>Align center</source>
        <translation>Zentriert ausrichten</translation>
    </message>
    <message>
        <source>Align right</source>
        <translation>Rechtsbündig ausrichten</translation>
    </message>
    <message>
        <source>Send to back</source>
        <translation>Nach hinten</translation>
    </message>
    <message>
        <source>Bring to front</source>
        <translation>Nach vorne</translation>
    </message>
    <message>
        <source>Callable update() not found.</source>
        <translation>Aufrufbares update() nicht gefunden.</translation>
    </message>
    <message>
        <source>update() is not callable.</source>
        <translation>update() ist nicht aufrufbar.</translation>
    </message>
    <message>
        <source>Error in excecution!</source>
        <translation type="obsolete">Fehler bei der Ausführung!</translation>
    </message>
    <message>
        <source>Edit stylesheet...</source>
        <translation type="obsolete">Stylesheet editieren...</translation>
    </message>
    <message numerus="yes">
        <source>Edit stylesheet of %Ln widget(s)...</source>
        <translation>
            <numerusform>Stylesheet editieren...</numerusform>
            <numerusform>Stylesheet von %n Widgets editieren...</numerusform>
        </translation>
    </message>
    <message>
        <source>Edit script...</source>
        <translation>Script bearbeiten...</translation>
    </message>
    <message>
        <source>Show legend</source>
        <translation>Legende anzeigen</translation>
    </message>
    <message>
        <source>Url not found: %1</source>
        <translation>URL nicht gefunden: %1</translation>
    </message>
    <message>
        <source>Process variables required.</source>
        <translation>Prozessdaten benötigt.</translation>
    </message>
    <message numerus="yes">
        <source>Connect %Ln process variables.</source>
        <translation>
            <numerusform>Prozessvariable verbinden.</numerusform>
            <numerusform>%n Prozessvariablen verbinden.</numerusform>
        </translation>
    </message>
    <message>
        <source>Connect %1.</source>
        <translation>Verbinde mit %1.</translation>
    </message>
    <message numerus="yes">
        <source>Replace %Ln process variables.</source>
        <translation>
            <numerusform>Prozessvariable ersetzen.</numerusform>
            <numerusform>%n Prozessvariablen ersetzen.</numerusform>
        </translation>
    </message>
    <message>
        <source>Replace with %1.</source>
        <translation>Ersetze durch %1.</translation>
    </message>
</context>
<context>
    <name>XYGraphPlugin</name>
    <message>
        <source>XY Graph</source>
        <translation>X/Y-Graph</translation>
    </message>
</context>
<context>
    <name>XYGraphSlotModel</name>
    <message>
        <source>X Axis</source>
        <translation>X-Achse</translation>
    </message>
    <message>
        <source>Y Axis</source>
        <translation>Y-Achse</translation>
    </message>
</context>
</TS>
