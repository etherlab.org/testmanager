/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2023 Bjarne von Horn <vh at igh dot de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "SelectorDialog.h"

#include <QJsonArray>
#include <QMessageBox>

#include <pdcom5/Variable.h>
#include <pdcom5/SizeTypeInfo.h>

#include "DataNode.h"

#include "ui_SelectorDialog.h"

enum {
    NoDataNodes = -1,
    NoVariable = -2,
    DimensionMismatch = -3,
};

static int checkNodeDimensions(const QList<DataNode *> &nodes)
{
    if (nodes.empty()) {
        return NoDataNodes;
    }
    if (nodes.first()->getVariable().empty()) {
        return NoVariable;
    }
    const auto dimension =
            nodes.first()->getVariable().getSizeInfo().dimension();
    for (int i = 1; i < nodes.size(); ++i) {
        if (nodes[i]->getVariable().empty()) {
            return NoVariable;
        }
        if (nodes[i]->getVariable().getSizeInfo().size() != dimension) {
            return DimensionMismatch;
        }
    }
    return dimension;
}

SelectorDialog::SelectorDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SelectorDialog())
{
    ui->setupUi(this);
    connect(ui->rbSelectScalar,
            &QAbstractButton::toggled,
            this,
            &SelectorDialog::scalarRadioButtonToggled);
    spinBoxes.append(ui->spinBox);

    ui->rbSelectScalar->setEnabled(false);
    ui->rbUseFullVector->setEnabled(false);
    ui->spinBox->setEnabled(false);
}

SelectorDialog::SelectorDialog(
        QList<DataNode *> nodes,
        SlotNode::AcceptedDataConnectionModes requestedMode,
        QWidget *parent):
    SelectorDialog(parent)
{
    this->nodes = nodes;

    if (requestedMode == SlotNode::AcceptedDataConnectionModes::Scalar) {
        // scalars requested. nodes contain at least one non-scalar,
        // otherwise dialog would not be displayed.
        prepareScalar();
    }
}

void SelectorDialog::prepareScalar()
{
    const auto num_dimensions = checkNodeDimensions(nodes);
    if (num_dimensions < 0) {
        ui->buttonBox->setStandardButtons(QDialogButtonBox::Cancel);

        QString err = tr("Unknown error.");
        switch (num_dimensions) {
            case NoDataNodes:
                err = tr("No Variables supplied to choose Selector.");
                break;
            case NoVariable:
                err = tr("Supplied DataNode is not a Variable.");
                break;
            case DimensionMismatch:
                err = tr("Shapes of the Variables are too different. Please "
                         "try to drop them separately.");
                break;
            default:
                break;
        };
        auto mb = new QMessageBox(QMessageBox::Critical, tr("Error"), err, QMessageBox::Ok, this);
        connect(mb, &QDialog::finished, mb, &QObject::deleteLater);
        connect(mb, &QDialog::finished, this, &QDialog::reject);
        connect(this, &SelectorDialog::opened, mb, &QDialog::open);
    }
    else {
        ui->rbSelectScalar->setChecked(true);
        ui->rbUseFullVector->setEnabled(false);
        // add missing spinboxes
        for (int i = 1; i < num_dimensions; ++i) {
            auto sp = new QSpinBox(this);
            ui->hlIndices->addWidget(sp);
            spinBoxes.append(sp);
        }
        // set maxima
        for (int dim = 0; dim < num_dimensions; ++dim) {
            int max = INT_MAX;
            for (const auto node : nodes) {
                max = std::min<int>(
                        max,
                        node->getVariable().getSizeInfo()[dim]);
            }
            spinBoxes[dim]->setMaximum(max - 1 /* zero based index */);
            spinBoxes[dim]->setEnabled(true);
        }
    }
}

void SelectorDialog::changeEvent(QEvent *event)
{
    QDialog::changeEvent(event);
    if (event->type() == QEvent::LanguageChange) {
        ui->retranslateUi(this);
    }
}

void SelectorDialog::setData(DataNode &node, QJsonObject selector)
{
    nodes = {&node};

    if (selector["type"] == "Scalar") {
        auto var = node.getVariable();
        if (var.empty())
            return;

        const auto sizeInfo = var.getSizeInfo();
        const auto indices = selector["indices"].toArray();
        ui->rbSelectScalar->setChecked(true);
        ui->rbUseFullVector->setEnabled(false);

        prepareScalar();

        int maxIdx = std::min<int>(indices.size(), spinBoxes.size());
        for (int i = 0; i < maxIdx; ++i) {
            spinBoxes[i]->setValue(indices[i].toInt());
        }
    }
}

bool SelectorDialog::needsToShow(
        const QList<DataNode *> &nodes,
        SlotNode::AcceptedDataConnectionModes requestedMode)
{
    using ACDM = SlotNode::AcceptedDataConnectionModes;
    if (requestedMode == ACDM::Process
        || requestedMode == ACDM::VariableButNoSelector) {
        return false;
    }

    if (requestedMode == ACDM::Scalar
        || requestedMode == ACDM::MatrixOrScalar) {
        for (const auto node : nodes) {
            const auto var = node->getVariable();
            if (var.empty() || !var.getSizeInfo().isScalar()) {
                return true;
            }
        }
        // all variables are scalars, scalars requested -> nothing to do.
        return false;
    }
    return true;
}

void SelectorDialog::open()
{
    QDialog::open();

    emit opened();
}

SelectorDialog::~SelectorDialog()
{
    delete ui;
}

void SelectorDialog::accept()
{
    if (ui->rbSelectScalar->isChecked()) {
        std::vector<int> indices;
        for (const auto sp : spinBoxes) {
            indices.push_back(sp->value());
        }
        selector = Selector::scalarSelector(indices);
    }
    // verify that selector is happy with all variables
    bool success = true;
    const auto pdSelector = selector.toPdComSelector();
    for (const auto node : nodes) {
        try {
            pdSelector.getViewSizeInfo(node->getVariable());
        }
        catch (PdCom::Exception const &) {
            success = false;
            break;
        }
    }
    if (!success) {
        QMessageBox::critical(
                this,
                tr("Error during choosing a variable selector"),
                tr("Your choices do not apply cleanly to all variables. "
                   "Please reconsider your choices (e.g. the indices) or try "
                   "to drop the variables one by one."));
    }
    else {
        QDialog::accept();
    }
}

void SelectorDialog::scalarRadioButtonToggled(bool state)
{
    for (const auto sp : spinBoxes) {
        sp->setEnabled(state);
    }
}
